import {
	REQUEST_NAME_CHANGED,
	REQUEST_EMAIL_CHANGED,
	REQUEST_LOCATION_CHANGED,
	REQUEST_MESSAGE_CHANGED,
	SEND_REQUEST_BEGIN,
	SEND_REQUEST_SUCCESS,
	SEND_REQUEST_FAIL
} from './types';

export const requestNameChanged = (text) => {
	return {
		type: REQUEST_NAME_CHANGED,
		payload: text
	};
};
export const requestEmailChanged = (text) => {
	return {
		type: REQUEST_EMAIL_CHANGED,
		payload: text
	};
};
export const requestLocationChanged = (text) => {
	return {
		type: REQUEST_LOCATION_CHANGED,
		payload: text
	};
};
export const requestMessageChanged = (text) => {
	return {
		type: REQUEST_MESSAGE_CHANGED,
		payload: text
	};
};

export const sendRequest = (name, email, location, message, images) => {
	return (dispatch) => {
		dispatch({ type: SEND_REQUEST_BEGIN });

		var data = new FormData();
		data.append('name', name);
		data.append('email', email);
		data.append('location', location);
		data.append('message', message);

		if(images.length > 0) {
			var i = 1;
			images.map((image) => {
				data.append('image'+i, {
					uri: image,
					name: "image"+i+".jpg",
					type: "multipart/form-data"
				});

				i++;
			});
		}

		console.log(data);

		fetch(global.site_url+'/zeleni-api/request.php', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data'
			},
			body: data
		}).then((response) => response.json()).then((responseJson) => {
			console.log(responseJson);
			sendRequestSuccess(dispatch, responseJson);
		}).catch(error => {
			sendRequestFail(dispatch, responseJson);
		});
	};
};

const sendRequestSuccess = (dispatch, response) => {
	dispatch({
		type: SEND_REQUEST_SUCCESS,
		payload: response
	});
};

const sendRequestFail = (dispatch, error) => {
	dispatch({
		type: SEND_REQUEST_FAIL,
		payload: error
	});
};
