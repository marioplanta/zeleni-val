export const FETCH_NEWS_BEGIN = 'fetch_news_begin';
export const FETCH_NEWS_SUCCESS = 'fetch_news_success';
export const FETCH_NEWS_FAIL = 'fetch_news_fail';

export const FETCH_LISTINGS_BEGIN = 'fetch_listings_begin';
export const FETCH_LISTINGS_SUCCESS = 'fetch_listings_success';
export const FETCH_LISTINGS_FAIL = 'fetch_listings_fail';
export const LISTING_NAME_CHANGED = 'listing_name_changed';
export const LISTING_EMAIL_CHANGED = 'listing_email_changed';
export const LISTING_MESSAGE_CHANGED = 'listing_message_changed';
export const SEND_LISTING_BEGIN = 'send_listing_begin';
export const SEND_LISTING_SUCCESS = 'send_listing_success';
export const SEND_LISTING_FAIL = 'send_listing_fail';

export const CONTACT_NAME_CHANGED = 'contact_name_changed';
export const CONTACT_EMAIL_CHANGED = 'contact_email_changed';
export const CONTACT_MESSAGE_CHANGED = 'contact_message_changed';
export const SEND_MESSAGE_BEGIN = 'send_message_begin';
export const SEND_MESSAGE_SUCCESS = 'send_message_success';
export const SEND_MESSAGE_FAIL = 'send_message_fail';

export const REPORT_NAME_CHANGED = 'report_name_changed';
export const REPORT_EMAIL_CHANGED = 'report_email_changed';
export const REPORT_LOCATION_CHANGED = 'report_location_changed';
export const REPORT_MESSAGE_CHANGED = 'report_message_changed';
export const SEND_REPORT_BEGIN = 'send_report_begin';
export const SEND_REPORT_SUCCESS = 'send_report_success';
export const SEND_REPORT_FAIL = 'send_report_fail';

export const REQUEST_NAME_CHANGED = 'request_name_changed';
export const REQUEST_EMAIL_CHANGED = 'request_email_changed';
export const REQUEST_LOCATION_CHANGED = 'request_location_changed';
export const REQUEST_MESSAGE_CHANGED = 'request_message_changed';
export const SEND_REQUEST_BEGIN = 'send_request_begin';
export const SEND_REQUEST_SUCCESS = 'send_request_success';
export const SEND_REQUEST_FAIL = 'send_request_fail';