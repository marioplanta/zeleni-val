import {
	FETCH_NEWS_BEGIN,
	FETCH_NEWS_SUCCESS,
	FETCH_NEWS_FAIL
} from './types';

export const fetchNews = () => {
	return (dispatch) => {
		dispatch({ type: FETCH_NEWS_BEGIN });

		fetch(global.site_url+'/wp-json/wp/v2/posts?_embed&per_page=100', {
			method: 'GET'
		}).then((response) => response.json()).then((responseJson) => {
			console.log(responseJson);
			fetchNewsSuccess(dispatch, responseJson);
		}).catch(error => {
			fetchNewsFail(dispatch, responseJson);
		});
	};
};

const fetchNewsSuccess = (dispatch, response) => {
	console.log(response);
	dispatch({
		type: FETCH_NEWS_SUCCESS,
		payload: response
	});
};

const fetchNewsFail = (dispatch, error) => {
	dispatch({
		type: FETCH_NEWS_FAIL,
		payload: error
	});
};
