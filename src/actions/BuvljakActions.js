import {
	LISTING_NAME_CHANGED,
	LISTING_EMAIL_CHANGED,
	LISTING_MESSAGE_CHANGED,
	FETCH_LISTINGS_BEGIN,
	FETCH_LISTINGS_SUCCESS,
	FETCH_LISTINGS_FAIL,
	SEND_LISTING_BEGIN,
	SEND_LISTING_SUCCESS,
	SEND_LISTING_FAIL
} from './types';

export const listingNameChanged = (text) => {
	return {
		type: LISTING_NAME_CHANGED,
		payload: text
	};
};
export const listingEmailChanged = (text) => {
	return {
		type: LISTING_EMAIL_CHANGED,
		payload: text
	};
};
export const listingMessageChanged = (text) => {
	return {
		type: LISTING_MESSAGE_CHANGED,
		payload: text
	};
};

export const fetchListings = () => {
	return (dispatch) => {
		dispatch({ type: FETCH_LISTINGS_BEGIN });

		fetch(global.site_url+'/wp-json/wp/v2/posts?_embed&per_page=100', {
			method: 'GET'
		}).then((response) => response.json()).then((responseJson) => {
			console.log(responseJson);
			fetchListingsSuccess(dispatch, responseJson);
		}).catch(error => {
			fetchListingsFail(dispatch, responseJson);
		});
	};
};

const fetchListingsSuccess = (dispatch, response) => {
	console.log(response);
	dispatch({
		type: FETCH_LISTINGS_SUCCESS,
		payload: response
	});
};

const fetchListingsFail = (dispatch, error) => {
	dispatch({
		type: FETCH_LISTINGS_FAIL,
		payload: error
	});
};

export const sendListing = (name, email, message, images) => {
	return (dispatch) => {
		dispatch({ type: SEND_LISTING_BEGIN });

		var data = new FormData();
		data.append('name', name);
		data.append('email', email);
		data.append('message', message);

		if(images.length > 0) {
			var i = 1;
			images.map((image) => {
				data.append('image'+i, {
					uri: image,
					name: "image"+i+".jpg",
					type: "multipart/form-data"
				});

				i++;
			});
		}

		console.log(data);

		fetch(global.site_url+'/zeleni-api/listing.php', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data'
			},
			body: data
		}).then((response) => response.json()).then((responseJson) => {
			console.log(responseJson);
			sendListingSuccess(dispatch, responseJson);
		}).catch(error => {
			sendListingFail(dispatch, responseJson);
		});
	};
};

const sendListingSuccess = (dispatch, response) => {
	console.log(response);
	dispatch({
		type: SEND_LISTING_SUCCESS,
		payload: response
	});
};

const sendListingFail = (dispatch, error) => {
	dispatch({
		type: SEND_LISTING_FAIL,
		payload: error
	});
};
