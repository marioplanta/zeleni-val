import {
	REPORT_NAME_CHANGED,
	REPORT_EMAIL_CHANGED,
	REPORT_LOCATION_CHANGED,
	REPORT_MESSAGE_CHANGED,
	SEND_REPORT_BEGIN,
	SEND_REPORT_SUCCESS,
	SEND_REPORT_FAIL
} from './types';

export const reportNameChanged = (text) => {
	return {
		type: REPORT_NAME_CHANGED,
		payload: text
	};
};
export const reportEmailChanged = (text) => {
	return {
		type: REPORT_EMAIL_CHANGED,
		payload: text
	};
};
export const reportLocationChanged = (text) => {
	return {
		type: REPORT_LOCATION_CHANGED,
		payload: text
	};
};
export const reportMessageChanged = (text) => {
	return {
		type: REPORT_MESSAGE_CHANGED,
		payload: text
	};
};

export const sendReport = (name, email, location, message, images, latitude, longitude) => {
	return (dispatch) => {
		dispatch({ type: SEND_REPORT_BEGIN });

		var data = new FormData();
		data.append('name', name);
		data.append('email', email);
		data.append('location', location);
		data.append('message', message);
		data.append('latitude', latitude);
		data.append('longitude', longitude);

		if(images.length > 0) {
			var i = 1;
			images.map((image) => {
				data.append('image'+i, {
					uri: image,
					name: "image"+i+".jpg",
					type: "multipart/form-data"
				});

				i++;
			});
		}

		console.log(data);

		fetch(global.site_url+'/zeleni-api/report.php', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data'
			},
			body: data
		}).then((response) => response.json()).then((responseJson) => {
			console.log(responseJson);
			sendReportSuccess(dispatch, responseJson);
		}).catch(error => {
			sendReportFail(dispatch, responseJson);
		});
	};
};

const sendReportSuccess = (dispatch, response) => {
	console.log(response);
	dispatch({
		type: SEND_REPORT_SUCCESS,
		payload: response
	});
};

const sendReportFail = (dispatch, error) => {
	console.log(error);
	dispatch({
		type: SEND_REPORT_FAIL,
		payload: error
	});
};
