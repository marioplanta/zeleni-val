import {
	CONTACT_NAME_CHANGED,
	CONTACT_EMAIL_CHANGED,
	CONTACT_MESSAGE_CHANGED,
	SEND_MESSAGE_BEGIN,
	SEND_MESSAGE_SUCCESS,
	SEND_MESSAGE_FAIL
} from './types';

export const contactNameChanged = (text) => {
	return {
		type: CONTACT_NAME_CHANGED,
		payload: text
	};
};
export const contactEmailChanged = (text) => {
	return {
		type: CONTACT_EMAIL_CHANGED,
		payload: text
	};
};
export const contactMessageChanged = (text) => {
	return {
		type: CONTACT_MESSAGE_CHANGED,
		payload: text
	};
};

export const sendMessage = (name, email, message) => {
	return (dispatch) => {
		dispatch({ type: SEND_MESSAGE_BEGIN });

		var data = new FormData();
		data.append('your-name', name);
		data.append('your-email', email);
		data.append('your-subject', 'Upit s mobilne aplikacije Zeleni val');
		data.append('your-message', message);

		console.log(data);

		fetch(global.site_url+'/wp-json/contact-form-7/v1/contact-forms/7/feedback', {
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data'
			},
			body: data
		}).then((response) => response.json()).then((responseJson) => {
			sendMessageSuccess(dispatch, responseJson);
		}).catch(error => {
			sendMessageFail(dispatch, responseJson);
		});
	};
};

const sendMessageSuccess = (dispatch, response) => {
	dispatch({
		type: SEND_MESSAGE_SUCCESS,
		payload: response
	});
};

const sendMessageFail = (dispatch, error) => {
	console.log(error);
	dispatch({
		type: SEND_MESSAGE_FAIL,
		payload: error
	});
};
