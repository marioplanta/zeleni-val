import React, { Component } from 'react';

import Main from './Main';

class App extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Main />
		);
	}
}

export default App;
