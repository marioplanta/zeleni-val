import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Body,
	Title,
	Tabs,
	Tab,
	ScrollableTab,
	Left,
	Right,
	Button,
	Icon
} from "native-base";

import ReportLocationScreen from "./ZaKorisnike/ReportLocationScreen";
import WastePickupScreen from "./ZaKorisnike/WastePickupScreen";
import FleaMarketScreen from "./ZaKorisnike/FleaMarketScreen";

class ForUsersScreen extends Component {
	constructor(props) {
		super(props);

		this.state = {
			initialPage: 0
		};
	}

	componentDidMount() {
		if(this.props.navigation.getParam('tab') == 'Report') {
			setTimeout(() => {
				this.setState({
					initialPage: 0
				});
			}, 0);
		} else if(this.props.navigation.getParam('tab') == 'FleaMarket') {
			setTimeout(() => {
				this.setState({
					initialPage: 2
				});
			}, 0);
		} else if(this.props.navigation.getParam('tab') == 'WastePickup') {
			setTimeout(() => {
				this.setState({
					initialPage: 1
				});
			}, 0);
		}
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header style={{ backgroundColor: '#ffffff' }}>
					<Left>
						<Button transparent onPress={() => {
							// maybe use .goBack() ?
							this.props.navigation.navigate('HomeScreen');
						}}>
							<Icon name="menu" style={{ color: '#019147' }} />
						</Button>
					</Left>
					<Body>
						<Image source={ require('./../assets/images/logo.png') } style={{alignSelf: 'center', width: 200, height: 35, resizeMode: "contain"}} />
					</Body>
					<Right />
				</Header>
				<Tabs initialPage={this.state.initialPage} page={this.state.initialPage} tabBarUnderlineStyle={{ backgroundColor: '#019147' }} renderTabBar={() => <ScrollableTab style={{ backgroundColor: '#ffffff' }} />}>
					<Tab heading="Report illegal waste dump" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<ReportLocationScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Order waste pickup" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<WastePickupScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Flea market" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<FleaMarketScreen navigation={this.props.navigation} />
					</Tab>
				</Tabs>
			</Container>
		);
	}
}

export default ForUsersScreen;
