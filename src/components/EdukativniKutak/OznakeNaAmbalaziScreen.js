import React, { Component } from "react";
import {
	View,
	Text,
	Image
} from "react-native";
import {
	Content
} from 'native-base';

import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';

class OznakeNaAmbalaziScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Content padder>
				<Text style={globalStyles.paragraph}>Kako bi pravilno odvajali otpad potrebno je poznavati što znače oznake na ambalaži. Ako niste sigurni gdje ide koji otpad, potražite oznake koje svaka ambalaža mora imati na sebi.</Text>
				<Text style={globalStyles.paragraph}>Što znače simboli na ambalaži?</Text>
				<Text style={globalStyles.subtitle}>1. Univerzalni simbol recikliranja ili Mobiusova petlja</Text>
				<ResponsiveImageView source={ require('./../../assets/images/mobius.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={globalStyles.paragraph}>Simbol predstavlja Mobiusovu petlju koja sadrži tri povezane strelice u obliku trokuta sa zaobljenim kutevima. Sve tri se nadovezuju jedna na drugu i svaka predstavlja ciklus recikliranja. Ovaj simbol nije zaštićen i koristi se na razne načine i u raznim varijacijama, ali generalno se može reći da označava proizvode koji se mogu reciklirati. Ako je Mobiusova petlja u krugu, označava proizvode dobivene reciklažom, a ponekad se unutar petlje nalazi postotak koji označava udio recikliranog materijala u proizvodu.</Text>

				<Text style={globalStyles.subtitle}>2. Simbol RECIKLIRAJ! / Ambalaža za recikliranje</Text>
				<ResponsiveImageView source={ require('./../../assets/images/recikliraj.png') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={globalStyles.paragraph}>Osnovno značenje simbola RECIKLIRAJ! (eng. Recycle Now, Recycle Mark), kao što i njegov naziv govori – poziv je na akciju, odnosno apel svima da se reciklira u što većoj mogućoj mjeri. Simbol nalazimo na većini proizvoda koji su pogodni za recikliranje, od proizvoda prehrambene industrije, namještaja, najrazličitijih oblika ambalaže i slično. Kružni oblik simbola predstavlja samoodrživost procesa recikliranja, a sam simbol ima i varijaciju sa srcem koja ima isto značenje, ali i naglašava pozitivan učinak recikliranja na okolinu.</Text>

				<Text style={globalStyles.subtitle}>3. Sustav numeriranja i kratica za označavanje ambalaže</Text>
				<ResponsiveImageView source={ require('./../../assets/images/numeriranje.png') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={globalStyles.paragraph}>Princip numeriranja i kratica za označavanje ambalažnog materijala je vrlo jednostavan. Ispod simbola Mobiusove petlje nalazi se slovna skraćenica kemijskog spoja od kojeg je ambalaža proizvedena, dok se unutar petlje nalazi i brojčana oznaka. Kratice se pišu samo velikim slovima, a ako je unutar petlje samo broj bez kratice, radi se o kombinaciji materijala. Značenje svih kratica i brojeva možete jednostavno provjeriti na internetu.</Text>
				<Text style={globalStyles.paragraph}>Najvažnije je poznavati simbole za plastičnu ambalažu kojih imamo 7:</Text>
				<ResponsiveImageView source={ require('./../../assets/images/plastika2.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>

				<Text style={globalStyles.paragraph}>1.(PET ili PETE)</Text>
				<Text style={globalStyles.paragraph}>Ova vrsta plastike među najčešće je korištenom plastikom u proizvodnji namijenjenoj širokoj potrošnji. Većina bočica s vodom i gaziranih sokova te neka pakiranja napravljena su upravo od ove vrste plastike. Proizvodi napravljeni od PET plastike trebali bi se koristiti samo jednom jer se višekratnom uporabom povećava rizik od razvoja bakterija. Osim toga, ova plastika može ispuštati teške metale i kemikalije koje utječu na ravnotežu hormona.</Text>
				<Text style={globalStyles.paragraph}>2.(HDP ili HDPE)</Text>
				<Text style={globalStyles.paragraph}>Od ove krute plastike izrađuju su pakiranja i boce za mlijeko, ulje, tekuće deterdžente te igračke i neke plastične vrećice. S obzirom na to da ne ispušta skoro nikakve kemikalije najsigurnija je za uporabu te stručnjaci savjetuju kupovanje vode u bočicama od ove vrste plastike.</Text>
				<Text style={globalStyles.paragraph}>3.(PVC or 3V)</Text>
				<Text style={globalStyles.paragraph}>Prozirni celofani, boce od ulja, plastični krugovi za bebe kojima rastu zubi, igračke za djecu i kućne ljubimce, pakiranja od baterija, lijekova i mnoštva drugih proizvoda izrađuju se od ove vrste plastike. Sadrži ftalate – toksične kemikalije koji utječu na hormone u tijelu. Stručnjaci preporučuju kupovanje alternativnog proizvoda ako ga možete pronaći.</Text>
				<Text style={globalStyles.paragraph}>4.(LDPE)</Text>
				<Text style={globalStyles.paragraph}>Iako ne ispušta nikakve kemikalije u vodu, ova vrsta plastike ne može se koristiti u proizvodnji boca. Koristi se najčešće za proizvodnju plastičnih vrećica. Smatra se bezopasnom.</Text>
				<Text style={globalStyles.paragraph}>5.(PP)</Text>
				<Text style={globalStyles.paragraph}>Od ove bijelo obojane ili polu prozirne plastike rade se čašice za jogurt i sirupe. PP plastika je čvrsta i lagana te otporna na toplinu, a ako se zagrije neće se otopiti te je relativno sigurna za upotrebu. Također je dobar izolator protiv vlage, masnoće i kemikalija.</Text>
				<Text style={globalStyles.paragraph}>6.(PS)</Text>
				<Text style={globalStyles.paragraph}>Polistiren je jeftina, lagana vrsta plastike sa širokom lepezom uporabe. Najviše se koristi u izradi pakiranja za hranu i pića, kartona za jaja i plastičnog pribora za jelo. Treba biti pažljiv jer kada se ugrije ispušta kancerogene supstance te se ne smije dugotrajno koristiti za spremanje hrane ili pića.</Text>
				<Text style={globalStyles.paragraph}>7.(PC i sva druga plastika)</Text>
				<Text style={globalStyles.paragraph}>Riječ je o najopasnijoj vrsti plastike, ali unatoč tome od nje se proizvode pakiranja za sportska pića i hranu. Unutar ove kategorije ne postoji standardizacija o njenom ponovnom korištenju i recikliranju. Kod ove vrste plastike postoji velika opasnost od curenja kemikalija u hranu ili piće, naročito ako je u izradi plastike korišten Bisfenol A.</Text>

				<Text style={globalStyles.subtitle}>Izbjegavajte ambalažu s oznakama 1,3,6 i 7 !! A za najbolji učinak na okoliš i zdravlje, pokušajte izbjegavati plastičnu ambalažu i proizvode od plastike u potpunosti.</Text>
				<Text style={globalStyles.paragraph}>Simboli na papirnoj i kartonskoj ambalaži:</Text>
				<ResponsiveImageView source={ require('./../../assets/images/papir1.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraphNoMargin, { marginTop: 20 } ]}>20 – Kartonska ambalaža – uglavnom se koristi za kartonske kutije</Text>
				<Text style={globalStyles.paragraphNoMargin}>21 – Razne vrste papira – novine, časopisi, katalozi (ravan karton)</Text>
				<Text style={globalStyles.paragraphNoMargin}>22 – Uredski papir, knjige</Text>
				<Text style={globalStyles.paragraph}>23 – Karton (ambalaža za nesmrznutu hranu, čestitke, korice knjiga)</Text>
				<Text style={globalStyles.paragraph}>Brojčana oznaka i skraćenica za metale</Text>
				<Text style={globalStyles.paragraphNoMargin}>40 – Čelik (FE)</Text>
				<Text style={globalStyles.paragraph}>41– Aluminij (ALU)</Text>
				<Text style={globalStyles.paragraph}>Brojčana oznaka i skraćenica za drvene materijale</Text>
				<Text style={globalStyles.paragraphNoMargin}>50 – Drvo (FOR)</Text>
				<Text style={globalStyles.paragraph}>51 – Pluta (FOR)</Text>
				<Text style={globalStyles.paragraph}>Brojčana oznaka i skraćenica za tekstilne materijale</Text>
				<Text style={globalStyles.paragraphNoMargin}>60 – Pamuk (TEX)</Text>
				<Text style={globalStyles.paragraph}>61 – Juta (TEX)</Text>
				<Text style={globalStyles.paragraph}>Brojčana oznaka i skraćenica za staklo</Text>
				<Text style={globalStyles.paragraphNoMargin}>70 – Bezbojno staklo (GL)</Text>
				<Text style={globalStyles.paragraphNoMargin}>71 – Zeleno staklo (GL)</Text>
				<Text style={globalStyles.paragraph}>72 – Smeđe staklo (GL)</Text>
				<Text style={globalStyles.paragraph}>Brojčane oznake na višeslojnoj ambalaži – ambalaža koja se dominantno sastoji od papira i kartona, ali se ne može ručno rastaviti na sastavne materijale, JAKO SE TEŠKO ILI NIKAKO RECIKLIRA!</Text>
				<Text style={globalStyles.paragraphNoMargin}>80 – Papir i karton u kombinaciji s metalima</Text>
				<Text style={globalStyles.paragraphNoMargin}>81 – Papir i karton u kombinaciji s plastikom – kartonski tanjuri, ambalaža za sladoled</Text>
				<Text style={globalStyles.paragraphNoMargin}>82 – Papir i karton u kombinaciji s aluminijem</Text>
				<Text style={globalStyles.paragraphNoMargin}>83 – Papir i karton u kombinaciji s bijelim limom</Text>
				<Text style={globalStyles.paragraphNoMargin}>84 – Papir i karton u kombinaciji s plastikom i aluminijem– tetrapak, mlijeko, sokovi</Text>
				<Text style={globalStyles.paragraph}>85 – Papir i karton u kombinaciji s plastikom, bijelim limom i aluminijem</Text>
				<Text style={globalStyles.paragraph}>Brojčane oznake na višeslojnoj ambalaži – ambalaža koja se dominantno sastoji od plastike i stakla, ali se ne može ručno rastaviti na sastavne materijale</Text>
				<Text style={globalStyles.paragraphNoMargin}>90 – Plastika/aluminij</Text>
				<Text style={globalStyles.paragraphNoMargin}>91 – Plastika/bijeli lim</Text>
				<Text style={globalStyles.paragraphNoMargin}>92 – Plastika/raznovrsni metali</Text>
				<Text style={globalStyles.paragraphNoMargin}>95 – Staklo/plastika</Text>
				<Text style={globalStyles.paragraphNoMargin}>96 – Staklo/aluminij</Text>
				<Text style={globalStyles.paragraphNoMargin}>97 – Staklo/bijeli lim</Text>
				<Text style={globalStyles.paragraphNoMargin}>98 – Staklo/raznovrsni metali</Text>

				<Text style={globalStyles.subtitle}>4. Zelena točka</Text>
				<View style={{ flex: 1, flexDirection: 'row', marginBottom: 15 }}>
					<Image
						style={{width: 70, height: 70}}
						source={ require('./../../assets/images/zelena.png') }
					/>
					<Text style={{ flex: 1, paddingHorizontal: 15, fontSize: 16 }}>U hrvatskoj je preko 80 % komunalnog otpada završavalo na odlagalištima čime je u samo 10 godina izgubljeno sirovina u vrijednosti preko 5 milijardi kuna.</Text>
				</View>

				<Text style={globalStyles.subtitle}>5. Ne zagađuj okoliš / Čuvajmo prirodu</Text>
				<View style={{ flex: 1, flexDirection: 'row', marginBottom: 15 }}>
					<Image
						style={{width: 70, height: 70}}
						source={ require('./../../assets/images/nezagadjuj.png') }
					/>
					<Text style={{ flex: 1, paddingHorizontal: 15, fontSize: 16 }}>Originalni naziv ovog simbola je engleski “THE TIDYMAN”, što u slobodnom prijevodu označava osobu koja brine o okolišu, odnosno osobu koja ne zagađuje okoliš time što otpatke baca u za to predviđena mjesta. Simbol predstavlja podsjetnik da vlastite otpatke odlažemo u za to predviđena mjesta (kante za otpatke, kontejnere i sl.). Važno je naglasiti da značenje simbola nije izravno povezano s činom recikliranja, ali zasigurno potiče savjesno ponašanje prema odlaganju otpadaka, odnosno očuvanju i zaštiti okoliša.</Text>
				</View>

				<Text style={globalStyles.subtitle}>6. FSC (Forest Stewardship Council)</Text>
				<View style={{ flex: 1, flexDirection: 'row', marginBottom: 15 }}>
					<Image
						style={{width: 70, height: 70}}
						source={ require('./../../assets/images/fsc.jpg') }
					/>
					<Text style={{ flex: 1, paddingHorizontal: 15, fontSize: 16 }}>FSC je Vijeće za upravljanje šumama. To je neovisna, nevladina i neprofitna organizacija uspostavljena radi promoviranja odgovornog upravljanja svjetskim šumama. Ako neki proizvod, najčešće proizvodi izrađeni od drva, sadrže simbol FSC, to znači da su drvena građa kao i proizvođač u skladu s principima FSC te da je sam proizvođač svjestan važnosti promicanja ekološki odgovornog, društveno korisnog i ekonomski održivog gospodarenja svjetskim šumskim resursima. Kupnjom proizvoda s oznakom FSC direktno potičete odgovorno gospodarenje šumama te pridonosite očuvanje šumskih resursa diljem planeta.</Text>
				</View>

				<Text style={globalStyles.subtitle}>7. Odvojeno sakupljanje otpada</Text>
				<View style={{ flex: 1, flexDirection: 'row', marginBottom: 15 }}>
					<Image
						style={{width: 70, height: 70}}
						source={ require('./../../assets/images/odvojeno.png') }
					/>
					<Text style={{ flex: 1, paddingHorizontal: 15, fontSize: 16 }}>Simbol precrtane kante za smeće na proizvodu, bateriji ili ambalažnom materijalu označava da se svi električni i elektronički proizvodi, baterije i akumulatori nakon isteka vijeka trajanja moraju odložiti na odvojeno odlagalište. Ne bacajte proizvode s navedenom oznakom kao nerazvrstani gradski otpad, već ih odlažite na za to propisana mjesta! Ako je ispod oznake otisnut kemijski simbol, to naznačuje da navedeni proizvod sadrži teški metal (živu, kadmij ili olovo) u koncentraciji iznad prihvatljive granice.</Text>
				</View>
			</Content>
		);
	}
}

export default OznakeNaAmbalaziScreen;
