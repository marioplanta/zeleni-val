import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Content,
	Body,
	Icon,
	Left,
	Right,
	Title,
	Text,
	Button,
} from "native-base";
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';

class StakloScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" style={globalStyles.backButtonIcon} />
							<Text style={globalStyles.backButtonText}>Natrag</Text>
						</Button>
					</Left>
					<Body>
						<Title>Staklo</Title>
					</Body>
					<Right/>
				</Header>
				<Content padder>
					<ResponsiveImageView source={ require('./../../assets/images/staklo.jpg') }>
						{({ getViewProps, getImageProps }) => (
							<View {...getViewProps()}>
								<Image {...getImageProps()} />
							</View>
						)}
					</ResponsiveImageView>
					<Text style={globalStyles.paragraph}>U recikliranje stakla ubraja se u prvom redu staklena ambalaža, tj. boce za piće i hranu. Drugi stakleni prozivodi se ne miješaju i ne recikliraju zajedno sa staklenom ambalažom, npr. zrcalo, čaše, prozorsko staklo, rasvjetna tijela. Potrošači odlažu staklenu ambalažu u specijalne spremnike za staklo ili predaju najbližim otkupljivačima stakla. Prikupljena staklena ambalaža se odvozi u određena mjesta za recikliranje stakla. Staklenu ambalažu radnici stavljaju na pokretnu traku čime recikliranje započinje.</Text>
					<Text style={globalStyles.paragraph}>Kako je staklo materijal koji se može u potpunosti preraditi i to bezbroj puta, treba prikupiti što veće količine starih staklenki i boca i vratiti ih u tvornicu stakla jer time:</Text>
					<Text style={globalStyles.paragraphNoMargin}>• štedimo prirodne sirovine (upotrebom 1000 kg starog stakla uštedi se 700 kg pijeska, 200 kg kalcita, 200 kg sode),</Text>
					<Text style={globalStyles.paragraphNoMargin}>• štedimo energiju (trošak energije pada za 2-3 posto za svakih 10 posto udjela starog stakla u smjesi),</Text>
					<Text style={globalStyles.paragraphNoMargin}>• recikliranjem jedne boce uštedjet ćemo toliko energije koliko je potrebno žarulji od 60 W da svijetli 4 sata, računalu da radi 30 minuta, a televizor 20 minuta,</Text>
					<Text style={globalStyles.paragraphNoMargin}>• korištenjem starog stakla smanjujemo potrošnju primarnih sirovina i produljujemo životni vijek staklarske peći,</Text>
					<Text style={globalStyles.paragraphNoMargin}>• smanjujemo onečišćenje okoliša,</Text>
					<Text style={globalStyles.paragraph}>• štedimo prostor na odlagalištima otpada.</Text>
					<Text style={globalStyles.paragraph}>Proces recikliranja stakla je proces pretvaranja odbačenog stakla u korisni proizvod. Ovisno o konačnoj upotrebi, ovo često uključuje razdvajanje stakla prema boji. Staklo dolazi u različitim bojama, ali tri najčešće su: prozirno, zeleno, smeđe.</Text>
					<Text style={globalStyles.paragraph}>Staklo čini veliki dio kućnog i industrijskog otpada prema svojoj težini i gustoći. Stakleni otpad u gradskom otpadu se sastoji od staklenih boca, staklene robe i posuđa, žarulja i drugih stvari.</Text>
					<Text style={globalStyles.paragraph}>Recikliranje stakla troši manje energije nego njegova proizvodnja od pijeska, vapna i sode. Svaka tona stakla iskorištena za proizvodnju novog stakla sačuva oko 315 kg ispuštenog ugljičnog dioksida.</Text>
				</Content>
			</Container>
		);
	}
}

export default StakloScreen;
