import React, { Component } from "react";
import {
	View,
	Text,
	FlatList
} from "react-native";
import {
	Container,
	Content,
	ListItem,
	Left,
	Right,
	Icon
} from 'native-base';

const screens = [
	{
		id: '0',
		title: "Papir",
		screen: "PapirScreen"
	},
	{
		id: '1',
		title: "Plastika",
		screen: "PlastikaScreen"
	},
	{
		id: '2',
		title: "Staklo",
		screen: "StakloScreen"
	},
	{
		id: '3',
		title: "Metal",
		screen: "MetalScreen"
	}
];

class RecikliranjeScreen extends Component {
	constructor(props) {
		super(props);
	}

	renderItem = ({ item }) => {
		return (
			<ListItem onPress={() => this.props.navigation.navigate(item.screen)}>
				<Left>
					<Text>
						{item.title}
					</Text>
				</Left>
				<Right>
					<Icon name="arrow-forward" />
				</Right>
			</ListItem>
		);
	};

	render() {
		const { textStyle } = styles;

		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Content>
					<Text style={styles.textStyle}>Nakon što odbijemo ono što nam ne treba, reduciramo kupovinu na ono što nam je stvarno potrebno i pokušamo ponovo upotrijebiti ono što smo već unijeli u naš dom, slijedi recikliranje najvećeg dijela preostalog otpada. Recikliranjem se štedi energija jer ju je potrebno uložiti znatno manje nego za novi proizvod, čuvaju se prirodni resursi, smanjuje se količina otpada na odlagalištima, a time i štetne emisije stakleničkih plinova i zagađenje voda. Usprkos tome, nije dobro shvatiti recikliranje kao krajnje rješenje problema otpada jer je sam proces skup, zahtijeva dodatne energetske resurse te se u procesu odvojenog prikupljanja otpada kao i u proizvodnji dodatno zagađuje okoliš. Osim toga, recikliranje plastike zbog brojnih razloga predstavlja veliki problem. Više informacija o recikliranju na sljedećim poveznicama:</Text>

					<FlatList
						data={screens}
						renderItem={this.renderItem}
						keyExtractor={item => item.id}
					/>
				</Content>
			</Container>
		);
	}
}

const styles = {
	textStyle: {
		fontSize: 16,
		padding: 15
	}
};

export default RecikliranjeScreen;
