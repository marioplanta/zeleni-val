import React, { Component } from "react";
import {
	View,
	Text,
	Image
} from "react-native";
import {
	Content
} from 'native-base';
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';

class SprjecavanjeNastankaOtpadaScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { textStyle } = styles;

		return (
			<Content padder>
				<Text style={globalStyles.paragraph}>Kada govorimo o gospodarenju otpadom, najčešće čujemo pojmove “recikliranje otpada” i “odlaganje otpada”. Mnogi misle da je „recikliranje“ prvo na što se trebamo usmjeriti. Međutim, realnost je puno drugačija. Mnoge vrste otpada se ne mogu uopće reciklirati, a većinu još uvijek nije isplativo reciklirati. Jeste li znali da je samo 9% svjetskog plastičnog otpada reciklirano do sada? Mali dio je spaljen, a većina je završila na odlagalištima i u oceanima. Zbog toga recikliranje nije rješenje za sve. Najpoželjniji, najjednostavniji i najjeftinij način gospodarenja otpadom je uopće ne stvarati otpad. Mislite da to nije moguće? Moguće je, i sve više ljudi se odlučuje na ovakav način života koji nazivamo „zero waste“ ili nula otpada.</Text>
				<Text style={globalStyles.subtitle}>Što je „zero waste“?</Text>
				<Text style={globalStyles.paragraph}>Jednostavan odgovor: Zero waste znači nula otpada. Pokret potiče ponovnu upotrebu stvari ili njihovu prenamjenu u druge svrhe. Također potiče kompostiranje i odlaganje što manje stvari za reciklažu. Ovaj pokret se zalaže za kupovinu stvari koje su nužno potrebne, organske ili zapakirane u minimalno ambalaže.</Text>
				<Text style={globalStyles.paragraph}>Komplicirani odgovor: Zero waste radi na redefiniranju sustava. Trenutačni sustav zbrinjavanja otpada nije održiv i zero waste tu nudi rješenje za budućnost eko sustava. Suvremeni sustav počiva na konzumaciji velike količine resursa gdje se višak samo gomila na odlagalištima. Cilj ovog pokreta je stvoriti kružni ciklus u kojem se otpad eliminira iz postojanja. Cirkularno gospodarstvo imitira prirodu jer u prirodi nema smeća.</Text>
				<Text style={globalStyles.paragraph}>Zero waste naglašava prevenciju otpada. Ovaj pristup ima za cilj promjenu načina na koji materijali prolaze kroz društvo. Zero waste se ne fokusira samo na reduciranje, ponovnu upotrebu i reciklažu, on je također usredotočen na restrukturiranje proizvodnih i distribucijskih sustava za smanjenje otpada te promiče i ideju redizajna proizvoda s minimalno otpadne ambalaže. Zero waste podupire održivost štiteći okoliš, smanjujući troškove i stvarajući dodatne poslove u upravljanju i zbrinjavanju otpada natrag u sustav. Strategija zero waste se može primijeniti na kompanije, zajednice, industrijske sektore, škole, domove.</Text>
				<Text style={globalStyles.paragraph}>Evo i nekoliko prednosti zero waste strategije koje zagovornici pokreta naglašavaju:</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Ušteda novca: Budući da je otpad znak neučinkovitosti, smanjenje otpada može smanjiti troškove.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Brži napredak: Strategija nula otpada poboljšava proizvodne procese i zaštitu okoliša koji mogu dovesti do različitih inovacija.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Poboljšani tokovi materijala. Strategija zero waste bi koristila daleko manje novih sirovina. Bilo koji materijalni otpad bi se vratio u sustav kao reciklirani materijal ili bi bio prikladan za uporabu kao kompost.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Zdravlje: sprečava dodatno zagađenje te štiti zdravlje ljudi i čuva ekosustav.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Umjesto odbacivanja resursa, zero waste je koncept u kojem se svi resursi mogu vratiti u sustav!</Text>
				<Text style={globalStyles.paragraph}>Zašto ne možemo zauvijek ovisiti o odlagalištima otpada?</Text>
				<Text style={globalStyles.paragraph}>Živimo u bogatom društvu u kojemu ne cijenimo svoje stvari i trošimo previše resursa. Nepotrebni materijali završavaju na odlagalištima koja rastu iz godine u godinu.</Text>
				<Text style={globalStyles.paragraph}>Mnogo smeća uopće ne završi na predviđenim lokacijama. Umjesto toga, se skuplja na divljim odlagalištima, ili još gore, završava u morima i oceanima. Plastika je pogotovo opasna za oceane jer nije biorazgradiva i uništava morski život, ali i naše zdravlje.</Text>
				<Text style={globalStyles.paragraph}>Zero waste i reciklaža: Možemo li jednostavno reciklirati sav otpad? Nažalost ne.</Text>
				<Text style={globalStyles.paragraph}>Zbog pretjerane ljudske konzumacije ga ima previše za obradu. Recikliranje nije savršeno rješenje, ono je jedan mali dio rješenja. Glavni problem je kako smanjiti našu ovisnost o stvarima. Postoji razlog zašto se recikliraj nalazi na posljednjem mjestu u poznatom sloganu: „Reduciraj, ponovno upotrebi, recikliraj“. Recikliranje ne smije biti prva linija obrane nego posljednje sredstvo. Zero waste zagovornici nisu protiv recikliranja, samo smatraju kako bi trebali manje ovisiti o njemu kao konačnom rješenju problema zagađenja okoliša. Zagovornici zero waste strategije u fokus stavljaju reduciranje otpada i ponovnu upotrebu materijala.</Text>
				<Text style={globalStyles.paragraph}>Zero waste lekcija: Što se može ponovno upotrijebiti, a što možemo reducirati?</Text>
				<Text style={globalStyles.paragraph}>Pogledajte stvari u svom domu i zapitajte se je li vam zbilja sve to treba. Ne trebate kupovati sve što se nudi u trgovinama. Na primjer, ne trebate šest različitih proizvoda za pranje i čišćenje podova, kupaonice ili prozora. Ne treba vam osam crvenih majica. Zero waste se zalaže za izbacivanje suvišnih stvari iz života i prebacuje fokus na nabavu onoga što je zbilja nužno. S tom strategijom ćemo na kraju smanjiti i količinu stvari koje bacamo. Kada se oslobodite materijalnog u vašem životu, prodisat ćete u duhovnom. Manje stvari znači manje obaveza, manje čišćenja i manje briga…</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Način života „Zero waste“</Text>
				<Text style={globalStyles.paragraph}>Principi:</Text>
				<Text style={globalStyles.subtitle}>1. Sva se hrana kupuje u rinfuzi</Text>
				<Text style={globalStyles.paragraph}>Zero waste zagovornici nose vlastite platnene vrećice i staklenke od kuće, a hranu kupuju u rinfuzi, te na taj način ne stvaraju otpad od ambalaže od hrane. Ujedno se i zdravo hrane, jer ne kupuju konzerviranu i procesuiranu hranu, već svježe i domaće proizvode.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/bag.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.subtitle, { marginTop: 20 } ]}>2. Proizvode se sredstva za čišćenje kućne radinosti od ostataka hrane</Text>
				<Text style={globalStyles.paragraph}>Umjesto da kupujete desetak različitih sredstava za čišćenje, postoje razni recepti za proizvodnju domaćih sredstava za čišćenje koji nisu štetni, otrovni i nagrizajući. Sredstva se proizvode od prirodnih sastojaka.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/sredstvo.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Recept za sredstvo za čišćenje od narančine kore:</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>1. kora od 4 naranče</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>2. jedna boca bijelog (alkoholnog) octa</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>3. Eterično ulje po izboru</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>4. jedna velika staklenka (npr. od kiselih krastavaca)</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>5. Cjedilo</Text>
				<Text style={globalStyles.paragraph}>6. Raspršivač ( npr. prazna ambalaža od sredstva za čišćenje stakla)</Text>

				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Upute:</Text>
				<Text style={globalStyles.paragraph}>Naranče pojedite, kore stavite u staklenku i ispunite je do kraja alkoholnim octom te zatvorite. Spremite staklenku tako da nije izložena sunčevim zrakama minimalno 3 tjedna da kore fermentiraju. Što duže stoje, sredstvo će biti jače. Nakon 3 tjedna procijedite tekućinu. Pomiješajte tekućinu iz staklenke s vodom u omjeru 50/50. Dodajte 20-30 kapi eteričnog ulja po izbora radi mirisa. Ulijte u raspršivač i sredstvo je spremno za korištenje. Narančine kore kompostirajte 😊</Text>
				<ResponsiveImageView source={ require('./../../assets/images/kompost.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0, marginTop: 15 } ]}>S ovim sredstvom možete očistiti sve površine u kućanstvu: kuhinjske površine, štednjake, cijelu kupaonic, čak i podove-jednostavno razrijedite sredstvo u vodi i prebrišite podove.</Text>
				<Text style={globalStyles.paragraph}>Idealno rješenje- niste potrošili novce na skupa sredstva za čišćenje koja su ujedno otrovna, niste stvorili otpad od ambalaže od tih proizvoda, a dobili ste prirodno univerzalno sredstvo koje čisti cijelu kuću.</Text>
				
				<Text style={globalStyles.subtitle}>3. Zero waste kupaonica</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>U kupaonici modernog doba većina je proizvoda plastična ili u plastičnoj ambalaži.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>U „zero waste“ kupaonici koristi se četkica od bambusa koja se nakon upotrebe može kompostirati, kao i četka za kosu i češljevi.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Umjesto šampona u plastičnim bocama, zero waste zagovornici prave prirodne šampone, ili kupuju tvrde šampone koji su u papirnoj ambalaži koja se lako može reciklirati.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Prave prirodnu pastu za zube, dezodorans ili biraju kozmetiku u papirnoj, metalnoj ili staklenoj ambalaži. Sve osim jednokratne plastike.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Koriste dugotrajne britvice.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Ne koriste plastične štapiće za uši, već drvene ili od bambusa.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Umjesto plastičnih uložaka, koriste menstrualnu čašicu ili platnene uloške koji se peru nakon upotrebe.</Text>
				<Text style={globalStyles.paragraph}>Koriste tvrdi sapun.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/kupaonica.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>

				<Text style={globalStyles.subtitle}>4. Zero waste hladnjak</Text>
				<Text style={globalStyles.paragraph}>Za pohranu hrane koriste se platnene vrećice i staklene vakumirane posude. Osim što je hladnjak apsolutno bez štetne plastike koja otpušta štetne spojeve u hranu, hrana će trajati duže i manje će se bacati.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/hladnjak.png') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>

				<Text style={globalStyles.subtitle}>5. Kompostiranje hrane</Text>
				<Text style={globalStyles.paragraph}>Kao prvo, „zero waste“ zagovornici paze da ne bacaju hranu. Čuvaju je u vakuumiranim staklenkama, i planski je pripremaju bez višaka. Ako ne iskoriste ostatke za proizvodnju prirodne kozmetike ili prirodnih sredstava za čišćenje, ostatke hrane kompostiraju.</Text>
				
				{/* FALI VIDEO */}

				<Text style={globalStyles.paragraph}>Zero waste zagovornici proizvode 1 staklenku otpada mjesečno!! Zamislite da svi proizvodimo samo toliko, svi bi problemi otpada bili zauvijek riješeni!!</Text>
				<ResponsiveImageView source={ require('./../../assets/images/kako-smanjiti.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>

				<Text style={globalStyles.subtitle}>Kako smanjiti količine otpada?</Text>
				<Text style={globalStyles.paragraph}>Nisu svi spremni oduprijeti se čarima „novoga“ i svega što nam nude trgovci, te se prepustiti „zero waste“ načinu života. Ali svi možemo napraviti male promjene kojima se zapravo ničega ne odričemo, a uvelike smanjujemo količine otpada koje nastaju.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Evo savjeta kako možete smanjiti količine otpada bez velikog odricanja:</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>1. Platnena vrećica- umjesto da svaki put u trgovini uzimate novu plastičnu vrećicu, nosite sa sobom platnenu.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>2. Staklena ili metalna boca- zaboravite na kupovanje nove boce vode svaki put kad ožednite. Nosite sa sobom staklenu ili metalnu bocu koju možete koristiti godinama.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>3. Staklenke- umjesto da hranu čuvate u plastičnim posudama, radije odaberite staklene teglice.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>4. Kuhinjske krpe- ne trošite novac na papirnate ručnike. Nabavite nekoliko kuhinjskih krpi kojima je dovoljno pranje i spremne su za ponovnu upotrebu.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>5. Šalica za kavu- umjesto da svaki dan bacite 2-3 plastične čaše od kave u smeće, nabavite putnu šalicu za kavu koja će vam trajati godinama.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>6. Ne koristite slamke, štapiće za uši i jednokratni plastični pribor za jelo. Ako već morate, koristite njihove metalne ili drvene alternative.</Text>
				<Text style={globalStyles.paragraph}>7. Kupujte proizvode na tržnici ili u rinfuzi</Text>
				<ResponsiveImageView source={ require('./../../assets/images/zero.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>

				<Text style={globalStyles.subtitle}>Mislite da je „zero waste“ naporan i da vam je život prepun obaveza da radite još i to?</Text>
				<Text style={globalStyles.paragraph}>Uz malo truda puno stvari se može promijeniti. Pristaše zero wastea smatraju da ova filozofija nosi brojne benefite za okoliš, ali i za kvalitetu života svakog pojedinca. Nuspojave zero wastea su bolja prehrana, bolje zdravlje, ušteda novca i manje smeća!</Text>
				
				<Text style={globalStyles.subtitle}>Koliko traje prelazak na zero waste sustav?</Text>
				<Text style={globalStyles.paragraph}>Živimo u instant društvu u kojem želimo rezultate našeg rada vidjeti odmah. Kod zero wastea ne možete očekivati promjene preko noći. Bitno je krenuti malim koracima i biti strpljiv.</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Ako vas zanima zero waste, evo nekoliko savjeta za početak vaše bezotpadne avanture:</Text>
				<Text style={globalStyles.paragraph}>Pronađite svoje ZAŠTO</Text>
				<Text style={globalStyles.paragraph}>Da biste se zbilja držali zero waste koncepta morate pronaći osobni razlog zbog čega vam je ova filozofija važna. Ako želite istražiti život bez otpada mora postojati „zašto“ iza onoga što radite. Je li vas živciraju one plastične vrećice pobacane u obližnjem parku? Imate alergije na određene proizvode? Zabrinuti ste zbog klimatskih promjena? Vaši razlozi moraju biti osobni tako da imate unutarnji motiv kako biste bili nepokolebljivi u svom cilju. Zapišite svoje zašto i zalijepite ga negdje na vidljivom mjestu u svom domu.</Text>
				<Text style={globalStyles.paragraph}>Donirajte, poklonite ili prodajte stare stvari</Text>
				
				<Text style={globalStyles.subtitle}>Vaš cilj je stvaranje što manje otpada, stoga nemojte odmah bacati stare stvari u smeće!</Text>
				<Text style={globalStyles.paragraph}>Pronađite način da ponovno upotrijebite stari predmet ili materijal kad god možete. Kompostirajte kad god možete. Reciklirajte što god možete. Odjeću ili predmete koje ne koristite poklonite prijateljima ili donirajte. Cilj zero waste pokreta je sprječavanje materijala da završe na odlagalištima.</Text>
			
				<Text style={globalStyles.subtitle}>ZAPAMTITE: Zero waste je proces!</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Preobražaj na zero waste lifestyle se neće dogoditi preko noći…Proces će vjerojatno potrajati godinu ili dvije dok se u potpunosti ne prilagodite bezotpadnom životu. Svatko na drugačiji način proživljava ovu transformaciju. Važno je da se ne uspoređujete s drugima, nego se inspirirate njihovim pričama. Povežite se sa svojom Zero waste zajednicom i zajedno podržavajte svoje napore da živite bez otpada.</Text>
			</Content>
		);
	}
}

const styles = {
	textStyle: {
		fontSize: 16
	}
};

export default SprjecavanjeNastankaOtpadaScreen;
