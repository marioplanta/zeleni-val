import React, { Component } from "react";
import {
	View,
	Image,
	Button,
	Linking
} from "react-native";
import {
	Content,
	Text,
} from "native-base";
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';

class KompostiranjeScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Content padder>
				<Text style={globalStyles.paragraph}>Kompostiranje je proces biološke razgradnje organskog dijela otpada i spontano se događa u prirodi. Procesom kompostiranja rješava se problem biološkog otpada, a kao krajnji rezultat dobivamo kompost koji sadrži humus i druge korisne tvari i toliko je blagotvoran u povrtlarstvu da ga se naziva i “crno zlato”. Organski otpad u hrvatskim kućanstvima čini oko 30% pa njegovim izdvajanjem za jednu trećinu smanjujemo količinu otpada na odlagalištima. Osim toga, sprečavamo nastajanje stakleničkih plinova, osobito metana, plina koji je višestruko štetniji od ugljičnog dioksida i doprinosi efektu staklenika, a nastaje kao nusprodukt procesa anaerobnog raspadanja organskog otpada. Korištenjem humusa nastalog u procesu kompostiranja smanjujemo potrebu za korištenjem umjetnih mineralnih gnojiva koja također mogu biti štetna za okoliš. Kompostiranje je savršen primjer cirkularne ekonomije koju bismo trebali primjenjivati i za sve ostale vrste otpada.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/kompost1.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Kompostiranje je pametan način korištenja ostataka poslije košnje, rezidbe, skupljanja lišća i drugih aktivnosti u dvorištu ili vrtu, jer se od onoga što je potencijalno bio otpad dobiva besplatan i vrlo kvalitetan oplemenjivač zemljišta.</Text>
				<Text style={globalStyles.paragraph}>Bazira se na aktivnosti bakterija, koje razlažu materijal uz istovremenu produkciju topline, koja može biti viša ili niža, ovisno o početnom materijalu. Krajnji cilj je kompost, bogat hranjivim česticama, mikro i makro elementima, koji poboljšavaju kvalitetu zemljišta. Najčešće tvari koji se mogu kompostirati su sljedeće: biootpad bogat ugljikom (pokošenu travu, uvelo cvijeće, korov, sitno granje, lišće), te biootpad bogat dušikom (sirove ostatke povrća, talog kave, vrećice čaja, ljuske od jaja, kora od agruma). Dok s druge strane tvari koje se ne mogu kompostirati su osjemenjene korove, lišće oraha, bolesne biljke, otpatke kuhanih jela (jer privlače glodavce), pelene, velike količine novinskog papira, časopise u boji, sadržaj vrećica iz usisavača. Nikako ne smijemo kompostirati otpatke koji sadrže kemikalije.</Text>
				<Text style={globalStyles.paragraph}>Cijeli proces kompostiranja se može svesti na 6 koraka:</Text>
				<Text style={globalStyles.paragraph}>1. Materijal se usitni u što manju veličinu kako bi se lakše razgradio</Text>
				<Text style={globalStyles.paragraph}>2. U jednakom omjeru pomiješamo biootpad bogat dušikom (ostaci povrća i voća, talog kave i čaja, trava…) i biootpad bogat ugljikom (suho lišće, granje, slama, piljevina, iglice četinjača). Biootpad bogat dušikom brže se razgrađuje i osigurava vlagu dok se biootpad bogat ugljikom sporije razgrađuje i osigurava prozračnost.</Text>
				<Text style={globalStyles.paragraph}>3. Materijal stavimo na hrpu koju možemo i dodatno ograditi žicom, drvetom ili ciglom (na dno hrpe poželjno je staviti sloj granja da se osigura prozračivanje). Kompostnu hrpu zaštititi od prejakog sunca i oborina.</Text>
				<Text style={globalStyles.paragraph}>4. Hrpu barem jednom mjesečno preokrenuti kako bi osigurali prozračivanje (nikad ju ne zbijati!)</Text>
				<Text style={globalStyles.paragraph}>5. Vlažnost kompostne hrpe provjerimo metodom “knedle”. Uzeti šaku kompostnog materijala i lagano ga stisnuti. Ako iz šake curi tekućina, previše je vode. Ako se u stisnutoj šaci ne osjeća vlažnost, voda nedostaje. Kada materijal u šaci ostaje zbijen u grudi, vlažnost je primjerena.</Text>
				<Text style={globalStyles.paragraph}>6. Kada kompost postane rastresit, tamne boje i poprimi specifičan miris “šumske zemlje”, umješajmo ga u zemlju svog vrtnog ili kućnog bilja (otprilike nakon 9 mjeseci).</Text>
				<Text style={globalStyles.subtitle}>Komposteri</Text>
				<Text style={globalStyles.paragraph}>Postoje plastični, klasični komposteri no imamo i drvene i kompostere izrađene od žice. Drveni i žičani sanduci za kompostiranje, zahvaljujući svojoj rešetkastoj strukturi, gnojivu osigurava dobru cirkulaciju zraka, što pretvara organski otpad u iskoristivo i hranjivo gnojivo.</Text>
				<Text style={[ globalStyles.paragraph, { fontWeight: 'bold' } ]}>Drveni komposter</Text>
				<ResponsiveImageView source={ require('./../../assets/images/kompost2.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginTop: 20, fontWeight: 'bold' } ]}>Žičani komposter</Text>
				<ResponsiveImageView source={ require('./../../assets/images/kompost3.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginTop: 20 } ]}>Zbog prirodnih procesa, kao i protoka vlage, komposter nema dno jer biootpad koji se nalazi u komposteru treba da bude u kontaktu sa zemljom. Kod plastičnih kompostera, većinom se nalaze dva otvora, gornja vrata koja omogućuju jednostavno nasipanje komposta i donja vrata koja omogućuju olakšano povlačenje. Komposteru treba da bude osiguran dobar protok vjetra i da bude postavljen u polusjeni.</Text>
				<Text style={globalStyles.paragraph}>Možete nabaviti kupovni komposter, kojih ima u raznim bojama, oblicima i veličinama, a i cijene im variraju. Na taj način možete odabrati pravi model koji najviše odgovara vašim potrebama. I konačno, komposter možete napraviti i sami, a ako pritom koristite reciklirani materijal, poput paleta, onda doista možete reći kako ste ekološki orijenitrani.</Text>
				<Button title="Za više o načinu izrade vlastitog kompostera kliknite ovdje." onPress={() => { Linking.openURL('http://www.mojvrt.eu/') } } />
				<Text style={[ globalStyles.paragraph, { marginTop: 20, fontWeight: 'bold' } ]}>Plastični komposter</Text>
				<ResponsiveImageView source={ require('./../../assets/images/kompost4.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
			</Content>
		);
	}
}

export default KompostiranjeScreen;
