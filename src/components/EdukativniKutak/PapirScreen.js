import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Content,
	Body,
	Icon,
	Left,
	Right,
	Title,
	Text,
	Button,
} from "native-base";
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';

class PapirScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" style={globalStyles.backButtonIcon} />
							<Text style={globalStyles.backButtonText}>Natrag</Text>
						</Button>
					</Left>
					<Body>
						<Title>Papir</Title>
					</Body>
					<Right/>
				</Header>
				<Content padder>
					<Text style={globalStyles.paragraph}>Nakon što odbijemo ono što nam ne treba, reduciramo kupovinu na ono što nam je stvarno potrebno i pokušamo ponovo upotrijebiti ono što smo već unijeli u naš dom, slijedi recikliranje najvećeg dijela preostalog otpada. Recikliranjem se štedi energija jer ju je potrebno uložiti znatno manje nego za novi proizvod, čuvaju se prirodni resursi, smanjuje se količina otpada na odlagalištima, a time i štetne emisije stakleničkih plinova i zagađenje voda. Usprkos tome, nije dobro shvatiti recikliranje kao krajnje rješenje problema otpada jer je sam proces skup, zahtijeva dodatne energetske resurse te se u procesu odvojenog prikupljanja otpada kao i u proizvodnji dodatno zagađuje okoliš. Osim toga, recikliranje plastike zbog brojnih razloga predstavlja veliki problem.</Text>
					<Text style={globalStyles.subtitle}>PAPIR I KARTON</Text>
					<ResponsiveImageView source={ require('./../../assets/images/papir.jpg') }>
						{({ getViewProps, getImageProps }) => (
							<View {...getViewProps()}>
								<Image {...getImageProps()} />
							</View>
						)}
					</ResponsiveImageView>
					<Text style={globalStyles.paragraph}>U komunalnom otpadu nalazi se do 30% papirnatog otpada. Ukoliko se odlaže, organski otpad kao što je papir razgrađuje se u bioplin, koji sadrži metan – staklenički plin povezan sa globalnim zatopljenjem. Otpadni papir i karton u tijelu odlagališta vrlo se sporo razgrađuju te značajno produljuju vijek aktivnosti odlagališta.</Text>
					<Text style={globalStyles.paragraph}>Recikliranjem papira čuvamo šume, štedimo energiju, smanjujemo onečišćenje zraka i vode te štedimo skupi odlagališni prostor. Na taj način možemo za ¼ smanjiti količinu kućanskog otpada. Po 1 toni sakupljenog papira uštedi se 65% energije i 50% vode. Onečišćenje zraka smanjuje se za 74%, te se zamjenjuje 17 stabala.</Text>
					<Text style={globalStyles.paragraph}>Papir se može i do 7 puta reciklirati. Reciklirani papir se proizvodi od 80-100% starog papira i nove celuloze uz dodatak kemijskih pomoćnih sirovina. Karton se u prosjeku proizvodi od 90% starog papira. Glavni nedostatak recikliranja papirnatog otpada je ograničenje broja ponovne upotrebe. Nakon 6-7 postupaka oporabe, reciklirana vlakna više nemaju dovoljnu duljinu i čvrstoću, te je potrebno dodavanje novih celuloznih vlakana.</Text>
				</Content>
			</Container>
		);
	}
}

export default PapirScreen;
