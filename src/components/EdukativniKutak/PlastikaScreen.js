import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Content,
	Body,
	Icon,
	Left,
	Right,
	Title,
	Text,
	Button,
} from "native-base";
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';

class PlastikaScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" style={globalStyles.backButtonIcon} />
							<Text style={globalStyles.backButtonText}>Natrag</Text>
						</Button>
					</Left>
					<Body>
						<Title>Plastika</Title>
					</Body>
					<Right/>
				</Header>
				<Content padder>
					<ResponsiveImageView source={ require('./../../assets/images/plastika.png') }>
						{({ getViewProps, getImageProps }) => (
							<View {...getViewProps()}>
								<Image {...getImageProps()} />
							</View>
						)}
					</ResponsiveImageView>
					<Text style={globalStyles.paragraph}>Plastike, u usporedbi sa metalom i staklom, postavlja neke jedinstvene izazove i ograničenja za recikliranje. Prvi izazov je da njezini polimeri moraju biti gotovo istog sastava da bi se mogli miješati. Drugo ograničenje recikliranju je velika količina bojila, punila i drugih dodataka u plastici.</Text>
					<Text style={globalStyles.paragraph}>Naime svaka plastična ambalaža mora imati na sebi oznaku, odn. trokut sa strelicama i brojem u sredini koji nam govori o kojoj se plastici točno radi i koliko je ona opasna. Sveukupno ih je sedam:</Text>
					<ResponsiveImageView source={ require('./../../assets/images/plastika1.jpg') }>
						{({ getViewProps, getImageProps }) => (
							<View {...getViewProps()}>
								<Image {...getImageProps()} />
							</View>
						)}
					</ResponsiveImageView>
					<Text style={[ globalStyles.paragraph, { marginTop: 20 } ]}>1. PET ili PETE – predstavljalju boce namijenjene jednokratnoj upotrebi. Postoji mogućnost da ispuštaju teški metal koji ometa djelovanje hormona. Takve boce mogu ispuštati i kancerogene spojeve.</Text>
					<Text style={globalStyles.paragraph}>2. HDPE – nazivaju je „dobra plastika“ iz tog razloga što ne ispušta gotovo nikakve kemikalije, tim putem je najsigurnija za uporabu.</Text>
					<Text style={globalStyles.paragraph}>3. PVC – ova plastika ispušta dvije otrovne kemikalije, a obje ometaju djelovanje hormona u ljudskom tijelu. Usprkos tome, to je još uvijek često upotrijebljavana plastika.</Text>
					<Text style={globalStyles.paragraph}>4. LPDE – plastika, koja ne ispušta kemikalije u vodu. Kao npr. plastične vrećice za namirnice. Nedostatak joj je taj što, iako se može reciklirati, to se do sada u praksi slabo provodi.</Text>
					<Text style={globalStyles.paragraph}>5. PP – plastika obično bijele boje, boce u koje se pakiraju sirupi, ili čašice za jogurt. U pravilu je čvrsta, lagana i otporna na toplinu, te zbog toga ima široku primjenu. Koristi se još i za proizvodnju cijevi, laboratorijskog posuđa itd. Nedostatak isti kao i kod LDPE plastike je taj da se danas reciklira samo oko 3% proizvedenog PP (podaci iz SAD).</Text>
					<Text style={globalStyles.paragraph}>6. PS – plastika koja ispušta u vodu kancerogenu tvar stiren. Koristi se za proizvodnju kartona za jaja, CD i DVD kućišta, izolaciju, čašice za kavu za jednokratnu uporabu, ili u ambalaži brze hrane itd. Ona je jedna od najčešće korištenih plastičnih vrsta. Može se reciklirati, no u praksi se slabo provodi.</Text>
					<Text style={globalStyles.paragraph}>7. Ostalo (BPA, polikarbonat, LEXAN…) – to je najlošija plastika za prehrambene proizvode jer izlučuje kemikaliju BPA. Istraživanja na laboratorijskim životinjama su pokazala da BPA može u organizmu djelovati poput ženskog spolnog hormona estrogena i poremetiti razvoj i djelovanje reprodukcijckog sustava. Ono što je najgore jest činjenica da se plastika ove kategorije upotrebljava za izradu bočica za dječiju hranu.</Text>
					<Text style={globalStyles.paragraph}>Prema navedenim vrstama plastike, kada god je moguće, preporučuje se izabrati plastiku  2, 4 i 5 umjesto 1, 3, 6 i 7.</Text>
					<Text style={globalStyles.paragraph}>Najbolja opcija je izbjegavati plastiku u potpunosti jer je recikliranje plastike još uvijek neisplativo i ne postoji tržište za plastični otpad u Hrvatskoj.</Text>
				</Content>
			</Container>
		);
	}
}

export default PlastikaScreen;
