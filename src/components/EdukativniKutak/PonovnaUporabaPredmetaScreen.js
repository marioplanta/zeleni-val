import React, { Component } from "react";
import {
	View,
	Text,
	Image
} from "react-native";
import {
	Content
} from 'native-base';
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';

class PonovnaUporabaPredmetaScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Content padder>
				<Text style={globalStyles.paragraph}>Ponovna uporaba nije isto što i recikliranje. U osnovi je recikliranja prerada odvojeno prikupljenog otpada u neku novu korisnu formu dok ponovna uporaba podrazumijeva koristiti proizvod u njegovom originalnom obliku za istu ili za neku novu svrhu. Na taj način produžujemo njegovo trajanje prije nego što postane otpad i tako štedimo resurse koje bi bilo potrebno uložiti za recikliranja tog proizvoda.</Text>
				<Text style={globalStyles.subtitle}>Primjeri ponovne uporabe</Text>
				<Text style={globalStyles.paragraph}>– ponovno korištenje razne ambalaže za druge svrhe (važno je da se plastična ambalaža u kojoj nije bila hrana ne koristi za hranu) – npr. upotreba staklenki za zimnicu čime ponovo koristimo staru ambalažu za njeno pakiranje, i na taj način izbjegavate kupovinu tvornički konzerviranih namirnica, usput se i smanjuje količina ambalaže potrebne za proizvodnju iste. Razno povrće poput krastavaca, feferona, paprika, maslina, cikla itd. se može konzervirati, no mogu se konzervirati i razne vrste plave ribe poput srdele, palamide itd.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/ambalaza.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>-doniranje, poklanjanje ili razmjena rabljene odjeće i obuće, knjiga, udžbenika, igračaka, elektroničke opreme, malih kućanskih aparata, bijele tehnike i sl.</Text>
				<Text style={globalStyles.paragraph}>Od sada možete donirati, poklanjati ili razmjenjivati predmete koji vam više ne trebaju putem našeg web buvljaka</Text>
				<Text style={globalStyles.paragraph}>– popravak ili prenamjena namještaja, elektroničke opreme, malih kućanskih aparata, bijele tehnike i sl.</Text>
				<Text style={globalStyles.paragraph}>Iznenadili biste se što se sve može napraviti od starih kućanskih aparata, namještaja, pa čak i otpada! Dalje smo naveli par primjera da vidite što se sve može prenamijeniti uz malu dozu kreativnosti:</Text>
				
				<Text style={globalStyles.paragraph}>Naslonjač od stare kade</Text>
				<ResponsiveImageView source={ require('./../../assets/images/naslonjac.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginTop: 20 } ]}>Stare žarulje pretvorite u nove lampe</Text>
				<ResponsiveImageView source={ require('./../../assets/images/lampe.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginTop: 20 } ]}>Kupaonski ormarić od starog bicikla</Text>
				<ResponsiveImageView source={ require('./../../assets/images/ormaric.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginTop: 20 } ]}>Reciklirajte stare boce u kreativan luster</Text>
				<ResponsiveImageView source={ require('./../../assets/images/luster.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginTop: 20 } ]}>Kalem preuređen u stol</Text>
				<ResponsiveImageView source={ require('./../../assets/images/stol.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginTop: 20 } ]}>Donesite predmete koji vam više ne trebaju u „Kutak za ponovnu uporabu“ u Reciklažno dvorište u Pločama ili ih poklonite ili prodajte putem web-buvljaka!</Text>
			</Content>
		);
	}
}

export default PonovnaUporabaPredmetaScreen;
