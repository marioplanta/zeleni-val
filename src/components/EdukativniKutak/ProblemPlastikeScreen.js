import React, { Component } from "react";
import {
	View,
	Text,
	Image
} from "react-native";
import {
	Content
} from 'native-base';

import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';

class ProblemPlastikeScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Content padder>
				<Text style={[ globalStyles.subtitle, { marginTop: 0 } ]}>Kako je plastika zarazila svijet?</Text>
				<Text style={globalStyles.paragraph}>Proizvodnja plastike započela je 1930-ih i 40-ih godina, a omasovila se 1950-ih. Radi se o vrlo praktičnom materijalu; savitljivom, laganom i lako obradivom pa nije ni čudo da je vrlo brzo ušao u svakodnevnu upotrebu, pogotovo u Americi. Ubrzo, proizvodnja plastike se nezamislivo povećala, tako da je 2015. iznosila 269 milijuna tona. Glavni proizvođač plastičnih materijala je Kina (27,8%), zatim Europa (18,5%) i zemlje članice Sjevernoameričkog sporazuma o slobodnoj trgovini ili NAFTA-e (18,5%). Većina sirovina za izradu plastike dolazi od fosilnih goriva; nafte, ugljena i plina, znači od neobnovljivih izvora koji zagađuju okoliš, ugrožavaju zdravlje ljudi i uzrokuju klimatske promjene. Sasvim dovoljno razloga da ih, bez obzira na namjenu, ostavimo pod zemljom.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/plastika3.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Velika većina proizvedene plastike koristi se jednokratno, a najveći dio za najjednokratniju od svih upotreba – za pakiranje, tj. ambalažu. Sad je dobar trenutak za naglasiti da plastični materijali traju jako, jako dugo i da se zapravo nikad ne razgrade. Plastičnim predmetima trebaju stotine pa i tisuće godina da se raspadnu, a ni onda ne nestaju već prelaze u mikroplastiku. Zato je i više nego apsurdno što neke od njih, poput spomenute ambalaže, zatim žličica za miješanje kave ili štapića za uši, koristimo doslovno nekoliko sekundi i zatim bacamo.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/plastika4.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Zbog masovne proizvodnje udružene s jednokratnom upotrebom, stvaraju se nezamislive količine otpada, koji nažalost vrlo često završi u morima i oceanima. Dosad je, u svijetu proizvedeno 8.300 milijuna tona nove plastike, a do 2015. velika većina toga , tj. 6.300 milijuna tona postala je otpad. Od tog otpada, čak 79% je završilo na odlagalištima, 12% je spaljeno, a samo 9% reciklirano. Plastika s odlagališta, zbog lošeg gospodarenja otpadom diljem svijeta, raznim vodenim tokovima dospijeva u mora i oceane. Tamo čini čak 60-80% svog morskog otpada od kojeg stradavaju životinje, bilo zaplitanjem, gušenjem ili zbog pothranjenosti (jer jedu plastiku misleći da je npr. meduza), a usitnjavanjem u mikro- i nanoplastiku dospijeva i u hranidbeni lanac ljudi. Recikliranje, iako nužno, ne funkcionira ni približno dovoljno dobro da se sanira značajan dio plastičnog otpada.</Text>
				<Text style={globalStyles.paragraph}>Zašto onda i dalje koristimo taj materijal, za koji plaćamo tako visoku cijenu? Zašto pristajemo na to? Kako nas uspijevaju natjerati da sudjelujemo u uništenju okoliša, posebno naših mora u koja godišnje dospijeva i do 12 milijuna tona plastičnog otpada? Nametanjem (ne)kulture bacanja, normaliziranjem i nametanjem jednokratne upotrebe i uvjeravanjem javnosti da je plastika jeftina, praktična i sigurna.</Text>
				<Text style={globalStyles.paragraph}>Mediteran, u koji spada i Jadran, već je jedno od najugroženijih područja što se tiče onečišćenja plastikom. Gustoća plastičnog otpada je jedan komad na svaka 4m², što ga svrstava uz bok s tzv. oceanskim vrtlozima plastike u svjetskim oceanima. Stanje je alarmantno, no rješenja postoje. Osim uvođenja veće odgovornosti proizvođača i korporacija za svoje proizvode,  ponovne upotrebe materijala i puno efikasnijeg recikliranja, najvažniji korak je da odbacimo bacanje kao svakodnevni stil života.  Poštujmo prirodu, poštujmo život, poštujmo sebe. Zaslužujemo kvalitetu i trajne predmete koje ćemo koristiti puno puta i uživati u njima.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/plastika5.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.subtitle, { marginTop: 15 } ]}>EU ZABRANILA JEDNOKRATNU PLASTIKU OD 2021.</Text>
				<Text style={globalStyles.paragraph}>Po direktivi o smanjenju utjecaja određenih plastičnih proizvoda na okoliš, koje su dogovorene između tri glavne institucije EU-a još u prosincu, uvodi se zabrana određenih plastičnih proizvoda za jednokratnu uporabu za sve zemlje članice od 2021.g.</Text>
				<Text style={globalStyles.paragraph}>To se odnosi na proizvode, kao što su slamke, štapići za uši, pribor za jelo, tanjuri, oksorazgradiva plastika, spremnici za hranu, čaše od ekspandiranog polistirena te štapići za pridržavanje balona, koji zajedno s izgubljenom opremom za ribolov čine 70 posto otpada na europskim obalama i u morima.</Text>
			</Content>
		);
	}
}

export default ProblemPlastikeScreen;
