import React, { Component } from "react";
import {
	View,
	Image,
	Button,
	Linking
} from "react-native";
import {
	Content,
	Text,
} from "native-base";
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';

class PosebneKategorijeOtpadaScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Content padder>
				<Text style={[ globalStyles.subtitle, { marginTop: 0 } ]}>Ambalažni otpad</Text>
				<Text style={globalStyles.paragraph}>Gospodarenje otpadnom ambalažom složena je djelatnost koja ima za svrhu sprječavanje odlaganja otpadne ambalaže na deponije. Osim štete za okoliš, odlaganje otpadne ambalaže predstavlja i znatnu ekonomsku štetu, zbog odlaganja tvari s vrijednim materijalnim i/ili energetskim svojstvima (papir, karton, metali, plastika, staklo, drvo).</Text>
				<Text style={globalStyles.paragraph}>Ambalaža je sve ono što u odnosu na proizvod ima zaštitnu, transportnu, uporabnu, informativnu i ekološku funkciju, te koja se prije ili tijekom konzumacije proizvoda (sadržaja) mora odložiti ili odbaciti. U tom trenutku ambalaža postaje otpad.</Text>
				<Text style={globalStyles.paragraph}>Ambalažni otpad dijeli se na sljedeće materijale:</Text>
				<Text style={globalStyles.paragraphNoMargin}>– papir/karton, ključni broj otpada</Text>
				<Text style={globalStyles.paragraphNoMargin}>– plastika,</Text>
				<Text style={globalStyles.paragraphNoMargin}>– drvo,</Text>
				<Text style={globalStyles.paragraphNoMargin}>– metalni,</Text>
				<Text style={globalStyles.paragraphNoMargin}>– višeslojni (kompozitni),</Text>
				<Text style={globalStyles.paragraphNoMargin}>– stakleni,</Text>
				<Text style={globalStyles.paragraph}>– tekstilni.</Text>
				<Text style={globalStyles.paragraph}>Republika Hrvatska je prihvatila model sustava povratne ambalaže za jedinice (boce i limenke) otpadne ambalaže od PET-a, stakla i Al/Fe.</Text>
				<Text style={globalStyles.paragraph}>Gdje sa ambalažom?</Text>
				<Text style={globalStyles.paragraphNoMargin}>1. Zeleni otoci – ambalažu možete baciti u posebne spremnike za papir i karton (plavi), plastiku i metal (žuti) i staklo (zeleni)</Text>
				<Text style={globalStyles.paragraphNoMargin}>2. Kanta za reciklabilni otad u područjima gdhe se provode pilot projekti</Text>
				<Text style={globalStyles.paragraphNoMargin}>3. Reciklažno dvorište Ploče</Text>
				<Text style={globalStyles.paragraph}>4. Ambalažu koja je u sustavu povratne naknade možete odnijeti u trgovine koje istu prodaju</Text>
				<Text style={globalStyles.subtitle}>Električni i elektronički otpad</Text>
				<Text style={globalStyles.paragraph}>Električni i elektronički otpad (EE otpad) spada u posebne kategorije otpada (PKO).Sadržava vrijedne metalne i nemetalne sirovine koje se dobiju materijalnom oporabom (recikliranjem), a mogu se koristiti i u energetske svrhe. Izdvajaju se i dijelovi koji se koriste za ponovnu uporabu.</Text>
				<Text style={globalStyles.paragraph}>Kategorije električne i elektroničke opreme:</Text>
				<Text style={globalStyles.paragraphNoMargin}>– Veliki kućanski uređaji,</Text>
				<Text style={globalStyles.paragraphNoMargin}>– Mali kućanski uređaji,</Text>
				<Text style={globalStyles.paragraphNoMargin}>– Oprema informatičke tehnike (IT) i oprema za telekomunikacije,</Text>
				<Text style={globalStyles.paragraphNoMargin}>– Oprema široke potrošnje i fotonaponske ploče,</Text>
				<Text style={globalStyles.paragraphNoMargin}>– Rasvjetna oprema,</Text>
				<Text style={globalStyles.paragraphNoMargin}>– Električni i elektronički alati (osim velikih nepokretnih industrijskih alata),</Text>
				<Text style={globalStyles.paragraphNoMargin}>– Igračke, oprema za razonodu i sportska oprema,</Text>
				<Text style={globalStyles.paragraphNoMargin}>– Medicinski proizvodi (osim svih implantiranih i inficiranih proizvoda),</Text>
				<Text style={globalStyles.paragraphNoMargin}>– Instrumenti za praćenje i kontrolu,</Text>
				<Text style={globalStyles.paragraph}>– Automatski samoposlužni uređaji.</Text>
				<Text style={globalStyles.paragraph}>Svake godine u svijetu se proda 300 milijuna računala i milijardu mobilnih uređaja, a 80% ih završi na odlagalištima. Računala i pametni telefoni sadrže elemente opasne po zdravlje ljudi kao što su olovo, živa, kadmij, berilij i arsen – zato se moraju sakupljati i odvoziti odvojeno od ostalog otpada</Text>
				<Text style={globalStyles.paragraph}>EE uređaji označeni su ovom oznakom:</Text>
				<ResponsiveImageView source={ require('./../../assets/images/ee-otpad.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={globalStyles.subtitle}>Gdje sa EE otpadom??</Text>
				<Text style={globalStyles.paragraph}>1. Uređaje veličine do 25 cm besplatno predajte u trgovini s oznakom Preuzimamo EE otpad bez obveze kupnje. Žarulje možete predati u bilo kojoj trgovini koja ih prodaje, neovisno o veličini.</Text>
				<Text style={globalStyles.paragraph}>Prodavatelj EE opreme s maloprodajnom trgovinom koja ima više od 400 m 2 prodajne površine za EE opremu i prodaje EE opremu obvezan je na vidnom mjestu na ulazu za kupce u trgovinu naljepnicom i na istaknutom mjestu informirati građane o preuzimanju EE optada.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/ee-otpad1.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraphNoMargin, { marginTop: 20 } ]}>2. Veće uređaje besplatno predajte u trgovinu prilikom kupnje novoga.</Text>
				<Text style={globalStyles.paragraphNoMargin}>3. Besplatno predajte EE otpad u reciklažno dvorište</Text>
				<Text style={globalStyles.paragraph}>4. Nazovite besplatan broj 0800 444 110 i dogovorite besplatan odvoz EE otpada s ovlaštenim sakuplljačima koji će doći na vašu adresu.</Text>
				
				<Text style={globalStyles.subtitle}>Otpadne gume</Text>
				<Text style={globalStyles.paragraph}>Odvojeno sakupljanje otpadnih guma osigurava se putem ovlaštenih sakupljača.</Text>
				<Text style={globalStyles.paragraph}>Otpadne gume ne smiju se odlagati na odlagališta.</Text>
				<Text style={globalStyles.paragraphNoMargin}>Gdje sa otpadnim gumama:</Text>
				<Text style={globalStyles.paragraphNoMargin}>1. Predajte ih besplatno u reciklažno dvorište</Text>
				<Text style={globalStyles.paragraphNoMargin}>2. Prilikom zamjene guma predajte ih serviseru koji ih je dužan preuzeti bez naknade</Text>
				<Text style={globalStyles.paragraph}>3. Predajte ih na skladište ovlaštenih sakupljača</Text>
				<Text style={globalStyles.paragraph}>Popis ovlaštenih sakupljača možete pronaći na sljedećoj poveznici.</Text>
				
				<Text style={globalStyles.subtitle}>Otpadna ulja</Text>
				<Text style={globalStyles.paragraph}>Otpadno ulje je svako ulje (biljno, životinjsko mineralno, sintetičko, industrijsko izolacijsko i/ili termičko ulje) koje više nije za uporabu kojoj je prvobitno bilo namijenjeno.</Text>
				<Text style={globalStyles.paragraph}>Razlikujemo otpadna jestiva i otpadna maziva ulja.</Text>
				<Text style={globalStyles.paragraph}>Pod otpadna jestiva ulja koja su biorazgradiva i čine neopasan otpad, svrstavaju se sva ulja koja nastaju obavljanjem ugostiteljske i turističke djelatnosti, industriji, obrtu, zdravstvenoj djelatnosti, javnoj upravi i drugim sličnim djelatnostima u kojima se priprema više od 20 obroka dnevno. Vrijedna su sirovina za proizvodnju bio dizela.</Text>
				<Text style={globalStyles.paragraph}>Otpadna maziva ulja su opasni otpad jer jedna litra ulja zagadi milijun litara vode, odnosno trajno onečisti tlo jer najvećim dijelom nisu biološki razgradiva. Vrijedna su sirovina jer se mogu regenerirati i služiti kao sirovina za proizvodnju svježih mazivih ulja, odnosno postupkom materijalne oporabe iz njih dobiti estere za proizvodnju sapuna, sredstava za pranje i slično. Otpadna maziva ulja vrijedan su energent u energetskim i proizvodnim postrojenjima instalirane snage uređaja veće ili jednake 3 MW, jer se takvom oporabom sprječava onečišćenje okoliša.</Text>
				<Text style={globalStyles.paragraph}>Gospodarenje otpadnim uljima obuhvaća postupke sakupljanja, predobrade/kondicioniranja i njihovu regeneraciju ili materijalnu oporabu odnosno korištenje u energetske svrhe.</Text>
				<Text style={globalStyles.paragraph}>Prilikom gospodarenja otpadnim uljima zabranjeno je:</Text>
				<Text style={globalStyles.paragraphNoMargin}>– ispuštanje otpadnih ulja u površinske vode, podzemne vode, priobalne vode i drenažne sustave,</Text>
				<Text style={globalStyles.paragraphNoMargin}>– odlaganje i/ili ispuštanje otpadnih ulja koje šteti tlu te svako nekontrolirano ispuštanje ostataka od obrade otpadnih ulja,</Text>
				<Text style={globalStyles.paragraphNoMargin}>– oporaba i/ili zbrinjavanje otpadnih ulja koji uzrokuju onečišćenje zraka iznad razine propisane važećim propisima i utječu na zdravlje ljudi i biljni i životinjski svijet,</Text>
				<Text style={globalStyles.paragraph}>– sakupljanje otpadnih ulja u spremnike koji nisu propisano opremljeni za prihvat otpadnih ulja.</Text>
				
				<Text style={globalStyles.subtitle}>Gdje sa otpadnim uljima</Text>
				<Text style={globalStyles.paragraphNoMargin}>1. Predajte ih besplatno u reciklažno dvorište</Text>
				<Text style={globalStyles.paragraph}>2. Otpadna maziva ulja možete predati ovlaštenim sakupljačima ili prodavateljima ulja (benzinske postaje, serviseri) koji su ih dužni preuzeti bez naknade.</Text>

				<Text style={globalStyles.subtitle}>Otpadna vozila</Text>
				<Text style={globalStyles.paragraphNoMargin}>Otpadno vozilo je vozilo koje radi oštećenja, dotrajalosti i drugih uzroka posjednik odbacuje, namjerava ili mora odbaciti. Zbog mogućnosti nekontroliranoga ispusta tekućina predstavljaju opasnost za okoliš te zahtijevaju posebnu brigu prilikom gospodarenja otpadnim vozilima.</Text>
				<Text style={globalStyles.paragraph}>Gospodarenje otpadnim vozilima i njihovim dijelovima je skup mjera koje obuhvaćaju sakupljanje, obradu, ponovnu uporabu dijelova otpadnih vozila, oporabu otpadnih vozila i zbrinjavanje novonastalog otpada koji se ne može oporabiti.</Text>
				<Text style={globalStyles.paragraph}>Posjednik otpadnog vozila obvezan je otpadno vozilo predati sakupljaču, po mogućnosti u cijelosti, i to na skladištu sakupljača ili pozivom sakupljača na lokaciju gdje se otpadno vozilo nalazi.</Text>
				<Text style={globalStyles.paragraph}>Cjelovito otpadno voziloje otpadno vozilo koje obavezno ima motor (glavu motora, blok motora i karter) i karoseriju (školjku, poklopac motora, poklopac prtljažnika i sva vrata).</Text>
				<Text style={globalStyles.paragraph}>Otpadno vozilo koje nema sve dijelove cjelovitog otpadnog vozila smatra se necjelovitim otpadnim vozilom.</Text>
				<Text style={globalStyles.paragraph}>Kategorije motornih vozila koje se preuzimaju sukladno odredbama Pravilnika o gospodarenju otpadnim vozilima jesu:</Text>
				<Text style={globalStyles.paragraphNoMargin}>– kategorija M1, motorna vozila za prijevoz osoba koja osim sjedala za vozača imaju još najviše osam sjedala,</Text>
				<Text style={globalStyles.paragraphNoMargin}>– kategorija N1, motorna vozila za prijevoz tereta čija najveća dopuštena masa nije veća od 3,5 tone i</Text>
				<Text style={globalStyles.paragraph}>– kategorija L2, Mopedi s tri kotača.</Text>
				<Text style={globalStyles.paragraph}>Posjednik otpadnog vozila koji je pravna ili fizička osoba – obrtnik obvezan je prilikom predaje otpadnog vozila predati sakupljaču dokaz o vlasništvu otpadnog vozila (presliku prometne dozvole otpadnog vozila koje predaje i ukoliko nije upisan kao vlasnik vozila u prometnoj dozvoli presliku drugog dokumenta iz kojeg je razvidno da je otpadno vozilo njegovo vlasništvo) ili dokaz o pravu na predaju otpadnog vozila na oporabu (ovjerenu punomoć od strane vlasnika vozila, presliku prometne dozvole otpadnog vozila) ili dokaz da je za predmetno vozilo plaćena naknada gospodarenja Fondu ili potvrdu o provjeri registriranosti vozila u Republici Hrvatskoj.</Text>
				<Text style={globalStyles.paragraph}>Posjednik otpadnog vozila koji je fizička osoba obvezan je prilikom predaje otpadnog vozila predati sakupljaču dokaz o vlasništvu otpadnog vozila (presliku prometne dozvole otpadnog vozila koje predaje i ukoliko nije upisan kao vlasnik vozila u prometnoj dozvoli presliku drugog dokumenta iz kojeg je razvidno da je otpadno vozilo njegovo vlasništvo, te na uvid osobni identifikacijski dokument) ili dokaz o pravu na predaju otpadnog vozila na oporabu (ovjerenu punomoć od strane vlasnika vozila, presliku prometne dozvole otpadnog vozila, te na uvid osobni identifikacijski dokument) ili dokaz da je za predmetno vozilo plaćena naknada gospodarenja Fondu ili potvrdu o provjeri registriranosti vozila u Republici Hrvatskoj te Izjavu o vlasništvu otpada propisanu posebnim propisom kojim se uređuje gospodarenje otpadom.</Text>
				<Text style={globalStyles.paragraph}>Posjednik cjelovitog otpadnog vozila koji je prilikom predaje sakupljaču predao jedan od gore navedenih dokumenata ima pravo na naknadu koju mu isplaćuje sakupljač na temelju vaganjem utvrđene mase predanog otpadnog vozila na skladištu sakupljača, a koja ne može biti veća od mase vozila iskazane u prometnoj dozvoli. Posjednik otpadnog vozila prilikom predaje cjelovitog otpadnog vozila sakupljaču ima pravo na naknadu koju mu isplaćuje sakupljač koja iznosi 1kn/kg, odnosno na polovinu tog iznosa ukoliko predaje otpadno vozilo koje je necjelovito umanjeno za pripadajući porez i prirez. Zahtjev za provjeru registriranosti vozila u Republici Hrvatskoj u ime posjednika podnosi sakupljač elektroničkim putem Fondu. Potvrdu o provjeri registriranosti vozila u Republici Hrvatskoj za potrebe posjednika Fond dostavlja elektroničkim putem sakupljaču.</Text>
				
				<Text style={globalStyles.subtitle}>Baterije i akumulatori</Text>
				<Text style={globalStyles.paragraph}>Baterija ili akumulator označava svaki izvor električne energije proizvedene izravnim pretvaranjem kemijske energije koji se sastoji od jedne ili više primarnih baterijskih ćelija/članaka (koje se ne mogu puniti) ili jedne ili više sekundarnih baterijskih ćelija/članaka (koje se mogu puniti).</Text>
				<Text style={globalStyles.paragraph}>Baterije (akumulatori) sadrže teške metale poput žive, olova, kadmija i zato su često vrlo toksične te stoga zahtjevaju specijalan način recikliranja.</Text>
				<Text style={globalStyles.paragraph}>Većina otpadnih baterija (akumulatora) klasificira se kao opasni otpad.</Text>
				<Text style={globalStyles.paragraph}>U opasni otpad uvrštavaju se olovne baterije, nikal-kadmij baterije, baterije sa živom te odvojeno skupljeni elektroliti iz baterija i akumulatora.</Text>
				<Text style={globalStyles.paragraph}>Glavna prednost recikliranja baterija jest smanjenje primarne proizvodnje materijala i energenata, te emisije žive, olova i kadmija u prirodu.</Text>
				<Text style={globalStyles.paragraph}>Istrošene baterije i akumulatori ne spadaju u komunalni otpad. Zakonski je propisano vraćanje otpadnih baterija (akumulatora) u spremnike, na mjesto kupnje ili sakupljalište.</Text>
				
				<Text style={globalStyles.subtitle}>Gdje sa baterijama i akumulatorima?</Text>
				<Text style={globalStyles.paragraphNoMargin}>1. Besplatno ih predajte u reciklažno dvorište</Text>
				<Text style={globalStyles.paragraph}>2. Besplatno ih predajte prodavateljima baterija i akumulatorima- potražite oznaku:</Text>
				<ResponsiveImageView source={ require('./../../assets/images/baterije.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>

				<Text style={globalStyles.subtitle}>Građevinski otpad koji sadrži azbest</Text>
				<Text style={globalStyles.paragraph}>Azbest je mineralni kristal vlaknaste strukture. Postoji šest (6) osnovnih tipova azbesta: aktinolit, antofilit, tremolit, krokidolit (plavi), krizotil (bijeli) i amozit (smeđi). Najčešće korišteni azbestni materijal u Republici Hrvatskoj bio je krizotil, koji je najmanje opasan od navedenih.</Text>
				<Text style={globalStyles.paragraph}>S obzirom na jako dobre osobine azbest se dodavao raznim proizvodima kako bi im se osigurala mehanička i kemijska svojstva, otpornost na vlagu, vatru, vrućinu, buku, elektricitet, habanje i trenje.</Text>
				<Text style={globalStyles.paragraph}>Smatra se da dugogodišnjom preradom i upotrebom azbestnih minerala u nekoliko tisuća proizvoda (npr. žbuci, izolacijskom materijalu, fasadnim pločama, krovnim ravnim i valovitim pločama, zidnim i podnim pločicama, crijepu, cigli, cementnim cijevima za vodu, azbestnim brtvama, kočnim pločicama, protupožarnim pokrivačima, radnim rukavicama, azbestnim ljepenkama, smjesama za brtvljenje i dr.) nailazimo na jednu ili više azbestnih vlakana.</Text>
				<Text style={globalStyles.paragraph}>Većina ih je danas u upotrebi, ali pod strogim oprezom i kontrolom jer mu nema zamjenskog tehničkog materijala. Svoju primjenu azbest ima u brodogradilištu, svemirskim letjelicama, elektroindustriji, bolnicama, školama, knjižnicama, državnim zgradama, kancelarijama, poduzećima, kućama, zgradama i mnogim drugim granama gospodarstva.</Text>
				<Text style={globalStyles.paragraph}>Međutim, istraživanja su pokazala da čestice azbesta tzv. prašina azbestnih igličastih vlakana koje se oslobađaju u zrak (emisija azbesta) izazivaju kod ljudi, uslijed kontakta, bilo u industriji ili neposrednoj blizini, nakon izvjesnog vremena teške kronične bolesti (azbestozu) pa i smrt.</Text>
				<Text style={globalStyles.paragraph}>Zabrana proizvodnje, prometa i upotrebe azbesta i materijala koji sadrže azbest u Republici Hrvatskoj stupila je na snagu 01.01.2006. godine.</Text>
				
				<Text style={globalStyles.subtitle}>Gdje sa otpadom koji sadrži azbest?</Text>
				<Text style={globalStyles.paragraph}>Prema odredbama Pravilnika o gospodarenju otpadom građani mogu u reciklažno dvorište odnijeti svakih šest mjeseci do 200 kg tog otpada. Također jedinice lokalne samouprave su prema odredbama istog Pravilnika dužne u cijelosti osobi koja upravlja reciklažnim dvorištem (najčešće je to komunalno društvo) nadoknaditi troškove gospodarenja građevnim otpadom koji sadrži azbest koji je nastao u kućanstvu korisnika usluga.</Text>
				<Text style={globalStyles.paragraph}>Kako postupati s otpadom koji sadrži azbest?</Text>
				<Button title="Za upute o postupanju kliknite ovdje." onPress={() => { Linking.openURL('http://www.cian.hr/userfiles/upute_azbest.pdf') } } />
				
				<Text style={globalStyles.subtitle}>Medicinski otpad</Text>
				<Text style={globalStyles.paragraph}>Vrste medicinskog otpada:</Text>
				<Text style={globalStyles.paragraph}>1. Otpad koji nastaje tijekom pružanja medicinskih usluga je opasan ako posjeduje jedno ili više od dolje navedenih svojstava ili obilježja.</Text>
				<Text style={globalStyles.paragraph}>2. Patološki otpad: dijelovi ljudskog tijela – amputati, tkiva i organi odstranjeni tijekom kirurških zahvata, tkiva uzeta u dijagnostičke svrhe, placente i fetusi, pokusne životinje i njihovi dijelovi.</Text>
				<Text style={globalStyles.paragraph}>3. Infektivni otpad: otpad koji sadrži patogene biološke agense koji zbog svojeg tipa, koncentracije ili broja mogu izazvati bolest u ljudi koji su im izloženi – kulture i pribor iz mikrobiološkog laboratorija, dijelovi opreme, materijal i pribor koji je došao u dodir s krvlju ili izlučevinama infektivnih bolesnika ili je upotrijebljen pri kirurškim zahvatima, previjanju rana i obdukcijama, otpad iz odjela za izolaciju bolesnika, otpad iz odjela za dijalizu, sistemi za infuziju, rukavice i drugi pribor za jednokratnu uporabu, te otpad koji je došao u dodir s pokusnim životinjama kojima je inokuliran zarazni materijal, itd.</Text>
				<Text style={globalStyles.paragraph}>4. Oštri predmeti: igle, lancete, štrcaljke, skalpeli i ostali predmeti koji mogu izazvati ubod ili posjekotinu.</Text>
				<Text style={globalStyles.paragraph}>5. Farmaceutski otpad: uključuje farmaceutske proizvode, lijekove i kemikalije koji su vraćeni s odjela gdje su bili proliveni, rasipani, pripremljeni a neupotrjebljeni, ili im je istekao rok uporabe ili se trebaju baciti iz bilo kojeg razloga.</Text>
				<Text style={globalStyles.paragraph}>6. Kemijski otpad: odbačene krute, tekuće ili plinovite kemikalije koje se upotrebljavaju pri medicinskim, dijagnostičkim ili eksperimentalnim postupcima, čišćenju i dezinfekciji. Dijeli se na opasni kemijski otpad – toksične, korozivne, lako zapaljive, reaktivne i genotoksične tvari i inertni kemijski otpad koji nema navedena svojstva.</Text>
				<Text style={globalStyles.paragraph}>7. Posude pod pritiskom: bočice koje sadrže inertne plinove pod pritiskom pomiješane s djelatnim tvarima (antibiotik, dezinficijens, insekticid itd.) koje se apliciraju u obliku aerosola, a pri izlaganju višim temperaturama mogu eksplodirati.</Text>
				<Text style={globalStyles.paragraph}>8. Radioaktivni otpad: podliježe posebnim propisima.</Text>
				
				<Text style={globalStyles.subtitle}>Gdje s medicinskim otpadom?</Text>
				<Text style={globalStyles.paragraph}>1. Djelatnici ustanova koje obavljaju zdravstvenu njegu u kući bolesnika i/ili slične djelatnosti, dužni su preuzimati opasni medicinski otpad nastao njihovom djelatnošću. Obrada preuzetog otpada mora se osigurati na trošak ustanove.</Text>
				<Text style={globalStyles.paragraph}>2. Ljekarne su dužne od građana preuzimati stare lijekove i/ili sličan farmaceutski otpad neovisno o njegovu podrijetlu.</Text>
				<Text style={globalStyles.paragraph}>3. Veterinarske ljekarne i veterinarske ambulante dužne su od građana preuzimati stare veterinarske lijekove i/ili sličan farmaceutski otpad nastao pružanjem veterinarskih usluga u kućanstvima i/ili na poljoprivrednim gospodarstvima.</Text>
				<Text style={globalStyles.paragraph}>4. Reciklažna dvorišta dužna su, sukladno posebnom propisu, preuzimati medicinski otpad iz kućanstva.</Text>
				
				<Text style={globalStyles.subtitle}>Otpadni tekstil i obuća</Text>
				<Text style={globalStyles.paragraph}>Otpadni tekstil i obuća imaju vrijedna svojstva koja mogu biti ponovno uporabljena, materijalno ili termički iskoristiva.</Text>
				<Text style={globalStyles.paragraph}>Gospodarenjem otpadnim tekstilom i obućom na pravilan i siguran način čuva se okoliš i ljudsko zdravlje, energetski i vodni resursi, znatno se smanjuje opterećenje odlagališta otpadom, otvaraju se nova radna mjesta i potiče gospodarstvo. Ponovno iskorištavanje i recikliranje pružaju ekološke i ekonomske dobrobiti.</Text>

				<Text style={globalStyles.subtitle}>Gdje s otpadnim tekstilom i obućom?</Text>
				<Text style={globalStyles.paragraph}>1. Besplatno predajte u reciklažno dvorište.</Text>
				<Text style={globalStyles.paragraph}>2. Ostavite u spremnicima postavljenim na javnim površinama.</Text>
				<Text style={globalStyles.paragraph}>3. Poklonite ili prodajte putem našeg web buvljaka ili nekim drugim putem.</Text>
				<Text style={globalStyles.paragraph}>4. Prekrojite i prenamijenite staru odjeću u jastuke, torbe, kuhinjske krpe ili krpe za čišćenje.</Text>
			</Content>
		);
	}
}

export default PosebneKategorijeOtpadaScreen;
