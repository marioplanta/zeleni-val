import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Content,
	Body,
	Icon,
	Left,
	Right,
	Title,
	Text,
	Button,
} from "native-base";
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';

class MetalScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" style={globalStyles.backButtonIcon} />
							<Text style={globalStyles.backButtonText}>Natrag</Text>
						</Button>
					</Left>
					<Body>
						<Title>Metal</Title>
					</Body>
					<Right/>
				</Header>
				<Content padder>
					<ResponsiveImageView source={ require('./../../assets/images/metal.jpg') }>
						{({ getViewProps, getImageProps }) => (
							<View {...getViewProps()}>
								<Image {...getImageProps()} />
							</View>
						)}
					</ResponsiveImageView>
					<Text style={globalStyles.paragraph}>Recikliranje metala je proces ponovnog korištenja metalnih materijala, ponajviše alumija i čelika. Svi proizvodi sačinjeni od aluminija i čelika u velikom se udjelu dadu reciklirati, a recikliranjem istih sirovina štedimo do 95% energije potrebne za proizvodnju novih materijala. Metali imaju jako veliki postotak ponovne iskoristivosti, a ponajviše čelik – do 100%. u, trajnost, mogućnost oblikovanja i recikliranje.</Text>
					<Text style={globalStyles.paragraph}>Najvažnije za upamtiti za recikliranje metala je da Reciklirani aluminij zahtjeva svega 5% energije koja je potrebna za proivodnju novog, primarnog aluminija.</Text>
				</Content>
			</Container>
		);
	}
}

export default MetalScreen;
