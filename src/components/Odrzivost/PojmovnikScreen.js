import React, { Component } from "react";
import {
	View,
	Text
} from "react-native";
import {
	Content
} from 'native-base';

import globalStyles from './../../assets/styles/Style';

class PojmovnikScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { textStyle } = styles;

		return (
			<Content padder>
				<Text style={globalStyles.paragraph}>BIOLOŠKI RAZGRADIVI OTPAD  je otpad koji se može razgraditi biološkim aerobnim ili anaerobnim postupkom;</Text>
				<Text style={globalStyles.paragraph}>BIOOTPAD je biološki razgradiv otpad iz vrtova i parkova, hrana i kuhinjski otpad iz kućanstava, restorana, ugostiteljskih i maloprodajnih objekata i slični otpad iz proizvodnje prehrambenih proizvoda;</Text>
				<Text style={globalStyles.paragraph}>BIORAZGRADIVI KOMUNALNI OTPAD je otpad nastao u kućanstvu i otpad koji je po prirodi i sastavu sličan otpadu iz kućanstva, osim proizvodnog otpada i otpada iz poljoprivrede, šumarstva, a koji u svom sastavu sadrži biološki razgradiv otpad;</Text>
				<Text style={globalStyles.paragraph}>CENTAR ZA GOSPODARENJE OTPADOM je sklop više međusobno funkcionalno i/ili tehnološki povezanih građevina i uređaja za obradu komunalnog otpada;</Text>
				<Text style={globalStyles.paragraph}>DEKLASIFIKACIJA je postupak dokazivanja da je određeni otpad koji je, u skladu s Katalogom otpada, određen kao opasan u pojedinačnom slučaju neopasni otpad;</Text>
				<Text style={globalStyles.paragraph}>DJELATNOST DRUGE OBRADE OTPADA je postupak pripreme prije oporabe ili zbrinjavanja otpada;</Text>
				<Text style={globalStyles.paragraph}>DJELATNOST OBRADE OTPADA uključuje postupke oporabe propisane Dodatkom II. ovoga Zakona;</Text>
				<Text style={globalStyles.paragraph}>DJELATNOST POSREDOVANJA U GOSPODARENJU OTPADOM uključuje postupak posredovanja u gospodarenju otpadom koji obuhvaća poslove posredništva i organizacije sakupljanja, oporabe, zbrinjavanja i druge obrade otpada, posredovanja u prijenosu prava i obveza u vezi otpada, vođenja evidencija i očevidnika u vezi gospodarenja otpadom za potrebe drugih;</Text>
				<Text style={globalStyles.paragraph}>DJELATNOST PRIJEVOZA OTPADA je prijevoz otpada za vlastite potrebe ili za potrebe drugih na teritoriju Republike Hrvatske;</Text>
				<Text style={globalStyles.paragraph}>DJELATNOST SAKUPLJANJA OTPADA uključuje postupke sakupljanja otpada i interventnog sakupljanja otpada i postupak sakupljanja otpada u reciklažno dvorište;</Text>
				<Text style={globalStyles.paragraph}>DJELATNOST TRGOVANJA OTPADOM je kupovanje i prodavanje otpada sa ili bez preuzimanja otpada u posjed bez obzira na način prodaje;</Text>
				<Text style={globalStyles.paragraph}>DJELATNOST ZBRINJAVANJA OTPADA uključuje postupke zbrinjavanja otpada propisane Dodatkom I. ovoga Zakona;</Text>
				<Text style={globalStyles.paragraph}>GOSPODARENJE OTPADOM su djelatnosti sakupljanja, prijevoza, oporabe i zbrinjavanja i druge obrade otpada, uključujući nadzor nad tim postupcima te nadzor i mjere koje se provode na lokacijama nakon zbrinjavanja otpada, te radnje koje poduzimaju trgovac otpadom ili posrednik;</Text>
				<Text style={globalStyles.paragraph}>GRAĐEVINA ZA GOSPODARENJE OTPADOM je građevina za sakupljanje otpada (skladište otpada, pretovarna stanica i reciklažno dvorište), građevina za obradu otpada i centar za gospodarenje otpadom. Ne smatra se građevinom za gospodarenje otpadom građevina druge namjene u kojoj se obavlja djelatnost oporabe otpada;</Text>
				<Text style={globalStyles.paragraph}>GRAĐEVNI OTPAD je otpad nastao prilikom gradnje građevina, rekonstrukcije, uklanjanja i održavanja postojećih građevina, te otpad nastao od iskopanog materijala, koji se ne može bez prethodne oporabe koristiti za građenje građevine zbog kojeg građenja je nastao;</Text>
				<Text style={globalStyles.paragraph}>INTERAKTIVNO SAKUPLJANJE OTPADA je sakupljanje otpada uređajima i opremom u svrhu hitnog uklanjanja otpada s određene lokacije radi sprječavanja nastanka i/ili smanjenja na najmanju moguću mjeru onečišćenja okoliša, ugrožavanja ljudskog zdravlja, uzrokovanja šteta biljnom i životinjskom svijetu i drugih šteta;</Text>
				<Text style={globalStyles.paragraph}>INTERNI OTPAD je otpad koji ne podliježe značajnim fizikalnim, kemijskim i/ili biološkim promjenama;</Text>
				<Text style={globalStyles.paragraph}>KRUPNI GLOMAZNI KOMUNALNI OTPAD je predmet ili tvar koju je zbog zapremine i/ili mase neprikladno prikupljati u sklopu usluge prikupljanja miješanog komunalnog otpada i određen je naputkom iz članka 29. stavka 11. ovoga Zakona;</Text>
				<Text style={globalStyles.paragraph}>KOMUNALNI OTPAD je otpad nastao u kućanstvu i otpad koji je po prirodi i sastavu sličan otpadu iz kućanstva, osim proizvodnog otpada i otpada iz poljoprivrede i šumarstva;</Text>
				<Text style={globalStyles.paragraph}>MATERIJALNA OPORBA je svaki postupak oporabe koji ne uključuje energetsku oporabu i preradu u materijale koji će se koristiti kao gorivo;</Text>
				<Text style={globalStyles.paragraph}>METODA je način izvođenja tehnološkog procesa na određenoj lokaciji određenom opremom, uređajem, vozilom i ljudstvom, uključujući upravljački nadzor izvođenja tehnološkog procesa;</Text>
				<Text style={globalStyles.paragraph}>MIJEŠANI KOMUNALNI OTPAD je otpad iz kućanstava i otpad iz trgovina, industrije i iz ustanova koji je po svojstvima i sastavu sličan otpadu iz kućanstava, iz kojeg posebnim postupkom nisu izdvojeni pojedini materijali (kao što je papir, staklo i dr.) te je u Katalogu otpada označen kao 20 03 01;</Text>
				<Text style={globalStyles.paragraph}>MOBILNI UREĐAJ ZA OBRADU OTPADA je pokretna tehnička jedinica u kojoj se otpad obrađuje, u pravilu, na mjestu nastanka ili na mjestu ugradnje u materijale postupcima obrade, osim R1, D1, D2, D3, D4, D5, D6, D7, D10, D11 i D12 i postupaka R i D u kojima nastaje otpadna voda koja se ispušta u okoliš. Mobilnim uređajem za obradu otpada ne smatra se onaj uređaj koji se koristi duže od šest mjeseci na određenoj lokaciji, osim mobilnog uređaja kojim se koristi radi sanacije onečišćene lokacije;</Text>
				<Text style={globalStyles.paragraph}>MORSKI OTPAD je otpad u morskom okolišu i obalnom području u neposrednom kontaktu s morem koji nastaje ljudskim aktivnostima na kopnu ili moru, a nalazi se na površini mora, u vodenom stupcu, na morskom dnu ili je naplavljen;</Text>
				<Text style={globalStyles.paragraph}>NASIPANJE OTPADA je postupak oporabe pri kojem se odgovarajući otpad koristi za nasipavanje iskopanih površina ili u tehničke svrhe pri krajobraznom uređenju i kojim se otpad koristi kao zamjena za materijal koji nije otpad sukladno ovom Zakonu i propisima donesenim na temelju ovoga Zakona;</Text>
				<Text style={globalStyles.paragraph}>NAJBOLJE RASPOLOŽIVE TEHNIKE su najbolje raspoložive tehnike sukladno zakonu kojim se uređuje zaštita okoliša;</Text>
				<Text style={globalStyles.paragraph}>NEOPASNI OTPAD je otpad koji ne posjeduje niti jedno od opasnih svojstava određenih Dodatkom III. ovoga Zakona;</Text>
				<Text style={globalStyles.paragraph}>NEUSKLAĐENO ODLAGALIŠTE je odlagalište koje ne ispunjava uvjete propisane pravilnikom iz članka 104. ovoga Zakona i određeno je odlukom iz članka 26. stavka 6. ovoga Zakona;</Text>
				<Text style={globalStyles.paragraph}>OBRADA OTPADA su postupci oporabe ili zbrinjavanja i postupci pripreme prije oporabe ili zbrinjavanja;</Text>
				<Text style={globalStyles.paragraph}>ODLAGALIŠTE OTPADA je građevina namijenjena odlaganju otpada na površinu ili pod zemlju (podzemno odlagalište), uključujući:</Text>
				<Text style={globalStyles.paragraph}>– interno odlagalište otpada na kojem proizvođač odlaže svoj otpad na samom mjestu proizvodnje,</Text>
				<Text style={globalStyles.paragraph}>– odlagalište otpada ili njegov dio koji se može koristiti za privremeno skladištenje otpada (npr. za razdoblje duže od jedne godine),</Text>
				<Text style={globalStyles.paragraph}>– iskorištene površinske kopove ili njihove dijelove nastale rudarskom eksploatacijom i/ili istraživanjem pogodne za odlaganje otpada</Text>
				<Text style={globalStyles.paragraph}>ODVOJENO SAKUPLJANJE je sakupljanje otpada na način da se otpad odvaja prema njegovoj vrsti i svojstvima kako bi se olakšala obrada i sačuvala vrijedna svojstva otpada;</Text>
				<Text style={globalStyles.paragraph}>OPASNI OTPAD je otpad koji posjeduje jedno ili više opasnih svojstava određenih Dodatkom III. ovoga Zakona;</Text>
				<Text style={globalStyles.paragraph}>OPORABA OTPADA je svaki postupak čiji je glavni rezultat uporaba otpada u korisne svrhe kada otpad zamjenjuje druge materijale koje bi inače trebalo uporabiti za tu svrhu ili otpad koji se priprema kako bi ispunio tu svrhu, u tvornici ili u širem gospodarskom smislu. U Dodatku II. ovoga Zakona sadržan je popis postupaka oporabe koji ne isključuje druge moguće postupke oporabe;</Text>
				<Text style={globalStyles.paragraph}>OTPAD je svaka tvar ili predmet koji posjednik odbacuje, namjerava ili mora odbaciti. Otpadom se smatra i svaki predmet i tvar čije su sakupljanje, prijevoz i obrada nužni u svrhu zaštite javnog interesa;</Text>
				<Text style={globalStyles.paragraph}>OTPADNA ULJA su mineralna ili sintetička ulja za podmazivanje ili industrijska ulja koja su postala neprikladna za uporabu za koju su prvobitno namijenjena, primjerice ulja iz motora s unutarnjim izgaranjem i ulja reduktora, ulja za podmazivanje, ulja za turbine i hidraulička ulja;</Text>
				<Text style={globalStyles.paragraph}>OVLAŠTENIK je pravna ili fizička osoba – obrtnik kojem je prema ovom Zakonu dana suglasnost za sklapanje ugovora s Fondom za zaštitu okoliša i energetsku učinkovitost u vezi gospodarenja posebnom kategorijom otpada;</Text>
				<Text style={globalStyles.paragraph}>PONOVNA UPORABA je svaki postupak kojim se omogućava ponovno korištenje proizvoda ili dijelova proizvoda, koji nisu otpad, u istu svrhu za koju su izvorno načinjeni;</Text>
				<Text style={globalStyles.paragraph}>POSJEDNIK OTPADA je proizvođač otpada ili pravna i fizička osoba koja je u posjedu otpada;</Text>
				<Text style={globalStyles.paragraph}>POSREDNIK je pravna ili fizička osoba – obrtnik koja obavlja djelatnost posredovanja u gospodarenju otpadom, uključujući i posrednika koji ne preuzima otpad u neposredni posjed;</Text>
				<Text style={globalStyles.paragraph}>POSTUPCI GOSPODARENJA OTPADOM su: sakupljanje otpada, interventno sakupljanje otpada, priprema za ponovnu uporabu, priprema prije oporabe i zbrinjavanja, postupci oporabe i zbrinjavanja, trgovanje otpadom, posredovanje u gospodarenju otpadom, prijevoz otpada, energetska oporaba određenog otpada, sakupljanje otpada u reciklažno dvorište i privremeno skladištenje vlastitog proizvodnog otpada;</Text>
				<Text style={globalStyles.paragraph}>POTAPANJE OTPADA je postupak zbrinjavanja otpada koji uključuje odlaganje otpada s plovnih objekata ili zrakoplova u more te odlaganje, skladištenje ili ukopavanje otpada s plovnih objekata ili zrakoplova na morsko dno ili u morsko podzemlje;</Text>
				<Text style={globalStyles.paragraph}>PRETHODNA SUGLASNOST je obavijest kojom nadležno tijelo države polazišta najavljuje prekogranični promet otpada koji podliježe notifikacijskom postupku;</Text>
				<Text style={globalStyles.paragraph}>PRETOVARNA STANICA (transfer stanica) je građevina za skladištenje, pripremu i pretovar otpada namijenjenog prijevozu prema mjestu njegove oporabe ili zbrinjavanja;</Text>
				<Text style={globalStyles.paragraph}>PRIPREMA ZA PONOVNU UPORABU su postupci oporabe kojima se proizvodi ili dijelovi proizvoda koji su postali otpad provjerom, čišćenjem ili popravkom, pripremaju za ponovnu uporabu bez dodatne prethodne obrade;</Text>
				<Text style={globalStyles.paragraph}>PROBLEMATIČNI OTPAD je opasni otpad iz podgrupe 20 01 Kataloga otpada koji uobičajeno nastaje u kućanstvu te opasni otpad koji je po svojstvima, sastavu i količini usporediv s opasnim otpadom koji uobičajeno nastaje u kućanstvu pri čemu se problematičnim otpadom smatra sve dok se nalazi kod proizvođača tog otpada;</Text>
				<Text style={globalStyles.paragraph}>PROIZVODNI OTPAD je otpad koji nastaje u proizvodnom procesu u industriji, obrtu i drugim procesima, osim ostataka iz proizvodnog procesa koji se koriste u proizvodnom procesu istog proizvođača;</Text>
				<Text style={globalStyles.paragraph}>PROIZVOĐAČ OTPADA je svaka osoba čijom aktivnošću nastaje otpad i/ili koja prethodnom obradom, miješanjem ili drugim postupkom mijenja sastav ili svojstva otpada;</Text>
				<Text style={globalStyles.paragraph}>PROIZVOĐAČ PROIZVODA je pravna ili fizička osoba – obrtnik koja na profesionalnoj osnovi razvija, proizvodi, prerađuje, obrađuje, prodaje, unosi ili uvozi, odnosno stavlja na tržište proizvode i/ili uređaje i/ili opremu;</Text>
				<Text style={globalStyles.paragraph}>RECIKLAŽNO DVORIŠTE je nadzirani ograđeni prostor namijenjen odvojenom prikupljanju i privremenom skladištenju manjih količina posebnih vrsta otpada;</Text>
				<Text style={globalStyles.paragraph}>RECIKLAŽNO DVORIŠTE ZA GRAĐEVNI OTPAD je građevina namijenjena razvrstavanju, mehaničkoj obradi i privremenom skladištenju građevnog otpada;</Text>
				<Text style={globalStyles.paragraph}>RECIKLIRANJE je svaki postupak oporabe, uključujući ponovnu preradu organskog materijala, kojim se otpadni materijali prerađuju u proizvode, materijale ili tvari za izvornu ili drugu svrhu osim uporabe otpada u energetske svrhe, odnosno prerade u materijal koji se koristi kao gorivo ili materijal za zatrpavanje;</Text>
				<Text style={globalStyles.paragraph}>REGENERACIJA OTPADNIH ULJA označava svaki postupak oporabe kojim se bazna ulja mogu proizvesti rafiniranjem otpadnih ulja, posebno uklanjanjem nečistoća, proizvoda oksidacije i aditiva sadržanih u takvim uljima;</Text>
				<Text style={globalStyles.paragraph}>SAKUPLJANJE OTPADA je prikupljanje otpada, uključujući prethodno razvrstavanje otpada i skladištenje otpada u svrhu prijevoza na obradu;</Text>
				<Text style={globalStyles.paragraph}>SKLADIŠTENJE OTPADA je privremeni smještaj otpada u skladištu najduže do godinu dana;</Text>
				<Text style={globalStyles.paragraph}>SPALJIVANJE OTPADA je postupak oporabe, odnosno zbrinjavanja otpada u kojem se spaljuje otpad sa ili bez oporabe topline proizvedene izgaranjem. To uključuje oksidacijsko spaljivanje otpada, kao i druge termičke procese, poput pirolize, rasplinjavanja ili plazma procesa, sve dok se rezultirajući produkti tih obrada nakon toga spaljuju;</Text>
				<Text style={globalStyles.paragraph}>SPRJEČAVANJE NASTANKA OTPADA su mjere poduzete prije nego li je tvar, materijal ili proizvod postao otpad, a kojima se smanjuju:</Text>
				<Text style={globalStyles.paragraph}>– količine otpada uključujući ponovnu uporabu proizvoda ili produženje životnog vijeka proizvoda,</Text>
				<Text style={globalStyles.paragraph}>– štetan učinak otpada na okoliš i zdravlje ljudi ili</Text>
				<Text style={globalStyles.paragraph}>– sadržaj štetnih tvari u materijalima i proizvodima.</Text>
				<Text style={globalStyles.paragraph}>STAVLJANJE NA TRŽIŠTE je svaki postupak proizvođača proizvoda kojim određeni proizvod čini dostupan kupcu, neovisno o načinu prodaje na teritoriju Republike Hrvatske,</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>SUSPALJIVANJE OTPADA je postupak oporabe, odnosno zbrinjavanja otpada čija je prvenstvena svrha proizvodnja energije ili materijalnih produkata (proizvoda) i u kojem se otpad koristi kao redovno ili dopunsko gorivo ili u kojem se otpad termički obrađuje radi zbrinjavanja. To uključuje oksidacijsko spaljivanje otpada, kao i druge termičke procese, poput pirolize, rasplinjavanja ili plazma procesa, sve dok se rezultirajući produkti tih obrada nakon toga spaljuju;</Text>
			</Content>
		);
	}
}

const styles = {
	textStyle: {
		fontSize: 16
	}
};

export default PojmovnikScreen;
