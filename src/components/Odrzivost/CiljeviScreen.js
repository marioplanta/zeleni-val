import React, { Component } from "react";
import {
	View,
	Text,
	Image
} from "react-native";
import {
	Content
} from 'native-base';
import { Table, Row, Rows } from 'react-native-table-component';
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';

const tableHead = ['', 'Do 2025.', 'Do 2030.', 'Do 2035.'];
const tableData = [
	['Komunalni otpad koji se reciklira ili ponovno uporabljuje', '55%', '60%', '65%']
];
const table2Head = ['', 'Do 2025.', 'Do 2030.'];
const table2Data = [
	['Sve vrste ambalaže', '65%', '70%'],
	['Plastika', '50%', '55%'],
	['Drvo', '25%', '30%'],
	['Metali koji sadrže željezo', '70%', '80%'],
	['Aluminij', '50%', '60%'],
	['Staklo', '70%', '75%'],
	['Papir i karton', '75%', '85%']
];

class CiljeviScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Content padder>
				<Text style={globalStyles.subtitle}>EU ciljevi</Text>
				<Text style={globalStyles.paragraph}>Države članice morat će ostvariti sljedeće ciljeve kako se budu više koristile ponovnom uporabom i recikliranjem komunalnog otpada:</Text>

				<Table style={{ marginBottom: 20 }} borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
					<Row data={tableHead} style={styles.head} textStyle={styles.text}/>
					<Rows data={tableData} textStyle={styles.text}/>
				</Table>

				<Text style={globalStyles.paragraph}>Države članice do 1. siječnja 2025. uspostavit će odvojeni sustav prikupljanja tekstila i opasnog otpada iz kućanstava. Osim toga, do 31. prosinca 2023. osigurat će da se biootpad ili prikuplja odvojeno ili reciklira na izvoru (npr. kompostiranje u domaćinstvu). To je dodatan zahtjev uz onaj koji već postoji o odvojenom prikupljanju papira i kartona, stakla, metala i plastike.</Text>
				<Text style={globalStyles.paragraph}>Zakonodavstvom se utvrđuju određeni ciljevi za recikliranje ambalaža:</Text>
				<Table style={{ marginBottom: 20 }} borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
					<Row data={table2Head} style={styles.head} textStyle={styles.text}/>
					<Rows data={table2Data} textStyle={styles.text}/>
				</Table>

				<Text style={globalStyles.paragraph}>Zakonodavstvom je obuhvaćen cilj u vezi sa smanjenjem odlaganja otpada na odlagalištima te se njime utvrđuju minimalni zahtjevi za sve programe proširene odgovornosti proizvođača. Proizvođači proizvoda obuhvaćeni tim programima moraju snositi odgovornost za gospodarenje fazom otpada koji nastaje od njihovih proizvoda te će se od njih tražiti financijski doprinosi. Također su uvedeni obvezni programi proširene odgovornosti proizvođača za svu ambalažu. Države članice nastoje osigurati da se sav otpad koji se može podvrgnuti recikliranju ili drugom postupku oporabe, osobito komunalni otpad, od 2030. ne prima na odlagališta.</Text>
				<Text style={globalStyles.subtitle}>Ciljevi prema Planu gospodarenja otpadom RH OD 2017.-2022.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/ciljevi1-1.png') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>

				<Text style={globalStyles.subtitle}>Ciljevi prema Planu gospodarenja otpadom Grada Ploča 2017.-2022.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/ciljevi2-1.png') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
			</Content>
		);
	}
}

const styles = {
	container: {
		flex: 1,
		padding: 16,
		paddingTop: 30,
		backgroundColor: '#fff'
	},
	head: {
		height: 40,
		backgroundColor: '#f1f8ff'
	},
	text: {
		margin: 6
	}
};

export default CiljeviScreen;
