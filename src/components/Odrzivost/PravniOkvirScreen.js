import React, { Component } from "react";
import {
	View,
	Text,
	Button,
	Linking
} from "react-native";
import {
	Content
} from 'native-base';

import globalStyles from './../../assets/styles/Style';

class PravniOkvirScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { linkStyle } = styles;

		return (
			<Content padder>
				<Text style={[ globalStyles.subtitle, { marginTop: 0 } ]}>PRAVNI PROPISI</Text>
				<Text style={globalStyles.paragraph}>Zakon o održivom gospodarenju otpadom:</Text>
				<Button style={linkStyle} title="NN 94/13" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2013_07_94_2123.html') } } />
				<Button style={linkStyle} title="NN 73/17" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2017_07_73_1767.html') } } />
				<Button style={linkStyle} title="NN 14/19" onPress={() => { Linking.openURL('https://narodne-novine.nn.hr/clanci/sluzbeni/2019_02_14_277.html') } } />
				
				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Pravilnik o katalogu otpada:</Text>
				<Button style={linkStyle} title="NN 90/15" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2015_08_90_1757.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Pravilnik o načinima i uvjetima odlaganja otpada, kategorijama i uvjetima rada za odlagališta otpada:</Text>
				<Button style={linkStyle} title="NN 114/15" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2015_10_114_2184.html') } } />
				<Button style={linkStyle} title="NN 103/18" onPress={() => { Linking.openURL('https://narodne-novine.nn.hr/clanci/sluzbeni/2018_11_103_2020.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Uredba o gospodarenju komunalnim otpadom:</Text>
				<Button style={linkStyle} title="NN 50/17" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2017_05_50_1138.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Pravilnik o gospodarenju otpadom:</Text>
				<Button style={linkStyle} title="NN 117/17" onPress={() => { Linking.openURL('https://narodne-novine.nn.hr/clanci/sluzbeni/full/2017_11_117_2708.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Naputak o glomaznom otpadu:</Text>
				<Button style={linkStyle} title="NN 79/15" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2015_07_79_1534.html') } } />
			
				<Text style={globalStyles.subtitle}>STRATEGIJE I PLANOVI</Text>
				<Text style={globalStyles.paragraph}>Strategija gospodarenja otpadom Republike Hrvatske:</Text>
				<Button style={linkStyle} title="NN 130/05" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2005_11_130_2398.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Plan gospodarenja otpadom Republike Hrvatske za razdoblje 2017. – 2022. godine:</Text>
				<Button style={linkStyle} title="NN 3/17" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2017_01_3_120.html') } } />

				<Button style={linkStyle} title="Odluka o implementaciji Plana gospodarenja otpadom Republike Hrvatske za razdoblje 2017.-2022. godine" onPress={() => { Linking.openURL('https://mzoe.gov.hr/UserDocsImages/UPRAVA-ZA-PROCJENU-UTJECAJA-NA-OKOLIS-ODRZIVO-GOSPODARENJE-OTPADOM/Sektor%20za%20održivo%20gospodarenje%20otpadom/Ostalo/Odluka%20o%20implementaciji%20PGO%20RH%20%202017_2022.pdf') } } />

				<Button style={linkStyle} title="Plan gospodarenja otpadom Grada Ploča za razdoblje 2017.-2022. godine" onPress={() => { Linking.openURL('http://komunalno.ploce.hr/wp-content/uploads/2019/03/2019-03-21-plan-gospodarnja-otpadom.pdf') } } />

				<Button style={linkStyle} title="Dinamika zatvaranja odlagališta" onPress={() => { Linking.openURL('https://mzoe.gov.hr/UserDocsImages/UPRAVA-ZA-PROCJENU-UTJECAJA-NA-OKOLIS-ODRZIVO-GOSPODARENJE-OTPADOM/Ostali%20dokumenti/Dinamika%20i%20redoslijed%20zatvaranja%20odlagalista%20neopasnog%20otpada%20na%20podrucju%20RH%20web%20mzoe%20korigirano%204_2_2019.doc') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Odluka o redoslijedu i dinamici zatvaranja odlagališta:</Text>
				<Button style={linkStyle} title="NN 3/19" onPress={() => { Linking.openURL('https://narodne-novine.nn.hr/clanci/sluzbeni/2019_01_3_65.html') } } />
				<Button style={linkStyle} title="NN 17/19" onPress={() => { Linking.openURL('https://narodne-novine.nn.hr/clanci/sluzbeni/2019_02_17_385.html') } } />

				<Button style={linkStyle} title="Europska strategija za plastiku u kružnom gospodarstvu" onPress={() => { Linking.openURL('https://eur-lex.europa.eu/resource.html?uri=cellar:2df5d1d2-fac7-11e7-b8f5-01aa75ed71a1.0004.02/DOC_1&format=PDF') } } />
			
				<Text style={globalStyles.subtitle}>POSEBNE KATEGORIJE OTPADA</Text>
				<Text style={globalStyles.paragraph}>Strategija gospodarenja otpadom Republike Hrvatske:</Text>
				<Button style={linkStyle} title="NN 124/06" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2006_11_124_2762.html') } } />
				<Button style={linkStyle} title="NN 121/08" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2008_10_121_3482.html') } } />
				<Button style={linkStyle} title="NN 31/09" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2009_03_31_682.html') } } />
				<Button style={linkStyle} title="NN 156/09" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2009_12_156_3896.html') } } />
				<Button style={linkStyle} title="NN 91/11" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2011_08_91_1946.html') } } />
				<Button style={linkStyle} title="NN 45/12" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2012_04_45_1127.html') } } />
				<Button style={linkStyle} title="NN 86/13" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2013_07_86_1933.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Pravilnik o gospodarenju medicinskim otpadom:</Text>
				<Button style={linkStyle} title="NN 50/15" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2015_05_50_989.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Pravilnik o ambalaži i otpadnoj ambalaži:</Text>
				<Button style={linkStyle} title="NN 88/15" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2015_08_88_1735.html') } } />
				<Button style={linkStyle} title="NN 78/16" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2016_08_78_1795.html') } } />
				<Button style={linkStyle} title="NN 116/17" onPress={() => { Linking.openURL('https://narodne-novine.nn.hr/clanci/sluzbeni/full/2017_11_116_2685.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Uredba o gospodarenju otpadnom ambalažom:</Text>
				<Button style={linkStyle} title="NN 97/15" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2015_09_97_1872.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Pravilnik o gospodarenju otpadnim tekstilom i otpadnom obućom:</Text>
				<Button style={linkStyle} title="NN 99/15" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2015_09_99_1933.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Pravilnik o baterijama i akumulatorima i otpadnim baterijama i akumulatorima:</Text>
				<Button style={linkStyle} title="NN 111/15" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2015_10_111_2147.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Pravilnik o gospodarenju otpadnim baterijama i akumulatorima:</Text>
				<Button style={linkStyle} title="NN 133/06" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/128770.html') } } />
				<Button style={linkStyle} title="NN 31/09" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2009_03_31_686.html') } } />
				<Button style={linkStyle} title="NN 156/09" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2009_12_156_3894.html') } } />
				<Button style={linkStyle} title="NN 45/12" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2012_04_45_1128.html') } } />
				<Button style={linkStyle} title="NN 86/13" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2013_07_86_1932.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Pravilnik o gospodarenju otpadnim vozilima:</Text>
				<Button style={linkStyle} title="NN 125/15" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2015_11_125_2384.html') } } />
				<Button style={linkStyle} title="NN 90/16" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2016_10_90_1914.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Uredba o gospodarenju otpadnim vozilima:</Text>
				<Button style={linkStyle} title="NN 112/15" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2015_10_112_2161.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Pravilnik o građevnom otpadu i otpadu koji sadrži azbest:</Text>
				<Button style={linkStyle} title="NN 69/16" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2016_07_69_1650.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Pravilnik o gospodarenju otpadnim gumama:</Text>
				<Button style={linkStyle} title="NN 113/16" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2016_12_113_2493.html') } } />

				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Pravilnik o gospodarenju muljem iz uređaja za pročišćavanje otpadnih voda kada se mulj koristi u poljoprivredi:</Text>
				<Button style={linkStyle} title="NN 38/08" onPress={() => { Linking.openURL('http://narodne-novine.nn.hr/clanci/sluzbeni/2008_04_38_1307.html') } } />
			</Content>
		);
	}
}

const styles = {
	linkStyle: {
		textAlign: 'left',
		alignItems: 'flex-start',
		justifyContent: 'flex-start'
	}
};

export default PravniOkvirScreen;
