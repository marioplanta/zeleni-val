import React, { Component } from "react";
import {
	View,
	Text,
	Image
} from "react-native";
import {
	Content,
	Icon
} from 'native-base';
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';

class OTemiScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { textStyle } = styles;

		return (
			<Content padder>
				<Text style={globalStyles.paragraph}>Otpad je svaka tvar ili predmet koji posjednik odbacuje, namjerava ili mora odbaciti. Otpadom se smatra i svaki predmet i tvar čije su sakupljanje, prijevoz i obrada nužni u svrhu zaštite javnog interesa.</Text>
				<Text style={globalStyles.paragraph}>Načela održivog gospodarenja otpadom</Text>
				<Text style={globalStyles.paragraph}>“načelo onečišćivač plaća” – proizvođač otpada, prethodni posjednik otpada, odnosno posjednik otpada snosi troškove mjera gospodarenja otpadom, te je financijski odgovoran za provedbu sanacijskih mjera zbog štete koju je prouzročio ili bi je mogao prouzročiti otpad,</Text>
				<Text style={globalStyles.paragraph}>“načelo blizine” – obrada otpada mora se obavljati u najbližoj odgovarajućoj građevini ili uređaju u odnosu na mjesto nastanka otpada, uzimajući u obzir gospodarsku učinkovitost i prihvatljivost za okoliš,</Text>
				<Text style={globalStyles.paragraph}>“načelo samodostatnosti” – gospodarenje otpadom će se obavljati na samodostatan način omogućavajući neovisno ostvarivanje propisanih ciljeva na razini države, a uzimajući pri tom u obzir zemljopisne okolnosti ili potrebu za posebnim građevinama za posebne kategorije otpada i</Text>
				<Text style={globalStyles.paragraph}>“načelo sljedivosti” – utvrđivanje porijekla otpada s obzirom na proizvod, ambalažu i proizvođača tog proizvoda kao i posjed tog otpada uključujući i obradu.</Text>
				<Text style={globalStyles.paragraph}>Proizvođač proizvoda od kojeg nastaje otpad, odnosno proizvođač otpada snosi troškove gospodarenja tim otpadom.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/piramida-2.png') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={globalStyles.paragraph}>U svrhu sprječavanja nastanka otpada te primjene propisa i politike gospodarenja otpadom primjenjuje se red prvenstva gospodarenja otpadom, i to:</Text>
				<Text style={globalStyles.paragraphNoMargin}>1) sprječavanje nastanka otpada,</Text>
				<Text style={globalStyles.paragraphNoMargin}>2) priprema za ponovnu uporabu,</Text>
				<Text style={globalStyles.paragraphNoMargin}>3) Recikliranje i kompostiranje</Text>
				<Text style={globalStyles.paragraphNoMargin}>4) drugi postupci oporabe npr. energetska oporaba i</Text>
				<Text style={globalStyles.paragraph}>5) Zbrinjavanje otpada (odlaganje)</Text>
				<Text style={globalStyles.paragraph}>Prema redu prvenstva gospodarenja otpadom prioritet je sprječavanje nastanka otpada, potom slijedi priprema za ponovnu uporabu, zatim recikliranje pa drugi postupci oporabe, dok je postupak zbrinjavanja otpada, koji uključuje i odlaganje otpada, najmanje poželjan postupak gospodarenja otpadom.</Text>
				<Text style={globalStyles.paragraph}>U RH još uvijek se gospodari otpadom na način da se najviše odlaže, a najmanje se provode mjere sprječavanje nastanka otpadom. Zbog toga je nužno da se promijeni hijerarhija (obrne piramida), jer ćemo se inače zatrpavati otpadom sve više i više.</Text>

				<View style={{ flex: 1, flexDirection: 'row', marginBottom: 15 }}>
					<Icon name="ios-alert" style={globalStyles.iconStyle} />
					<Text style={{ flex: 1, paddingHorizontal: 15, fontSize: 16 }}>U hrvatskoj je preko 80 % komunalnog otpada završavalo na odlagalištima čime je u samo 10 godina izgubljeno sirovina u vrijednosti preko 5 milijardi kuna.</Text>
				</View>

				<Text style={globalStyles.paragraph}>Gospodarenje otpadom provodi se na način koji ne dovodi u opasnost ljudsko zdravlje i koji ne dovodi do štetnih utjecaja na okoliš, a osobito kako bi se izbjeglo sljedeće:</Text>
				<Text style={globalStyles.paragraphNoMargin}>• rizik od onečišćenja mora, voda, tla i zraka te ugrožavanja biološke raznolikosti,</Text>
				<Text style={globalStyles.paragraphNoMargin}>• pojava neugode uzorkovane bukom i/ili mirisom,</Text>
				<Text style={globalStyles.paragraphNoMargin}>• štetan utjecaj na područja kulturno-povijesnih, estetskih i prirodnih vrijednosti te drugih vrijednosti koje su od posebnog interesa,</Text>
				<Text style={globalStyles.paragraph}>• nastajanje eksplozije ili požara.</Text>

				<Text style={globalStyles.subtitle}>Gospodarenjem otpadom mora osigurati da otpad koji preostaje nakon postupaka obrade i koji se zbrinjava odlaganjem ne predstavlja opasnost za buduće generacije.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/shema-1.png') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
			</Content>
		);
	}
}

const styles = {
	textStyle: {
		fontSize: 16
	}
};

export default OTemiScreen;
