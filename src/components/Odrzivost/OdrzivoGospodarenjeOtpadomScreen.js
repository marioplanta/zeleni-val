import React, { Component } from "react";
import {
	View,
	Text,
	Image,
	FlatList,
	TouchableWithoutFeedback
} from "react-native";
import {
	Content,
	ListItem
} from 'native-base';
import { connect } from "react-redux";
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';
import { fetchNews } from "../../actions";

class OdrzivoGospodarenjeOtpadomScreen extends Component {
	constructor(props) {
		super(props);
	}

	componentWillMount() {
		this.props.fetchNews();
	}

	renderItem = ({ item }) => {
		if(item.categories.includes(22)) {
			var excerpt = item.excerpt['rendered'].replace(/<\/?[^>]+(>|$)/g, "");

			excerpt = excerpt.replace('[vc_row]','');
			excerpt = excerpt.replace('[vc_column]','');
			excerpt = excerpt.replace('[vc_column_text]','');
			excerpt = excerpt.replace('[/vc_row]','');
			excerpt = excerpt.replace('[/vc_column]','');
			excerpt = excerpt.replace('[/vc_column_text]','');
			excerpt = excerpt.replace('[&hellip;]','');
			excerpt = excerpt.replace('&nbsp;','');

			var title = item.title['rendered'].replace('&#8211;', '-');

			return (
				<TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('SingleVijestScreen', {
					title: item.title['rendered'],
					content: item.content['rendered'],
					image: item._embedded['wp:featuredmedia'][0].source_url
				})}>
					<View style={{ flexDirection: 'column', marginTop: 15 }}>
						<Image style={{ width: '100%', height: 200 }} source={{ uri: item._embedded['wp:featuredmedia'][0].source_url }} />
						<Text style={{ fontWeight: 'bold', marginVertical: 10, fontSize: 18 }}>{title}</Text>
						<Text style={{ margin: 0 }}>{excerpt}</Text>
					</View>
				</TouchableWithoutFeedback>
			);
		}
	};

	render() {
		const { textStyle } = styles;

		return (
			<Content padder>
				<Text style={globalStyles.tagline}>Održiva budućnost</Text>
				<Text style={textStyle, globalStyles.title}>Zeleni val</Text>
				<Text style={globalStyles.paragraph}>Cilj projekta je aktivno uključivanje stanovništva i turista u sustav održivog gospodarenja otpadom, odnosno smanjenje nastanka i povećanje odvojeno prikupljenog otpada, a time smanjenje odloženog otpada na odlagalište Lovornik. Aktivnosti ovoga projekta usmjerene su uspostavljanju okolišno i ekonomski održivog sustava gospodarenja otpadom.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/piramida.png') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>

				<Text style={[ globalStyles.subtitle, { marginTop: 20, fontSize: 20 } ]}>Najnovije vijesti</Text>
				<FlatList
					data={this.props.news}
					renderItem={this.renderItem}
					keyExtractor={item => item.id}
				/>
			</Content>
		);
	}
}

const styles = {
	textStyle: {
		fontSize: 16
	}
};

const mapStateToProps = ({ newsReducer }) => {
	const { news, loading } = newsReducer;

	return {
		news,
		loading
	};
};

export default connect(mapStateToProps, {
	fetchNews
})(OdrzivoGospodarenjeOtpadomScreen);
