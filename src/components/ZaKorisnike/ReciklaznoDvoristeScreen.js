import React, { Component } from "react";
import {
	View,
	Text
} from "react-native";
import {
	Content
} from 'native-base';
import YouTube from 'react-native-youtube';

import globalStyles from './../../assets/styles/Style';

class ReciklaznoDvoristeScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Content padder>
				<Text style={[ globalStyles.subtitle, { marginTop: 0 } ]}>Što je Reciklažno dvorište?</Text>
				<Text style={globalStyles.paragraph}>Reciklažno dvorište je nadzirani ograđeni prostor namijenjen odvojenom prikupljanju i privremenom skladištenju manjih količina posebnih vrsta otpada nastalih u kućanstvu.</Text>
				
				<Text style={globalStyles.subtitle}>Radno vijeme:</Text>
				<Text style={globalStyles.paragraph}>ponedjeljak, srijeda i petak: 07:00 – 14:00</Text>
				<Text style={globalStyles.paragraph}>utorak i četvrtak: 14:00 – 19:00</Text>
				<Text style={globalStyles.paragraph}>subota: 07:00 – 12:00</Text>

				<Text style={globalStyles.subtitle}>Kako do reciklažnog?</Text>
				<Text style={globalStyles.paragraph}>Reciklažno dvorište u Pločama nalazi se na adresi Dalmatinska 5A.</Text>
			

				<Text style={globalStyles.subtitle}>Što vam treba?</Text>
				<Text style={globalStyles.paragraph}>Osobna iskaznica, kako bi utvrdili pravo korištenja reciklažnog dvorišta.</Text>

				<Text style={globalStyles.subtitle}>Kako funkcionira?</Text>
				<Text style={globalStyles.paragraph}>Otpad koji dovozite u reciklažno mora biti odvojen po vrstama. Svaka vrsta će se posebno izvagati, a Radnik u reciklažnom će Vas uputiti u koji spremnik koju vrstu trebate odložiti.</Text>
				<Text style={globalStyles.paragraph}>Korištenje reciklažnog dvorišta je u potpunosti besplatno za stanovnike Ploča i Gradca za otpad koji je nastao u kućanstvu.</Text>
				
				<Text style={globalStyles.subtitle}>Koje vrste otpada možete dovesti?</Text>

				<Text style={globalStyles.paragraph}>Za više informacija:</Text>
				<YouTube
					videoId="sjtPfNOtmqk"
					play={false}
					fullscreen
					style={{ alignSelf: 'stretch', height: 300, marginBottom: 15 }}
				/>
				<YouTube
					videoId="iBfD7_UwOa8"
					play={false}
					fullscreen
					style={{ alignSelf: 'stretch', height: 300, marginBottom: 15 }}
				/>
				<YouTube
					videoId="U-XFrZlfcv0"
					play={false}
					fullscreen
					style={{ alignSelf: 'stretch', height: 300, marginBottom: 15 }}
				/>
				<YouTube
					videoId="n4aZAoV1tyQ"
					play={false}
					fullscreen
					style={{ alignSelf: 'stretch', height: 300, marginBottom: 15 }}
				/>
			</Content>
		);
	}
}

export default ReciklaznoDvoristeScreen;
