import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Content,
	Body,
	Icon,
	Left,
	Right,
	Title,
	Text,
	Button,
} from "native-base";
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../../assets/styles/Style';

class JavniSpremniciScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" style={globalStyles.backButtonIcon} />
							<Text style={globalStyles.backButtonText}>Natrag</Text>
						</Button>
					</Left>
					<Body>
						<Title>Spremnici na javnim površinama</Title>
					</Body>
					<Right/>
				</Header>
				<Content padder>
					<Text style={[ globalStyles.subtitle, { marginTop: 0 } ]}>GRAD PLOČE</Text>
					<Text style={globalStyles.paragraph}>Na području Grada Ploča organizirano se prikuplja miješani komunalni otpad (MKO), putem spremnika od 1100 L i 5 m3 postavljenih na javnim površinama. Dinamika odvoza miješanog komunalnog otpada je prikazana u tablici.</Text>
					<ResponsiveImageView source={ require('./../../../assets/images/dinamika-ploce.jpg') }>
						{({ getViewProps, getImageProps }) => (
							<View {...getViewProps()}>
								<Image {...getImageProps()} />
							</View>
						)}
					</ResponsiveImageView>
					<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Spremnici za papir i katon, plastiku i metal i staklo se prazne po zapunjenosti.</Text>

					<Text style={globalStyles.subtitle}>OPĆINA GRADAC</Text>
					<Text style={globalStyles.paragraphNoMargin}>U Zimskom periodu ( 1.listopada-30.svibnja):</Text>
					<Text style={globalStyles.paragraphNoMargin}>Spremnici za miješani komunalni otpad na javnim površinama se prazne 3 puta tjedno: ponedjeljkom, srijedom i petkom</Text>
					<Text style={globalStyles.paragraph}>Zeleni otoci se prazne svako drugu srijedu.</Text>

					<Text style={globalStyles.paragraphNoMargin}>U ljetnom periodu (1.lipnja-30.rujna):</Text>
					<Text style={globalStyles.paragraphNoMargin}>Spremnici za miješani komunalni otpad na javnim površinama se prazne svaki dan u tjednu (pon-ned), čak i praznicima.</Text>
					<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>Zeleni otoci se prazne svaku srijedu.</Text>
				</Content>
			</Container>
		);
	}
}

export default JavniSpremniciScreen;
