import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Content,
	Body,
	Icon,
	Left,
	Right,
	Title,
	Text,
	Button,
} from "native-base";
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../../assets/styles/Style';

class PilotProjektiScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" style={globalStyles.backButtonIcon} />
							<Text style={globalStyles.backButtonText}>Natrag</Text>
						</Button>
					</Left>
					<Body>
						<Title>Pilot projekti</Title>
					</Body>
					<Right/>
				</Header>
				<Content padder>
					<Text style={[ globalStyles.subtitle, { marginTop: 0 } ]}>Birina i Staševica</Text>
					<ResponsiveImageView source={ require('./../../../assets/images/stasevica.png') }>
						{({ getViewProps, getImageProps }) => (
							<View {...getViewProps()}>
								<Image {...getImageProps()} />
							</View>
						)}
					</ResponsiveImageView>
					<Text style={globalStyles.subtitle}>Komin</Text>
					<ResponsiveImageView source={ require('./../../../assets/images/letak-komin2.png') }>
						{({ getViewProps, getImageProps }) => (
							<View {...getViewProps()}>
								<Image {...getImageProps()} />
							</View>
						)}
					</ResponsiveImageView>

					<Text style={globalStyles.subtitle}>Drvenik</Text>
					<Text style={globalStyles.paragraph}>U zimskom periodu (1. listopada-30.svibnja):</Text>
					<Text style={globalStyles.paragraph}>Kante za miješani komunalni otpad prazne se ponedjeljkom i petkom.</Text>
					<Text style={globalStyles.paragraph}>Kante za reciklabilni otpad se prazne svako drugu srijedu.</Text>
					<Text style={globalStyles.paragraph}>U ljetnom periodu (1. lipnja- 30.rujna):</Text>
					<Text style={globalStyles.paragraph}>Kante za miješani komunalni otpad se prazne svaki dan osim srijedom (pon, uto, čet, pet, sub, ned).</Text>
					<Text style={globalStyles.paragraph}>Kante za reciklabilni otpad se prazne svaku srijedu.</Text>
				</Content>
			</Container>
		);
	}
}

export default PilotProjektiScreen;
