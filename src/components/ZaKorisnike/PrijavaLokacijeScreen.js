import React, { Component } from "react";
import {
	View,
	Alert,
	Image,
	PermissionsAndroid
} from "react-native";
import {
	Content,
	Button,
	Text,
	Icon
} from 'native-base';
import { connect } from "react-redux";
//import ImagePicker from "react-native-image-picker";
import ImagePicker from 'react-native-image-crop-picker';
import ResponsiveImageView from 'react-native-responsive-image-view';
import Geolocation from '@react-native-community/geolocation';

import { Input } from "./../common";
import globalStyles from './../../assets/styles/Style';
import { reportNameChanged, reportEmailChanged, reportLocationChanged, reportMessageChanged, sendReport } from "../../actions";

class PrijavaLokacijeScreen extends Component {
	constructor(props) {
		super(props);

		this.state = {
			images: [],
			report_sent: false
		};

		this.latitude = 0;
		this.longitude = 0;
	}

	componentWillMount() {
		Geolocation.getCurrentPosition(info => {
			this.latitude = info.coords.latitude;
			this.longitude = info.coords.longitude;
		});
	}

	componentDidUpdate(prevProps, prevState) {
		if(prevProps != this.props) {
			if(this.props.report_sent && this.props.report_sent != prevProps.report_sent) {
				this.setState({
					report_sent: true,
					images: []
				});
			}
		}
	}

	selectPhoto() {
		// ImagePicker.showImagePicker({title: "Odaberi fotografiju"}, (response) => {
		// 	if (response.didCancel) {
		// 		console.log("User cancelled image picker");
		// 	} else if (response.error) {
		// 		console.log("ImagePicker Error: ", response.error);
		// 	} else if (response.customButton) {
		// 		console.log("User tapped custom button: ", response.customButton);
		// 	} else {
		// 		this.setState({
		// 			image: response.uri
		// 		});
		// 	}
		// });

		ImagePicker.openPicker({
			multiple: true
		}).then(images => {
			for(var i = 0; i < images.length; i++) {
				this.setState({
					images: this.state.images.concat(images[i]['path'])
				});
			}
		});
	}

	onNameChange(text) {
		this.props.reportNameChanged(text);
	}

	onEmailChange(text) {
		this.props.reportEmailChanged(text);
	}

	onLocationChange(text) {
		this.props.reportLocationChanged(text);
	}

	onMessageChange(text) {
		this.props.reportMessageChanged(text);
	}

	onSendReport() {
		if(this.props.report_name.length == 0 || this.props.report_email.length == 0 || this.props.report_location.length == 0) {
			Alert.alert(
				'Nepotpuna forma',
				'Polja označena zvjezdicom su obvezna',
				[
					{text: 'OK'},
				],
				{cancelable: false},
			);
		} else {
			this.props.sendReport(this.props.report_name, this.props.report_email, this.props.report_location, this.props.report_message, this.state.images, this.latitude, this.longitude);
		}
	}

	renderImages() {
		if(this.state.images.length > 0) {
			return this.state.images.map((image) => {
				return (
					<Image style={{ width: '50%', height: 150, marginBottom: 15 }} source={{ uri: image }} />
				);
			});
		}
	}

	renderNote() {
		if(this.state.report_sent) {
			return (
				<View style={{ flex: 1, flexDirection: 'row', marginBottom: 20, padding: 10, borderWidth: 1, borderColor: '#019147' }}>
					<Icon name="ios-checkmark-circle" style={styles.cardIconGreen} />
					<Text style={{ color: '#019147' }}>Uspješno ste prijavili lokaciju nepropisno odbačenog otpada</Text>
				</View>
			);
		}
	}

	render() {
		return (
			<Content padder>
				<Text style={globalStyles.paragraph}>Sukladno odredbi čl. 36. st. 2. Zakona o održivom gospodarenju otpadom obveza Grada Ploča je uspostava sustava za zaprimanje obavijesti o nepropisno odbačenom otpadu, te uspostava sustava evidentiranja lokacija odbačenog otpada.</Text>
				<Text style={globalStyles.paragraph}>Ukoliko primijetite nepropisno odbačen otpad u okolišu, molimo Vas da prijavite lokaciju putem navedenog obrasca i prijave lokacije na karti. Ukoliko Vam je poznat počinitelj i ako imate bilo kakve spoznaje ili dokaze da se radi o toj osobi, možete prijaviti i počinitelja. Obrazac prijave možete pronaći ovdje, a prijavu možete ispuniti i putem online obrasca.</Text>

				<Text style={globalStyles.subtitle}>PRIJAVA LOKACIJA NEPROPISNO ODBAČENOG OTPADA</Text>

				<Input
					label="Ime i prezime *"
					onChangeText={this.onNameChange.bind(this)}
					autoCorrect={false}
					value={this.props.report_name}
					keyboardType={"default"}
				/>
				<Input
					label="E-mail *"
					onChangeText={this.onEmailChange.bind(this)}
					autoCorrect={false}
					value={this.props.report_email}
					keyboardType={"email-address"}
				/>
				<Input
					label="Lokacija otpada *"
					onChangeText={this.onLocationChange.bind(this)}
					autoCorrect={false}
					value={this.props.report_location}
					keyboardType={"default"}
				/>
				<Input
					label="Poruka"
					onChangeText={this.onMessageChange.bind(this)}
					autoCorrect={false}
					value={this.props.report_message}
					keyboardType={"default"}
					multiline = {true}
					numberOfLines = {4}
				/>
				<View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
					{this.renderImages()}
				</View>
				<View style={{ flex: 1, flexDirection: 'row' }}>
					<Button success style={{ marginBottom: 20, backgroundColor: '#019147' }} onPress={this.selectPhoto.bind(this)}><Text>Dodaj fotografiju</Text></Button>
				</View>
				{this.renderNote()}
				<View style={{ flex: 1, flexDirection: 'row' }}>
					<Button success style={{ marginBottom: 20, backgroundColor: '#019147' }} onPress={this.onSendReport.bind(this)}><Text>Pošalji</Text></Button>
				</View>

				<Text style={globalStyles.paragraph}>Apeliramo na građane da ne bacaju otpad u okoliš. Svaki korisnik javne usluge prikupljanja miješanog komunalnog otpada ima pravo na 1 besplatan odvoz glomaznog otpada godišnje u količini od 5 m3, a građani mogu dovesti besplatno otpad u reciklažno dvorište, o čemu više možete pročitati u izborniku reciklažno dvorište.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/lokacija.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
			</Content>
		);
	}
}

const styles = {
	cardIconGreen: {
		fontSize: 18,
		width: 15,
		color: "#019147",
		marginRight: 12
	}
};

const mapStateToProps = ({ reportReducer }) => {
	const { report_name, report_email, report_location, report_message, report_sent, loading } = reportReducer;

	return {
		report_name,
		report_email,
		report_location,
		report_message,
		report_sent,
		loading
	};
};

export default connect(mapStateToProps, {
	reportNameChanged, reportEmailChanged, reportLocationChanged, reportMessageChanged, sendReport
})(PrijavaLokacijeScreen);
