import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Content,
	Body,
	Icon,
	Left,
	Right,
	Title,
	Text,
	Button,
} from "native-base";
import MapView from 'react-native-maps';

import globalStyles from './../../../assets/styles/Style';

class GradPloceScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" style={globalStyles.backButtonIcon} />
							<Text style={globalStyles.backButtonText}>Natrag</Text>
						</Button>
					</Left>
					<Body>
						<Title>Općina Gradac</Title>
					</Body>
					<Right/>
				</Header>
				<Content padder>
					<Text style={[ globalStyles.subtitle, { marginTop: 0 } ]}>ZELENI OTOCI</Text>
					<Text style={globalStyles.paragraph}>U Općini Gradac postoji 6 zelenih otoka na kojima se nalaze spremnici za papir, staklo i plastiku i metal, a na pojedinim lokacijama postoji i spremnik za tekstil.</Text>
					<MapView
						style={{ flex: 1, height: 300, backgroundColor: 'red' }}
						initialRegion={{
							latitude: 37.78825,
							longitude: -122.4324,
							latitudeDelta: 0.0922,
							longitudeDelta: 0.0421,
						}}
					/>

					<Text style={globalStyles.subtitle}>PILOT PROJEKTI – DRVENIK</Text>
					<Text style={globalStyles.paragraph}>Na području Općine Gradac, pilot projekt „prikupljanja otpada od vrata do vrata“ provodi se u Drveniku. Svako kućanstvo je dobilo 2 kante- 1 za miješani komunalni otpad i 1 za otpad namijenjen recikliranju.</Text>
				</Content>
			</Container>
		);
	}
}

export default GradPloceScreen;
