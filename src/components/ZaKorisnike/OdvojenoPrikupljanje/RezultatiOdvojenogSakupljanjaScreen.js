import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Content,
	Body,
	Icon,
	Left,
	Right,
	Title,
	Text,
	Button,
} from "native-base";
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../../assets/styles/Style';

class RezultatiOdvojenogSakupljanjaScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" style={globalStyles.backButtonIcon} />
							<Text style={globalStyles.backButtonText}>Natrag</Text>
						</Button>
					</Left>
					<Body>
						<Title>Rezultati</Title>
					</Body>
					<Right/>
				</Header>
				<Content padder>
					<Text style={globalStyles.paragraph}>Male količine prikupljenog korisnog otpada se odvoze u privremeno iznajmljeno skladište  „Plobest“ gdje se ručno sortira i skladišti do preuzimanja od ovlaštenih sakupljača. Skladište je privremeno i koristi se do izgradnje sortirnice korisnog otpada u Pločama.</Text>
					<Text style={globalStyles.paragraph}>U 2018. smo odvojeno prikupili 209 t otpada (4 %), od čega je 124 t (2%) predano ovlaštenim sakupljačima na daljnju oporabu.</Text>

					<ResponsiveImageView source={ require('./../../../assets/images/otpad1.jpg') }>
						{({ getViewProps, getImageProps }) => (
							<View {...getViewProps()}>
								<Image {...getImageProps()} />
							</View>
						)}
					</ResponsiveImageView>
					<ResponsiveImageView source={ require('./../../../assets/images/otpad2.jpg') }>
						{({ getViewProps, getImageProps }) => (
							<View {...getViewProps()}>
								<Image {...getImageProps()} />
							</View>
						)}
					</ResponsiveImageView>

					<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Kada ćemo svi odvajati na kućnom pragu?</Text>
					<Text style={globalStyles.paragraph}>Nakon uspješne prijave na javni poziv, s Fondom za zaštitu okoliša i energetsku učinkovitost je potpisan Ugovor o nabavi spremnika za odvojeno prikupljanje otpada, ukupne vrijednosti 1.479.903,34 kn, od čega će sufinanciranje Grada Ploča iznositi 15 %. Ukupno će se nabaviti 1400 spremnika za papir i karton, 1400 spremnika za plastiku i 620 spremnika za biootpad, različitih zapremnina (80, 120 i 240 L). Nabava dodatnih spremnika, uz postojeće spremnike, omogućit će svim stanovnicima grada Ploča odvajanje otpada na mjestu nastanka, čime će se povećati odvojeno prikupljeni i reciklirani otpad te smanjiti količina odloženog otpada.</Text>
					<Text style={globalStyles.paragraph}>Fond za zaštitu okoliša i energetsku učinkovitost kao nositelj projekta raspisuje javnu nabavu za sve JLS koje su iskazale interes za spremnike. Kada Fond provede nabavu i spremnici budu dostavljeni, prikupljanje otpada na kućnom pragu zaživjet će na svim područjima grada Ploča.</Text>
					<Text style={globalStyles.paragraph}>Osim opreme koja je potrebna na terenu, za odvajanje i sortiranje većih količina otpada potrebno je izgraditi sortirnicu otpada koja je planirana Planom gospodarenja otpada Grada Ploča (2017.-2022.).</Text>
				</Content>
			</Container>
		);
	}
}

export default RezultatiOdvojenogSakupljanjaScreen;
