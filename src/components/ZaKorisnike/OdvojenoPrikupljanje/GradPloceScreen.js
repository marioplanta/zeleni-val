import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Content,
	Body,
	Icon,
	Left,
	Right,
	Title,
	Text,
	Button,
} from "native-base";
import ResponsiveImageView from 'react-native-responsive-image-view';
import MapView from 'react-native-maps';

import globalStyles from './../../../assets/styles/Style';

class GradPloceScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" style={globalStyles.backButtonIcon} />
							<Text style={globalStyles.backButtonText}>Natrag</Text>
						</Button>
					</Left>
					<Body>
						<Title>Grad Ploče</Title>
					</Body>
					<Right/>
				</Header>
				<Content padder>
					<Text style={[ globalStyles.subtitle, { marginTop: 0 } ]}>ZELENI OTOCI</Text>
					<Text style={globalStyles.paragraph}>Od 2016. na području grada Ploča postavljeno je 19 „eko otoka“, s po tri spremnika od 1100 l (1 za papir, 1 za staklo, 1 za plastiku i metal). Obično se ovaj otpad prikuplja jednom u 2 tjedna.</Text>
					<MapView
						style={{ flex: 1, height: 300, backgroundColor: 'red' }}
						initialRegion={{
							latitude: 37.78825,
							longitude: -122.4324,
							latitudeDelta: 0.0922,
							longitudeDelta: 0.0421,
						}}
					/>

					<Text style={globalStyles.subtitle}>PILOT PROJEKTI</Text>
					<Text style={globalStyles.paragraph}>Svako kućanstvo dobilo je po 1 kantu za miješani komunalni otpad i 1 za reciklabilni otpad (papir, plastika, staklo, metal).</Text>

					<ResponsiveImageView source={ require('./../../../assets/images/pilot-ploce.jpg') }>
						{({ getViewProps, getImageProps }) => (
							<View {...getViewProps()}>
								<Image {...getImageProps()} />
							</View>
						)}
					</ResponsiveImageView>
				</Content>
			</Container>
		);
	}
}

export default GradPloceScreen;
