import React, { Component } from "react";
import {
	View,
	Text
} from "react-native";
import {
	Content
} from 'native-base';

import globalStyles from './../../assets/styles/Style';

class AkcijePrikupljanjaOtpadaScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Content padder>
				<Text style={globalStyles.paragraph}>Pravna i fizička osoba – obrtnik može, u suradnji s osobom koja posjeduje važeću dozvolu za gospodarenje vrstom otpada koji će se prikupljati akcijom, organizirati akciju prikupljanja određenog otpada u svrhu provedbe sportskog, edukativnog, ekološkog ili humanitarnog sadržaja ako je ishodila suglasnost upravnog odjela jedinice lokalne samouprave nadležnog za poslove zaštite okoliša.</Text>
				<Text style={globalStyles.paragraph}>Zahtjev za suglasnost podnosi se najmanje dva mjeseca prije početka akcije.</Text>
				<Text style={globalStyles.paragraph}>Suglasnost određuje:</Text>
				<Text style={globalStyles.paragraphNoMargin}>1. vrijeme trajanja akcije,</Text>
				<Text style={globalStyles.paragraphNoMargin}>2. vrstu otpada koji se prikuplja,</Text>
				<Text style={globalStyles.paragraphNoMargin}>3. način, uvjete i svrhu provedbe akcije,</Text>
				<Text style={globalStyles.paragraph}>4. rok za dostavu izvješća o provedenoj akciji.</Text>
				<Text style={globalStyles.paragraph}>Suglasnost iz stavka 1. ovoga članka nije upravni akt.(5) Osoba kojoj je izdana suglasnost dužna je osigurati predaju prikupljenog otpada akcijom osobi koja posjeduje važeću dozvolu za gospodarenje vrstom otpada koji će se prikupljati akcijom.</Text>
				<Text style={globalStyles.paragraph}>Nadležni upravni odjel će najkasnije mjesec dana prije početka akcije osigurati dostavu podataka o akciji u informacijski sustav gospodarenja otpadom.</Text>
				<Text style={globalStyles.paragraph}>Trajanje akcije prikupljanja otpada ograničeno je na najviše 30 dana.</Text>
				<Text style={globalStyles.paragraph}>Osoba koja organizira akciju prikupljanja otpada smatra se vlasnikom prikupljenog otpada tijekom trajanja akcije.</Text>
				<Text style={globalStyles.paragraph}>Nadzor nad provedbom akcije prikupljanja otpada obavlja komunalni redar.</Text>
				<Text style={globalStyles.paragraph}>Osoba koja organizira akciju obvezna je u roku osam dana od završetka akcije  dostaviti nadležnom upravnom odjelu izvješće o provedenoj akciji.</Text>
				<Text style={globalStyles.paragraph}>Nadležni upravni odjel dužan je dostaviti izvješće o provedenim akcijama na svojem području Ministarstvu do 31. ožujka tekuće godine za prethodnu kalendarsku godinu.</Text>
				<Text style={globalStyles.paragraph}>Temeljem dostavljenih izvješća i stručne analize, Ministarstvo izrađuje i na svojim mrežnim stranicama objavljuje godišnje izvješće o provedenim akcijama u Republici Hrvatskoj.</Text>
			</Content>
		);
	}
}

export default AkcijePrikupljanjaOtpadaScreen;
