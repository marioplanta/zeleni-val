import React, { Component } from "react";
import {
	View,
	Text
} from "react-native";
import {
	Content
} from 'native-base';

import globalStyles from './../../assets/styles/Style';

class CestoPostavljanaPitanjaScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { textStyle } = styles;

		return (
			<Content padder>
				<Text style={[ globalStyles.subtitle, { marginTop: 0 } ]}>Kada će svako kućanstvo dobiti kante??</Text>
				<Text style={globalStyles.paragraph}>Nakon uspješne prijave na javni poziv, s Fondom za zaštitu okoliša i energetsku učinkovitost je potpisan Ugovor o nabavi spremnika za odvojeno prikupljanje otpada, ukupne vrijednosti 1.479.903,34 kn, od čega će sufinanciranje Grada Ploča iznositi 15 %. Ukupno će se nabaviti 1400 spremnika za papir i karton, 1400 spremnika za plastiku i 620 spremnika za biootpad, različitih zapremnina (80, 120 i 240 L). Nabava dodatnih spremnika, uz postojeće spremnike, omogućit će svim stanovnicima grada Ploča odvajanje otpada na mjestu nastanka, čime će se povećati odvojeno prikupljeni i reciklirani otpad te smanjiti količina odloženog otpada.</Text>
				<Text style={globalStyles.paragraph}>Fond za zaštitu okoliša i energetsku učinkovitost kao nositelj projekta raspisuje javnu nabavu za sve JLS koje su iskazale interes za spremnike. Kada Fond provede nabavu i spremnici budu dostavljeni, prikupljanje otpada na kućnom pragu zaživjet će na svim područjima grada Ploča.</Text>

				<Text style={globalStyles.subtitle}>Što se događa s odvojeno prikupljenim otpadom?</Text>
				<Text style={globalStyles.paragraph}>Otpad se dodatno ručno sortira u skladištu „Plobest“, a zatim se predaje ovlaštenim sakupljačima. Plastični otpad se balira i skladišti. Staklo se skladišti u jumbo vrećama. Potrebno je skupiti dovoljne količine za isplativost transporta.</Text>
			</Content>
		);
	}
}

const styles = {
	textStyle: {
		fontSize: 16
	}
};

export default CestoPostavljanaPitanjaScreen;
