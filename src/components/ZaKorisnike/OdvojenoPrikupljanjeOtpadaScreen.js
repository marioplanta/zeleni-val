import React, { Component } from "react";
import {
	View,
	Text,
	FlatList
} from "react-native";
import {
	Content,
	ListItem,
	Left,
	Right,
	Icon
} from 'native-base';

const screens = [
	{
		id: '0',
		title: "Grad Ploče",
		screen: "GradPloceScreen"
	},
	{
		id: '1',
		title: "Općina Gradac",
		screen: "OpcinaGradacScreen"
	},
	{
		id: '2',
		title: "Dosadašnji rezultati odvojenog sakupljanja otpada",
		screen: "RezultatiOdvojenogSakupljanjaScreen"
	}
];

class OdvojenoPrikupljanjeOtpadaScreen extends Component {
	constructor(props) {
		super(props);
	}

	renderItem = ({ item }) => {
		return (
			<ListItem onPress={() => this.props.navigation.navigate(item.screen)}>
				<Left>
					<Text>
						{item.title}
					</Text>
				</Left>
				<Right>
					<Icon name="arrow-forward" />
				</Right>
			</ListItem>
		);
	};

	render() {
		const { textStyle } = styles;

		return (
			<Content>
				<Text style={textStyle}>Sustav odvojenog prikupljanja otpada „od vrata do vrata“ na području Grada Ploča započet je već u ožujku 2017. godine. Navedeni pilot-projekt uvodi se na području prigradskog naselja Birina za individualna stanovanja, te na području Ulice kralja Petra Svačića za višestambene zgrade. Tim putem, uklonjeni su bili spremnici od 1100 l s javnih površina i građanima dodijeljena 2 spremnika: kanta za MKO (miješani komunalni otpad) i kanta za selektivno prikupljeni otpad (papir, plastika,staklo i metal). Ovim pilot-projektom ukupno je obuhvaćeno 70 kućanstava i 241 stanovnik na Birini, i 121 kućanstvo i 338 stanovnika u Ulici kralja Petra Svačića.</Text>
				<Text style={textStyle}>U kolovozu 2017. pilot projekt je uveden i u Ulicama bana Josipa Jelačića i Tina Ujevića u Kominu. Kućanstvima su podijeljene 2 vrste spremnika: kanta za miješani komunalni otpad i kanta za selektivno prikupljeni otpad. Projektom je obuhvaćeno 40 domaćinstava i ukupno 152 stanovnika.</Text>
				<Text style={textStyle}>U travnju 2019. godine, pilot projekt je uveden i na području Staševica, Spilice-Crpala-Gnječi. Kućanstvima su također podijeljene 2 vrste spremnika: kanta za miješani komunalni otpad i kanta za selektivno prikupljeni otpad, dok je projektom obuhvaćeno 217 domaćinstava.</Text>
				<Text style={textStyle}>Pilot projekti se provode i u mjestu Drvenik, gdje mještani također imaju 1 spremnik za MKO i 1 za selektivno odvojeni otpad, a obuhvaćeno je 144 domaćinstva.</Text>
				<Text style={textStyle}>Osim što se selektivni otpad prikuplja putem pilot projekata „od vrata do vrata“, grad Ploče ima i 17 eko otoka, tj.spremika od 1100 l, gdje se također može selektivno odvajati otpad na papir, plastiku, staklo i metal.</Text>
				
				<FlatList
					data={screens}
					renderItem={this.renderItem}
					keyExtractor={item => item.id}
				/>
			</Content>
		);
	}
}

const styles = {
	textStyle: {
		fontSize: 16,
		padding: 15
	}
};

export default OdvojenoPrikupljanjeOtpadaScreen;
