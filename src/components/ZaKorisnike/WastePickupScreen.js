import React, { Component } from "react";
import {
	View,
	Image,
	Alert,
	Linking
} from "react-native";
import {
	Button,
	Content,
	Text,
	Icon
} from 'native-base';
import { connect } from "react-redux";
import ImagePicker from 'react-native-image-crop-picker';
import SwitchSelector from "react-native-switch-selector";

import { Input } from "./../common";
import globalStyles from './../../assets/styles/Style';
import { requestNameChanged, requestEmailChanged, requestLocationChanged, requestMessageChanged, sendRequest } from "../../actions";

class WastePickupScreen extends Component {
	constructor(props) {
		super(props);

		this.state = {
			images: [],
			request_sent: false
		};
	}

	componentDidUpdate(prevProps, prevState) {
		if(prevProps != this.props) {
			if(this.props.request_sent && this.props.request_sent != prevProps.request_sent) {
				this.setState({
					request_sent: true,
					images: []
				});
			}
		}
	}

	onNameChange(text) {
		this.props.requestNameChanged(text);
	}

	onEmailChange(text) {
		this.props.requestEmailChanged(text);
	}

	onLocationChange(text) {
		this.props.requestLocationChanged(text);
	}

	onMessageChange(text) {
		this.props.requestMessageChanged(text);
	}

	onSendRequest() {
		if(this.props.request_name.length == 0 || this.props.request_email.length == 0 || this.props.request_location.length == 0) {
			Alert.alert(
				'Nepotpuna forma',
				'Polja označena zvjezdicom su obvezna',
				[
					{text: 'OK'},
				],
				{cancelable: false},
			);
		} else {
			this.props.sendRequest(this.props.request_name, this.props.request_email, this.props.request_location, this.props.request_message, this.state.images);
		}
	}

	selectPhoto() {
		ImagePicker.openPicker({
			multiple: true
		}).then(images => {
			for(var i = 0; i < images.length; i++) {
				this.setState({
					images: this.state.images.concat(images[i]['path'])
				});
			}
		});
	}

	renderNote() {
		if(this.state.request_sent) {
			return (
				<View style={{ flex: 1, flexDirection: 'row', marginBottom: 20, padding: 10, borderWidth: 1, borderColor: '#019147' }}>
					<Icon name="ios-checkmark-circle" style={styles.cardIconGreen} />
					<Text style={{ color: '#019147' }}>Uspješno ste naručili odvoz glomaznog otpada</Text>
				</View>
			);
		}
	}

	renderImages() {
		if(this.state.images.length > 0) {
			return this.state.images.map((image) => {
				return (
					<Image style={{ width: '50%', height: 150, marginBottom: 15 }} source={{ uri: image }} />
				);
			});
		}
	}

	render() {
		return (
			<Content padder>
				<Text style={globalStyles.subtitle}>Order a pickup of cumbersome waste</Text>

				<Input
					label="Name *"
					onChangeText={this.onNameChange.bind(this)}
					autoCorrect={false}
					value={this.props.request_name}
					keyboardType={"default"}
				/>
				<Input
					label="E-mail *"
					onChangeText={this.onEmailChange.bind(this)}
					autoCorrect={false}
					value={this.props.request_email}
					keyboardType={"email-address"}
				/>
				<Input
					label="Location *"
					onChangeText={this.onLocationChange.bind(this)}
					autoCorrect={false}
					value={this.props.request_location}
					keyboardType={"default"}
				/>
				<Input
					label="Message"
					onChangeText={this.onMessageChange.bind(this)}
					autoCorrect={false}
					value={this.props.request_message}
					keyboardType={"default"}
					multiline = {true}
					numberOfLines = {4}
				/>
				<View style={{ flex: 1, flexDirection: 'row' }}>
					{this.renderImages()}
				</View>
				<View style={{ flex: 1, flexDirection: 'row' }}>
					<Button success style={{ marginBottom: 20, backgroundColor: '#019147' }} onPress={this.selectPhoto.bind(this)}><Text>Add photo</Text></Button>
				</View>
				{this.renderNote()}
				<View style={{ flex: 1, flexDirection: 'row' }}>
					<Button success style={{ marginBottom: 20, backgroundColor: '#019147' }} onPress={this.onSendRequest.bind(this)}><Text>Send</Text></Button>
				</View>
				<Text style={globalStyles.paragraphNoMargin}>Or by contacting us at:</Text>
				<Text style={globalStyles.paragraphNoMargin}>+385 20 676601</Text>
				<Text style={globalStyles.paragraphNoMargin}>+385 95 4477701</Text>
				<Text style={globalStyles.paragraph}>info@k-odrzavanje.hr</Text>
			</Content>
		);
	}
}

const styles = {
	cardIconGreen: {
		fontSize: 18,
		width: 15,
		color: "#019147",
		marginRight: 12
	}
};

const mapStateToProps = ({ requestReducer }) => {
	const { request_name, request_email, request_location, request_message, request_sent, loading } = requestReducer;

	return {
		request_name,
		request_email,
		request_location,
		request_message,
		request_sent,
		loading
	};
};

export default connect(mapStateToProps, {
	requestNameChanged, requestEmailChanged, requestLocationChanged, requestMessageChanged, sendRequest
})(WastePickupScreen);
