import React, { Component } from "react";
import {
	View,
	Text,
	Image
} from "react-native";
import {
	Content
} from 'native-base';
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';

class OdlagalisteLovornikScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { textStyle } = styles;

		return (
			<Content padder>
				<Text style={globalStyles.paragraph}>Odlagalište otpada “Lovornik” nalazi se na području Dubrovačko-neretvanske županije, u katastarskoj općini Baćina. Udaljeno je oko 4 kilometra sjeverozapadno od Ploča, odnosno oko 700 metara istočno od Baćinskih jezera koja su zaštićeno područje Natura 2000. Nadmorska visina lokacije zahvata varira od 80 m n.m. do oko 100 m n.m. Sjevernim rubom zahvata odlagališta prolazi cesta DC-8 na koju je zahvat spojen makadamskim pristupnim putem duljine cca 100 metara. Lokacija predstavlja dvije spojene vrtače na dnu kojih se nalazi crvenica. Okolno područje je krš, a lokacija je okružena makijom. Na odlagalištu postoji čuvarska kućica i rampa, te je odlagalište djelomično ograđeno. Ukupna površina odlagališta iznosi cca 3,6 ha.</Text>
				<ResponsiveImageView source={ require('./../../assets/images/lovornik-ortofoto.png') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Organizirano skupljen otpad odlaže se na odlagalište otpada “Lovornik” od 1970. godine.</Text>
				<Text style={globalStyles.paragraph}>Lokaciju je utvrdio Savjet za Stambeno-komunalne poslove bivše Općine Metković. Godišnja količina odloženog otpada procjenjuje se na 20 000 m3.</Text>
				<Text style={globalStyles.paragraph}>Prostor za odlaganje neopasnog otpada zauzima površinu cca 1,2 ha (Slika 8.). Otpad se trenutno odlaže na neuređenoj plohi površine cca 0,65 ha. Početkom rada ŽCGO ili zapunjenjem kapaciteta ovaj prostor odlagališta će se sanirati i zatvoriti za rad ugradnjom završnog pokrovnog sloja.</Text>
				<Text style={globalStyles.paragraph}>Tehnologija odlaganja otpada se sastoji iz sljedećih osnovnih operacija, koje se odvijaju tijekom radnog dana:</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>1. istresanje otpada na radnu površinu</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>2.rasprostiranje otpada u slojeve</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>3. zbijanje otpada</Text>
				<Text style={globalStyles.paragraph}>4. povremeno prekrivanje otpada inertnim materijalom</Text>
				<ResponsiveImageView source={ require('./../../assets/images/lovornik-kazeta.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Na gornjoj plohi odlagališta izgrađeno je stočno groblje, koje se više ne koristi. Svaki dan utovarivač poravnava otpad, te prekriva gornju plohu.</Text>
				<Text style={globalStyles.paragraph}>Odlagalište Lovornik je neusklađeno odlagalište koje zaprima otpad bez dozvole za gospodarenje otpadom. Njegova sanacija započeta je još 2008. godine, ali je zbog neriješenih imovinsko-pravnih odnosa zaustavljena.</Text>
				
				<Text style={globalStyles.paragraph}>Na dijelu odlagališta uređen je prostor površine cca 650 m2 za odlaganje azbestnog otpada (posebno odlagališno polje odvojeno od ostalog otpada na odlagalištu). Uređenje dijela odlagališta za prihvat azbestnog otpada je u skladu s Projektom odlaganja azbestnog otpada kao i s Pravilnikom o građevnom otpadu i otpadu koji sadrži azbest (NN 69/16), Naputkom o postupanju s otpadom koji sadrži azbest (NN br. 89/08), te s Pravilnikom o načinima i uvjetima odlaganja otpada, kategorijama i uvjetima rada za odlagališta otpada (NN br. 114/15).</Text>
				<ResponsiveImageView source={ require('./../../assets/images/lovornik-kazeta1.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginTop: 15 } ]}>Azbestna kazeta je jedina na širem području i prihvaća otpad sakupljen iz cijele RH. Nije obuhvaćena Glavnim projektom sanacije komunalnog odlagališta Lovornik. Azbestni otpad dovozi se na lokaciju odlagališta čvrsto vezan, na paletama ili u vrećama. Tehnologija odlaganja azbestnog otpada na pripremljenom dijelu odlagališta sastoji se iz sljedećih osnovnih operacija, koje se odvijaju tijekom radnog dana:</Text>
				<Text style={globalStyles.paragraph}>Odlagalište Lovornik je neusklađeno odlagalište koje zaprima otpad bez dozvole za gospodarenje otpadom. Njegova sanacija započeta je još 2008. godine, ali je zbog neriješenih imovinsko-pravnih odnosa zaustavljena.</Text>
				
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>• azbestni otpad zaštićen folijom (ili zaštićen na drugi način) dovozi se i odlaže se na pripremljenu radnu površinu</Text>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0 } ]}>• zbijanje otpada (samo krutog otpada). Otpad u praškastom stanju dolazi zaštićen od vanjskih utjecaja (omotan folijom ili sl.) te se kao takav odlaže na plohu odlagališta i prekriva inertnim materijalom.</Text>
				<Text style={globalStyles.paragraph}>• dnevno prekrivanje azbestnog otpada inertnim materijalom uz obavezno vlaženje</Text>
				<ResponsiveImageView source={ require('./../../assets/images/lovornik-kazeta2.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<Text style={[ globalStyles.paragraph, { marginBottom: 0, marginTop: 15 } ]}>Na lokaciji je izgrađen sustav za prikupljanje procjednih voda s uređene kazete za odlaganje azbestnog otpada. Procjedna voda se skuplja drenažnim sustavom (batuda + drenažne cijevi) te odvodi preko taložnika u upojni dren.</Text>
			</Content>
		);
	}
}

const styles = {
	textStyle: {
		fontSize: 16
	}
};

export default OdlagalisteLovornikScreen;
