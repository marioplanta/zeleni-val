import React, { Component } from "react";
import {
	View,
	FlatList,
	TouchableWithoutFeedback,
	Alert,
	Image,
} from "react-native";
import {
	Content,
	Button,
	Text,
	Icon
} from 'native-base';
import { connect } from "react-redux";
import ImagePicker from 'react-native-image-crop-picker';
import ResponsiveImageView from 'react-native-responsive-image-view';
import SwitchSelector from "react-native-switch-selector";

import { Input } from "./../common";
import globalStyles from './../../assets/styles/Style';
import { fetchListings, listingNameChanged, listingEmailChanged, listingMessageChanged, sendListing } from "../../actions";

class WebBuvljakScreen extends Component {
	constructor(props) {
		super(props);

		this.state = {
			listing_sent: false,
			images: []
		};
	}

	componentDidUpdate(prevProps, prevState) {
		if(prevProps != this.props) {
			if(this.props.listing_sent && this.props.listing_sent != prevProps.listing_sent) {
				this.props.fetchListings();

				this.setState({
					listing_sent: true,
					images: []
				});
			}
		}
	}

	componentWillMount() {
		this.props.fetchListings();
	}

	onNameChange(text) {
		this.props.listingNameChanged(text);
	}

	onEmailChange(text) {
		this.props.listingEmailChanged(text);
	}

	onMessageChange(text) {
		this.props.listingMessageChanged(text);
	}

	onSubmitListing() {
		if(this.props.listing_name.length == 0 || this.props.listing_email.length == 0) {
			Alert.alert(
				'Nepotpuna forma',
				'Polja označena zvjezdicom su obvezna',
				[
					{text: 'OK'},
				],
				{cancelable: false},
			);
		} else {
			this.props.sendListing(this.props.listing_name, this.props.listing_email, this.props.listing_message, this.state.images);
		}
	}

	selectPhoto() {
		ImagePicker.openPicker({
			multiple: true
		}).then(images => {
			for(var i = 0; i < images.length; i++) {
				this.setState({
					images: this.state.images.concat(images[i]['path'])
				});
			}
		});
	}

	renderImages() {
		if(this.state.images.length > 0) {
			return this.state.images.map((image) => {
				return (
					<Image style={{ width: '50%', height: 150, marginBottom: 15 }} source={{ uri: image }} />
				);
			});
		}
	}

	renderItem = ({ item }) => {
		var excerpt = item.excerpt['rendered'].replace(/<\/?[^>]+(>|$)/g, "");

		if(item.categories.includes(24)) {
			return (
				<TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('SingleVijestScreen', {
					title: item.title['rendered'],
					content: item.content['rendered'],
					image: item._embedded['wp:featuredmedia'][0].source_url
				})}>
					<View style={{ flexDirection: 'column', marginTop: 15 }}>
						<Image style={{ width: '100%', height: 200 }} source={{ uri: item._embedded['wp:featuredmedia'][0].source_url }} />
						<Text style={{ fontWeight: 'bold', marginVertical: 10, fontSize: 18 }}>{item.title['rendered']}</Text>
						<Text style={{ margin: 0 }}>{excerpt}</Text>
					</View>
				</TouchableWithoutFeedback>
			);
		}
	};

	renderNote() {
		if(this.state.listing_sent) {
			return (
				<View style={{ flex: 1, flexDirection: 'row', marginBottom: 20, padding: 10, borderWidth: 1, borderColor: '#019147' }}>
					<Icon name="ios-checkmark-circle" style={styles.cardIconGreen} />
					<Text style={{ color: '#019147' }}>Uspješno ste predali oglas</Text>
				</View>
			);
		}
	}

	render() {
		return (
			<Content padder>
				<Text style={[ globalStyles.subtitle, { marginTop: 0 } ]}>Imate li predmet viška koji može koristiti drugima?</Text>
				<Text style={globalStyles.paragraph}>Imate stvar koja vam više ne treba? Podrum vam je pun odbačenih predmeta koji samo skupljaju prašinu? Do sada je proljetno čišćenje rezultiralo gomilom smeća?</Text>
				<Text style={globalStyles.paragraph}>Imamo super vijesti. Možda baš to što nekome treba. Umjesto da bacite možete prodati-pokloniti-zamijeniti.</Text>
				<Text style={globalStyles.paragraph}>Napravili smo WEB BUVLJAK na kojem možete drugima ponuditi predmete koje više ne koristite, a ne želite ih baciti!</Text>
				<Text style={globalStyles.paragraph}>Osim što nećete stvoriti otpad, nekome ćete pomoći predmetom koji bi vi ionako bacili.</Text>
				<Text style={globalStyles.paragraph}>Možete poklanjati i hranu, kozmetiku, odjeću, namještaj, sve što vam nepotrebno zauzima prostor u kućanstvu i želite se toga riješiti.</Text>
				<Text style={globalStyles.paragraph}>Možete stavljati i oglase za podjelu troškova dostave putem web kupovine.</Text>

				<Text style={globalStyles.subtitle}>Web buvljak</Text>

				<Input
					label="Ime i prezime *"
					onChangeText={this.onNameChange.bind(this)}
					autoCorrect={false}
					value={this.props.listing_name}
					keyboardType={"default"}
				/>
				<Input
					label="E-mail *"
					onChangeText={this.onEmailChange.bind(this)}
					autoCorrect={false}
					value={this.props.listing_email}
					keyboardType={"email-address"}
				/>
				<Input
					label="Opis"
					onChangeText={this.onMessageChange.bind(this)}
					autoCorrect={false}
					value={this.props.listing_message}
					keyboardType={"default"}
					multiline = {true}
					numberOfLines = {4}
				/>
				<View style={{ flex: 1, flexDirection: 'row' }}>
					{this.renderImages()}
				</View>
				<View style={{ flex: 1, flexDirection: 'row' }}>
					<Button success style={{ marginBottom: 20, backgroundColor: '#019147' }} onPress={this.selectPhoto.bind(this)}><Text>Dodaj fotografiju</Text></Button>
				</View>
				{this.renderNote()}
				<View style={{ flex: 1, flexDirection: 'row' }}>
					<Button success style={{ backgroundColor: '#019147' }} onPress={this.onSubmitListing.bind(this)}><Text>Objavite oglas</Text></Button>
				</View>

				<Text style={[ globalStyles.subtitle, { marginTop: 30 } ]}>Pogledajte ponudu u našem WEB-BUVLJAKU!</Text>
				<FlatList
					data={this.props.listings}
					renderItem={this.renderItem}
					keyExtractor={item => item.id}
				/>
			</Content>
		);
	}
}

const styles = {
	cardIconGreen: {
		fontSize: 18,
		width: 15,
		color: "#019147",
		marginRight: 12
	}
};

const mapStateToProps = ({ buvljakReducer }) => {
	const { listings, listing_name, listing_email, listing_message, listing_sent, loading } = buvljakReducer;

	return {
		listings,
		listing_name,
		listing_email,
		listing_message,
		listing_sent,
		loading
	};
};

export default connect(mapStateToProps, {
	fetchListings, listingNameChanged, listingEmailChanged, listingMessageChanged, sendListing
})(WebBuvljakScreen);
