import React, { Component } from "react";
import {
	View,
	Image,
	Alert,
	Linking
} from "react-native";
import {
	Button,
	Content,
	Text,
	Icon
} from 'native-base';
import { connect } from "react-redux";
import ImagePicker from 'react-native-image-crop-picker';
import SwitchSelector from "react-native-switch-selector";

import { Input } from "./../common";
import globalStyles from './../../assets/styles/Style';
import { requestNameChanged, requestEmailChanged, requestLocationChanged, requestMessageChanged, sendRequest } from "../../actions";

class OdvozGlomaznogOtpadaScreen extends Component {
	constructor(props) {
		super(props);

		this.state = {
			images: [],
			request_sent: false
		};
	}

	componentDidUpdate(prevProps, prevState) {
		if(prevProps != this.props) {
			if(this.props.request_sent && this.props.request_sent != prevProps.request_sent) {
				this.setState({
					request_sent: true,
					images: []
				});
			}
		}
	}

	onNameChange(text) {
		this.props.requestNameChanged(text);
	}

	onEmailChange(text) {
		this.props.requestEmailChanged(text);
	}

	onLocationChange(text) {
		this.props.requestLocationChanged(text);
	}

	onMessageChange(text) {
		this.props.requestMessageChanged(text);
	}

	onSendRequest() {
		if(this.props.request_name.length == 0 || this.props.request_email.length == 0 || this.props.request_location.length == 0) {
			Alert.alert(
				'Nepotpuna forma',
				'Polja označena zvjezdicom su obvezna',
				[
					{text: 'OK'},
				],
				{cancelable: false},
			);
		} else {
			this.props.sendRequest(this.props.request_name, this.props.request_email, this.props.request_location, this.props.request_message, this.state.images);
		}
	}

	selectPhoto() {
		ImagePicker.openPicker({
			multiple: true
		}).then(images => {
			for(var i = 0; i < images.length; i++) {
				this.setState({
					images: this.state.images.concat(images[i]['path'])
				});
			}
		});
	}

	renderNote() {
		if(this.state.request_sent) {
			return (
				<View style={{ flex: 1, flexDirection: 'row', marginBottom: 20, padding: 10, borderWidth: 1, borderColor: '#019147' }}>
					<Icon name="ios-checkmark-circle" style={styles.cardIconGreen} />
					<Text style={{ color: '#019147' }}>Uspješno ste naručili odvoz glomaznog otpada</Text>
				</View>
			);
		}
	}

	renderImages() {
		if(this.state.images.length > 0) {
			return this.state.images.map((image) => {
				return (
					<Image style={{ width: '50%', height: 150, marginBottom: 15 }} source={{ uri: image }} />
				);
			});
		}
	}

	render() {
		return (
			<Content padder>
				<Text style={globalStyles.paragraph}>Svaki korisnik koji uredno plaća račune za javnu uslugu prikupljanja miješnanog komunalnog otpada ima pravo na 1 besplatan odvoz glomaznog otpada u količini od 5m3 na adresi na kojoj je prijavljen.</Text>

				<Text style={globalStyles.subtitle}>Naručite besplatni odvoz glomaznog otpada</Text>

				<Input
					label="Ime i prezime *"
					onChangeText={this.onNameChange.bind(this)}
					autoCorrect={false}
					value={this.props.request_name}
					keyboardType={"default"}
				/>
				<Input
					label="E-mail *"
					onChangeText={this.onEmailChange.bind(this)}
					autoCorrect={false}
					value={this.props.request_email}
					keyboardType={"email-address"}
				/>
				<Input
					label="Lokacija *"
					onChangeText={this.onLocationChange.bind(this)}
					autoCorrect={false}
					value={this.props.request_location}
					keyboardType={"default"}
				/>
				<Input
					label="Poruka"
					onChangeText={this.onMessageChange.bind(this)}
					autoCorrect={false}
					value={this.props.request_message}
					keyboardType={"default"}
					multiline = {true}
					numberOfLines = {4}
				/>
				<View style={{ flex: 1, flexDirection: 'row' }}>
					{this.renderImages()}
				</View>
				<View style={{ flex: 1, flexDirection: 'row' }}>
					<Button success style={{ marginBottom: 20, backgroundColor: '#019147' }} onPress={this.selectPhoto.bind(this)}><Text>Dodaj fotografiju</Text></Button>
				</View>
				{this.renderNote()}
				<View style={{ flex: 1, flexDirection: 'row' }}>
					<Button success style={{ marginBottom: 20, backgroundColor: '#019147' }} onPress={this.onSendRequest.bind(this)}><Text>Pošalji</Text></Button>
				</View>
				<Text style={globalStyles.paragraphNoMargin}>Ili na kontakte:</Text>
				<Text style={globalStyles.paragraphNoMargin}>020/676-601</Text>
				<Text style={globalStyles.paragraphNoMargin}>0954477701</Text>
				<Text style={globalStyles.paragraph}>info@k-odrzavanje.hr</Text>
				<Text style={globalStyles.paragraph}>Odvoz je potrebno naručiti tjedan dana prije samog odvoza.</Text>
				<Text style={globalStyles.paragraphNoMargin}>Pravne osobe i fizičke osobe koje su već iskoristile pravo na 1 besplatan odvoz godišnje plaćaju odvoz glomaznog otpada prema važećem cjeniku:</Text>
				<Text style={globalStyles.paragraphNoMargin}>Općina Gradac - 880,00 kn + PDV</Text>
				<Text style={globalStyles.paragraph}>Grad Ploče - 720,00 kn + PDV</Text>
				<Text style={globalStyles.paragraph}>Važno! U Općini Gradac glomazni otpad se NE odvozi u periodu od 15.6.-30.9.</Text>
				<Text style={globalStyles.paragraph}>Fizičke osobe s područja Općine Gradac i Grada Ploča u bilo kojem trenutku glomazni otpad mogu besplatno dovesti u reciklažno dvorište u Pločama.</Text>
				<Text style={globalStyles.paragraphNoMargin}>Što se sve smatra glomaznim otpadom?</Text>
				<Text style={globalStyles.paragraph}>Popis vrsta predmeta i tvari koji se smatraju krupnim (glomaznim) komunalnim otpadom pogledajte na slijedećem linku</Text>
				<View style={{ flex: 1, flexDirection: 'row' }}>
					<Button success style={{ marginBottom: 20, backgroundColor: '#019147' }} onPress={() => { Linking.openURL('https://narodne-novine.nn.hr/clanci/sluzbeni/2015_07_79_1534.html') } }><Text>POPIS</Text></Button>
				</View>
			</Content>
		);
	}
}

const styles = {
	cardIconGreen: {
		fontSize: 18,
		width: 15,
		color: "#019147",
		marginRight: 12
	}
};

const mapStateToProps = ({ requestReducer }) => {
	const { request_name, request_email, request_location, request_message, request_sent, loading } = requestReducer;

	return {
		request_name,
		request_email,
		request_location,
		request_message,
		request_sent,
		loading
	};
};

export default connect(mapStateToProps, {
	requestNameChanged, requestEmailChanged, requestLocationChanged, requestMessageChanged, sendRequest
})(OdvozGlomaznogOtpadaScreen);
