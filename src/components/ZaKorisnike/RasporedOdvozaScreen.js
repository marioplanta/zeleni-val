import React, { Component } from "react";
import {
	View,
	Text,
	FlatList
} from "react-native";
import {
	Content,
	ListItem,
	Left,
	Right,
	Icon
} from 'native-base';

const screens = [
	{
		id: '0',
		title: "Pilot projekti",
		screen: "PilotProjektiScreen"
	},
	{
		id: '1',
		title: "Spremnici na javnim površinama",
		screen: "JavniSpremniciScreen"
	}
];

class RasporedOdvozaScreen extends Component {
	constructor(props) {
		super(props);
	}

	renderItem = ({ item }) => {
		return (
			<ListItem onPress={() => this.props.navigation.navigate(item.screen)}>
				<Left>
					<Text>
						{item.title}
					</Text>
				</Left>
				<Right>
					<Icon name="arrow-forward" />
				</Right>
			</ListItem>
		);
	};

	render() {
		const { textStyle } = styles;

		return (
			<Content>
				<Text style={textStyle}>Radna jedinica obavlja poslove sakupljanja i odlaganja miješanog komunalnog i glomaznog otpada s područja Grada Ploča i Općine Gradac, odlaganja miješanog komunalnog otpada s područja Općine Slivno, sakupljanja, sortiranja i skladištenja odvojeno prikupljenog otpada (papir, staklo, plastika, metal) s područja Grada Ploča i Općine Gradac i odlaganja građevnog otpada koji sadrži azbest na posebno izgrađenu kazetu za azbestni otpad koji se dovozi s područja cijele Hrvatske. Raspored odvoza možete vidjeti na sljedećim poveznicama:</Text>
				
				<FlatList
					data={screens}
					renderItem={this.renderItem}
					keyExtractor={item => item.id}
				/>
			</Content>
		);
	}
}

const styles = {
	textStyle: {
		fontSize: 16,
		padding: 15
	}
};

export default RasporedOdvozaScreen;
