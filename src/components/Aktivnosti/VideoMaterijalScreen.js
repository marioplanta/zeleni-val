import React, { Component } from "react";
import {
	View,
	Text
} from "react-native";
import {
	Content
} from 'native-base';
import YouTube from 'react-native-youtube';

class VideoMaterijalScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Content padder>
				<YouTube
					videoId="gh5ymIhhGlo"
					play={false}
					fullscreen
					style={{ alignSelf: 'stretch', height: 300, marginBottom: 15 }}
				/>
				<YouTube
					videoId="n4aZAoV1tyQ"
					play={false}
					fullscreen
					style={{ alignSelf: 'stretch', height: 300, marginBottom: 15 }}
				/>
				<YouTube
					videoId="U-XFrZlfcv0"
					play={false}
					fullscreen
					style={{ alignSelf: 'stretch', height: 300, marginBottom: 15 }}
				/>
				<YouTube
					videoId="sjtPfNOtmqk"
					play={false}
					fullscreen
					style={{ alignSelf: 'stretch', height: 300, marginBottom: 15 }}
				/>
				<YouTube
					videoId="iBfD7_UwOa8"
					play={false}
					fullscreen
					style={{ alignSelf: 'stretch', height: 300 }}
				/>
			</Content>
		);
	}
}

export default VideoMaterijalScreen;
