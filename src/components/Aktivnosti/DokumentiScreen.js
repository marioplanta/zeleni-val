import React, { Component } from "react";
import {
	View,
	Text,
	Image
} from "react-native";
import {
	Content
} from 'native-base';
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';

class DokumentiScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Content padder>
				<Text style={globalStyles.subtitle}>Odluka o financiranju za projektni prijedlog – Provedba komunikacijske kampanje o održivom gospodarenju otpadom “Zeleni val”</Text>
				<ResponsiveImageView source={ require('./../../assets/images/odluka.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<ResponsiveImageView source={ require('./../../assets/images/odluka2.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
				<ResponsiveImageView source={ require('./../../assets/images/odluka3.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
			</Content>
		);
	}
}

export default DokumentiScreen;
