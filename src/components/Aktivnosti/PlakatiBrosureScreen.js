import React, { Component } from "react";
import {
	View,
	Text,
	Image
} from "react-native";
import {
	Content
} from 'native-base';
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../../assets/styles/Style';

class PlakatiBrosureScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Content padder>
				<Text style={globalStyles.subtitle}>Vodič za kućno recikliranje</Text>
				<ResponsiveImageView source={ require('./../../assets/images/letak-komin.png') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>

				<Text style={globalStyles.subtitle}>Upute i raspored odvoza glomaznog otpada – Komin</Text>
				<ResponsiveImageView source={ require('./../../assets/images/letak-komin2.png') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>

				<Text style={globalStyles.subtitle}>Upute i raspored odvoza glomaznog otpada – Staševica, Spilice, Crpala i Gnječi</Text>
				<ResponsiveImageView source={ require('./../../assets/images/stasevica.png') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>

				<Text style={globalStyles.subtitle}>Letak na poleđini računa za odvoz otpada</Text>
				<ResponsiveImageView source={ require('./../../assets/images/poledjina-racuna.png') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>

				<Text style={globalStyles.subtitle}>Platnena vrećica</Text>
				<ResponsiveImageView source={ require('./../../assets/images/vrecica.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>

				<Text style={globalStyles.subtitle}>Letak za zelene otoke 1</Text>
				<ResponsiveImageView source={ require('./../../assets/images/zeleni-otoci.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>

				<Text style={globalStyles.subtitle}>Letak za zelene otoke 2</Text>
				<ResponsiveImageView source={ require('./../../assets/images/zeleni-otoci2.jpg') }>
					{({ getViewProps, getImageProps }) => (
						<View {...getViewProps()}>
							<Image {...getImageProps()} />
						</View>
					)}
				</ResponsiveImageView>
			</Content>
		);
	}
}

export default PlakatiBrosureScreen;
