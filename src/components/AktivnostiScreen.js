import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Body,
	Title,
	Tabs,
	Tab,
	ScrollableTab,
	Left,
	Right,
	Button,
	Icon
} from "native-base";

import AktivnostiProjektaScreen from "./Aktivnosti/AktivnostiProjektaScreen";
import VideoMaterijalScreen from "./Aktivnosti/VideoMaterijalScreen";
import DokumentiScreen from "./Aktivnosti/DokumentiScreen";
import PlakatiBrosureScreen from "./Aktivnosti/PlakatiBrosureScreen";

class AktivnostiScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header style={{ backgroundColor: '#ffffff' }}>
					<Left>
						<Button transparent onPress={() => {
							this.props.navigation.navigate('HomeScreen');
						}}>
							<Icon name="menu" style={{ color: '#019147' }} />
						</Button>
					</Left>
					<Body>
						<Image source={ require('./../assets/images/logo.png') } style={{ alignSelf: 'center', width: 200, height: 35, resizeMode: "contain"}} />
					</Body>
					<Right />
				</Header>
				<Tabs tabBarUnderlineStyle={{ backgroundColor: '#019147' }} renderTabBar={() => <ScrollableTab style={{ backgroundColor: '#ffffff' }} />}>
					<Tab heading="Aktivnosti projekta" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<AktivnostiProjektaScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Video materijal" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<VideoMaterijalScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Dokumenti" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<DokumentiScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Plakati, brošure i letci" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<PlakatiBrosureScreen navigation={this.props.navigation} />
					</Tab>
				</Tabs>
			</Container>
		);
	}
}

export default AktivnostiScreen;
