import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Content,
	Body,
	Icon,
	Left,
	Right,
	Title,
	Text,
	Button,
} from "native-base";
import ResponsiveImageView from 'react-native-responsive-image-view';

import globalStyles from './../assets/styles/Style';

class SingleVijestScreen extends Component {
	constructor(props) {
		super(props);

		this.title = this.props.navigation.getParam('title');
		this.description = this.props.navigation.getParam('content').replace(/<\/?[^>]+(>|$)/g, "");
		this.image = this.props.navigation.getParam('image');
	}

	render() {
		var content = this.description.replace('[vc_row]','');
		content = content.replace('[vc_column]','');
		content = content.replace('[vc_column_text]','');
		content = content.replace('[/vc_row]','');
		content = content.replace('[/vc_column]','');
		content = content.replace('[/vc_column_text]','');
		content = content.replace('[&hellip;]','');
		content = content.replace('&nbsp;','');
		
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header style={{ backgroundColor: '#ffffff' }}>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" style={globalStyles.backButtonIcon} />
							<Text style={globalStyles.backButtonText}>Natrag</Text>
						</Button>
					</Left>
					<Body>
						<Title>{this.title}</Title>
					</Body>
					<Right/>
				</Header>
				<Content padder>
					<Image style={{ width: '100%', height: 200 }} source={{ uri: this.image }} />
					<Text style={globalStyles.subtitle}>{this.title}</Text>
					<Text style={globalStyles.paragraph}>{content}</Text>
				</Content>
			</Container>
		);
	}
}

export default SingleVijestScreen;
