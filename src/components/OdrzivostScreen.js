import React, { Component } from "react";
import {
	View,
	Image,
} from "react-native";
import {
	Header,
	Container,
	Body,
	Left,
	Right,
	Title,
	Tabs,
	Tab,
	ScrollableTab,
	Icon,
	Button
} from "native-base";

import OdrzivoGospodarenjeOtpadomScreen from "./Odrzivost/OdrzivoGospodarenjeOtpadomScreen";
import OTemiScreen from "./Odrzivost/OTemiScreen";
import PravniOkvirScreen from "./Odrzivost/PravniOkvirScreen";
import CiljeviScreen from "./Odrzivost/CiljeviScreen";
import PojmovnikScreen from "./Odrzivost/PojmovnikScreen";

class OdrzivostScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header style={{ backgroundColor: '#ffffff' }}>
					<Left>
						<Button transparent onPress={() => {
							this.props.navigation.navigate('HomeScreen');
						}}>
							<Icon name="menu" style={{ color: '#019147' }} />
						</Button>
					</Left>
					<Body>
						<Image source={ require('./../assets/images/logo.png') } style={{alignSelf: 'center', width: 200, height: 35, resizeMode: "contain"}} />
					</Body>
					<Right />
				</Header>
				<Tabs tabBarUnderlineStyle={{ backgroundColor: '#019147' }} renderTabBar={() => <ScrollableTab style={{ backgroundColor: '#ffffff' }} />}>
					<Tab heading="Održivo gospodarenje otpadom" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<OdrzivoGospodarenjeOtpadomScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="O temi" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<OTemiScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Pravni okvir" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<PravniOkvirScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Ciljevi" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<CiljeviScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Pojmovnik" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<PojmovnikScreen navigation={this.props.navigation} />
					</Tab>
				</Tabs>
			</Container>
		);
	}
}

export default OdrzivostScreen;
