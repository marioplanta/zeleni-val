import React from "react";
import { TextInput, View, Text } from "react-native";

const Input = ({ label, value, onChangeText, placeholder, secureTextEntry, keyboardType, multiline, numberOfLines, style }) => {
	const { inputStyle, labelStyle, containerStyle, borderStyle } = styles;

	return (
		<View style={containerStyle}>
			<Text style={labelStyle}>{label}</Text>
			<TextInput
				style={[inputStyle]}
				value={value}
				onChangeText={onChangeText}
				autoCorrect={false}
				placeholder={placeholder}
				placeholderTextColor='#6D6D6D'
				secureTextEntry={secureTextEntry}
				keyboardType={keyboardType}
				multiline = {multiline}
				numberOfLines = {numberOfLines}
			/>
		</View>
	);
};

const styles = {
	inputStyle: {
		color: "#000000",
		padding: 10,
		fontSize: 18,
		flex: 1,
		borderColor: '#019147',
		borderWidth: 1,
		backgroundColor: "transparent",
	},
	labelStyle: {
		fontSize: 12,
		paddingBottom: 4,
		fontWeight: "600",
		color: "#019147",
		textTransform: "uppercase",
	},
	containerStyle: {
		flex: 1,
		marginBottom: 15
	},
};

export { Input };
