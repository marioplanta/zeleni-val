import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Body,
	Title,
	Tabs,
	Tab,
	ScrollableTab,
	Left,
	Right,
	Button,
	Icon
} from "native-base";

import SprjecavanjeNastankaOtpadaScreen from "./EdukativniKutak/SprjecavanjeNastankaOtpadaScreen";
import PonovnaUporabaPredmetaScreen from "./EdukativniKutak/PonovnaUporabaPredmetaScreen";
import RecikliranjeScreen from "./EdukativniKutak/RecikliranjeScreen";
import KompostiranjeScreen from "./EdukativniKutak/KompostiranjeScreen";
import PosebneKategorijeOtpadaScreen from "./EdukativniKutak/PosebneKategorijeOtpadaScreen";
import OznakeNaAmbalaziScreen from "./EdukativniKutak/OznakeNaAmbalaziScreen";
import ProblemPlastikeScreen from "./EdukativniKutak/ProblemPlastikeScreen";

class EdukativniKutakScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header style={{ backgroundColor: '#ffffff' }}>
					<Left>
						<Button transparent onPress={() => {
							this.props.navigation.navigate('HomeScreen');
						}}>
							<Icon name="menu" style={{ color: '#019147' }} />
						</Button>
					</Left>
					<Body>
						<Image source={ require('./../assets/images/logo.png') } style={{alignSelf: 'center', width: 200, height: 35, resizeMode: "contain"}} />
					</Body>
					<Right />
				</Header>
				<Tabs tabBarUnderlineStyle={{ backgroundColor: '#019147' }} renderTabBar={() => <ScrollableTab style={{ backgroundColor: '#ffffff' }} />}>
					<Tab heading="Sprječavanje nastanka otpada" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<SprjecavanjeNastankaOtpadaScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Ponovna uporaba predmeta" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<PonovnaUporabaPredmetaScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Recikliranje" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<RecikliranjeScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Kompostiranje" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<KompostiranjeScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Posebne kategorije otpada" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<PosebneKategorijeOtpadaScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Oznake na ambalaži" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<OznakeNaAmbalaziScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Problem plastike" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<ProblemPlastikeScreen navigation={this.props.navigation} />
					</Tab>
				</Tabs>
			</Container>
		);
	}
}

export default EdukativniKutakScreen;
