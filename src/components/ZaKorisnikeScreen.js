import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Body,
	Title,
	Tabs,
	Tab,
	ScrollableTab,
	Left,
	Right,
	Button,
	Icon
} from "native-base";

import PrijavaLokacijeScreen from "./ZaKorisnike/PrijavaLokacijeScreen";
import RasporedOdvozaScreen from "./ZaKorisnike/RasporedOdvozaScreen";
import OdvojenoPrikupljanjeOtpadaScreen from "./ZaKorisnike/OdvojenoPrikupljanjeOtpadaScreen";
import OdvozGlomaznogOtpadaScreen from "./ZaKorisnike/OdvozGlomaznogOtpadaScreen";
import ReciklaznoDvoristeScreen from "./ZaKorisnike/ReciklaznoDvoristeScreen";
import OdlagalisteLovornikScreen from "./ZaKorisnike/OdlagalisteLovornikScreen";
import WebBuvljakScreen from "./ZaKorisnike/WebBuvljakScreen";
import AkcijePrikupljanjaOtpadaScreen from "./ZaKorisnike/AkcijePrikupljanjaOtpadaScreen";
import CestoPostavljanaPitanjaScreen from "./ZaKorisnike/CestoPostavljanaPitanjaScreen";

class ZaKorisnikeScreen extends Component {
	constructor(props) {
		super(props);

		this.state = {
			initialPage: 0
		};
	}

	componentDidMount() {
		if(this.props.navigation.dangerouslyGetParent().getParam('tab') == 'Prijava') {
			setTimeout(() => {
				this.setState({
					initialPage: 0
				});
			}, 0);
		} else if(this.props.navigation.dangerouslyGetParent().getParam('tab') == 'WebBuvljak') {
			setTimeout(() => {
				this.setState({
					initialPage: 6
				});
			}, 0);
		} else if(this.props.navigation.dangerouslyGetParent().getParam('tab') == 'OdvozOtpada') {
			setTimeout(() => {
				this.setState({
					initialPage: 3
				});
			}, 0);
		}
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header style={{ backgroundColor: '#ffffff' }}>
					<Left>
						<Button transparent onPress={() => {
							this.props.navigation.navigate('HomeScreen');
						}}>
							<Icon name="menu" style={{ color: '#019147' }} />
						</Button>
					</Left>
					<Body>
						<Image source={ require('./../assets/images/logo.png') } style={{alignSelf: 'center', width: 200, height: 35, resizeMode: "contain"}} />
					</Body>
					<Right />
				</Header>
				<Tabs initialPage={this.state.initialPage} page={this.state.initialPage} tabBarUnderlineStyle={{ backgroundColor: '#019147' }} renderTabBar={() => <ScrollableTab style={{ backgroundColor: '#ffffff' }} />}>
					<Tab heading="Prijava nepropisno odbačenog otpada" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<PrijavaLokacijeScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Raspored odvoza" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<RasporedOdvozaScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Odvojeno prikupljanje otpada" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<OdvojenoPrikupljanjeOtpadaScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Odvoz glomaznog otpada" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<OdvozGlomaznogOtpadaScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Reciklažno dvorište" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<ReciklaznoDvoristeScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Odlagalište Lovornik" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<OdlagalisteLovornikScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Web buvljak" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<WebBuvljakScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Akcije prikupljanja otpada" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<AkcijePrikupljanjaOtpadaScreen navigation={this.props.navigation} />
					</Tab>
					<Tab heading="Često postavljana pitanja" tabStyle={{backgroundColor: '#ffffff'}} textStyle={{color: '#000000'}} activeTabStyle={{backgroundColor: '#ffffff'}} activeTextStyle={{color: '#019147'}}>
						<CestoPostavljanaPitanjaScreen navigation={this.props.navigation} />
					</Tab>
				</Tabs>
			</Container>
		);
	}
}

export default ZaKorisnikeScreen;
