import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Content,
	Body,
	Icon,
	Left,
	Right,
	Title,
	Text,
	Button,
} from "native-base";

import globalStyles from './../../assets/styles/Style';

class OProjektuScreen extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header style={{ backgroundColor: '#ffffff' }}>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" style={globalStyles.backButtonIcon} />
							<Text style={globalStyles.backButtonText}>Natrag</Text>
						</Button>
					</Left>
					<Body>
						<Title>O projektu</Title>
					</Body>
					<Right/>
				</Header>
				<Content padder>
					<Text style={[ globalStyles.subtitle, { marginTop: 0 }]}>Projekt je sufinancirala Europska unija iz Kohezijskog fonda</Text>
					<Text style={globalStyles.paragraph}>Projekt „Provedba komunikacijske kampanje o održivom gospodarenju otpadom- Zeleni val“ financiran je u okviru programa „Konkurentnost i kohezija“, iz Kohezijskog Fonda EU.</Text>
					<Text style={globalStyles.paragraph}>Cilj projekta je aktivno uključivanje stanovništva i turista u sustav održivog gospodarenja otpadom, odnosno smanjenje nastanka i povećanje odvojeno prikupljenog otpada, a time smanjenje odloženog otpada na odlagalište Lovornik. Aktivnosti ovoga projekta usmjerene su uspostavljanju okolišno i ekonomski održivog sustava gospodarenja otpadom.</Text>
					<Text style={globalStyles.paragraph}>Opis projekta: Komunikacijska kampanja putem raspoloživih sredstava komuniciranja sa stanovništvom i turistima educirat će i informirati o mjerama spriječavanja nastanka otpada, mjerama ponovne uporabe i pravilnog odvajanja otpada, te reciklaže. Unutar kampanje ciljanim skupinama će se prenositi poticajna poruka o važnosti spriječavanja nastanka otpada, recikliranja, održivoj upotrebi materijala, kućnom kompostiranju, pravilnom odvajanju otpada u kućanstvima, te modelu kružnog gospodarstva. Projekt će se provoditi putem digitalnih komunikacijskih alata, tiskanja letaka na poleđini računa, izradu plakata, postavljanje trajnih obavijesnih tabli, uspostavljanje mrežne stranice o održivom gospodarenju otpadom Ploča i Gradca u suradnji s Općinom Gradac. Kroz radionice u osnovnim školama i vrtićima, poruke projekta prenijet će se prilagođenom formom i sadržajem i na najmlađe uzraste, a višejezični letci o pravilnom postupanju s otpadom bit će postavljeni u smještajnim jedinicama privatnih iznajmljivača, kako bi poruka kampanje došla i do turista.</Text>
					<Text style={globalStyles.paragraph}>Očekivani rezultati projekta su značajno smanjenje količine otpada koja se odlaže na odlagalištu Lovornik, povećanje stope odvojeno prikupljenog otpada te podizanje svijesti stanovništva o važnosti održivog gospodarenja otpadom.</Text>
					<Text style={globalStyles.paragraph}>Ukupna vrijednost projekta: 587.600,66 HRK</Text>
					<Text style={globalStyles.paragraph}>Iznos koji sufinancira EU: 479.239,11 HRK</Text>
					<Text style={globalStyles.paragraph}>Razdoblje provedbe projekta: 7/2018 – 2/2020</Text>
					<Text style={globalStyles.paragraph}>Kontakt osoba: Ivanka Pažin (+385 (0)20 676-147)</Text>
					<Text style={[ globalStyles.paragraph, { marginBottom: 0 }]}>www.strukturnifondovi.hr</Text>
				</Content>
			</Container>
		);
	}
}

export default OProjektuScreen;
