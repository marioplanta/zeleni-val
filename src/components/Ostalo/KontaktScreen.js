import React, { Component } from "react";
import {
	View,
	Image
} from "react-native";
import {
	Header,
	Container,
	Content,
	Body,
	Icon,
	Left,
	Right,
	Title,
	Text,
	Button,
} from "native-base";
import { connect } from "react-redux";

import { Input } from "./../common";
import globalStyles from './../../assets/styles/Style';
import { contactNameChanged, contactEmailChanged, contactMessageChanged, sendMessage } from "../../actions";

class KontaktScreen extends Component {
	constructor(props) {
		super(props);

		this.state = {
			message_sent: false
		};
	}

	componentDidUpdate(prevProps, prevState) {
		if(prevProps != this.props) {
			if(this.props.message_sent) {
				this.setState({
					message_sent: true
				});
			}
		}
	}

	onNameChange(text) {
		this.props.contactNameChanged(text);
	}

	onEmailChange(text) {
		this.props.contactEmailChanged(text);
	}

	onMessageChange(text) {
		this.props.contactMessageChanged(text);
	}

	onSendMessage() {
		this.props.sendMessage(this.props.contact_name, this.props.contact_email, this.props.contact_message);
	}

	renderMessage() {
		if(this.state.message_sent) {
			return (
				<Text style={[ globalStyles.paragraph, { color: '#019147' } ]}>Vaša poruka je uspješno poslana</Text>
			);
		}
	}

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header style={{ backgroundColor: '#ffffff' }}>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" style={globalStyles.backButtonIcon} />
							<Text style={globalStyles.backButtonText}>Natrag</Text>
						</Button>
					</Left>
					<Body>
						<Title>Kontakt</Title>
					</Body>
					<Right/>
				</Header>
				<Content padder>
					<Text style={[ globalStyles.subtitle, { marginTop: 0 } ]}>Projekt ZELENI VAL</Text>
					<Text style={globalStyles.paragraphNoMargin}>KOMUNALNO ODRŽAVANJE d.o.o.</Text>
					<Text style={globalStyles.paragraphNoMargin}>Trg kralja Tomislava 7,</Text>
					<Text style={globalStyles.paragraph}>HR-20340, Ploče</Text>

					<Text style={globalStyles.paragraphNoMargin}>Odvoz i deponiranje otpada</Text>
					<Text style={globalStyles.paragraph}>Mobile: +385 (0)95 44 777 01</Text>
					<Text style={globalStyles.paragraph}>kontakt@zeleni-val.com</Text>
					
					<Text style={globalStyles.subtitle}>Nositelj projekta</Text>
					<Text style={globalStyles.paragraphNoMargin}>GRAD PLOČE</Text>
					<Text style={globalStyles.paragraphNoMargin}>Trg kralja Tomislava 23,</Text>
					<Text style={globalStyles.paragraph}>HR-20340, Ploče</Text>

					<Text style={globalStyles.paragraphNoMargin}>Ured gradonačelnika</Text>
					<Text style={globalStyles.paragraph}>Telefon: +385(0)20 679 501</Text>

					<Text style={globalStyles.paragraph}>ured.gradonacelnika@ploce.hr</Text>

					<Text style={globalStyles.subtitle}>Sunositelj projekta</Text>
					<Text style={globalStyles.paragraphNoMargin}>OPĆINA GRADAC</Text>
					<Text style={globalStyles.paragraphNoMargin}>Stjepana Radića 3</Text>
					<Text style={globalStyles.paragraph}>HR-21330, Gradac</Text>

					<Text style={globalStyles.paragraphNoMargin}>Ured načelnika</Text>
					<Text style={globalStyles.paragraph}>Mobile: +385 (0)21 697 601</Text>

					<Text style={globalStyles.paragraph}>info@opcinagradac.hr</Text>

					<Text style={globalStyles.subtitle}>Pošaljite nam poruku</Text>

					<Input
						label="Ime i prezime *"
						onChangeText={this.onNameChange.bind(this)}
						autoCorrect={false}
						value={this.props.contact_name}
						keyboardType={"default"}
					/>
					<Input
						label="Email *"
						onChangeText={this.onEmailChange.bind(this)}
						autoCorrect={false}
						value={this.props.contact_email}
						keyboardType={"email-address"}
					/>
					<Input
						label="Poruka"
						onChangeText={this.onMessageChange.bind(this)}
						autoCorrect={false}
						value={this.props.contact_message}
						keyboardType={"default"}
						multiline = {true}
						numberOfLines = {4}
					/>
					{this.renderMessage()}
					<View style={{ flex: 1, flexDirection: 'row' }}>
						<Button success style={{ backgroundColor: '#019147' }} onPress={this.onSendMessage.bind(this)}><Text>Pošaljite poruku</Text></Button>
					</View>
				</Content>
			</Container>
		);
	}
}

const mapStateToProps = ({ contactReducer }) => {
	const { contact_name, contact_email, contact_message, message_sent, loading } = contactReducer;

	return {
		contact_name,
		contact_email,
		contact_message,
		message_sent,
		loading
	};
};

export default connect(mapStateToProps, {
	contactNameChanged, contactEmailChanged, contactMessageChanged, sendMessage
})(KontaktScreen);
