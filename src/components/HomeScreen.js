import React, { Component } from "react";
import {
	View,
	Image,
	Text
} from "react-native";
import {
	Header,
	Container,
	Content,
	Body,
	Title,
	Button
} from "native-base";
import LinearGradient from 'react-native-linear-gradient';

class HomeScreen extends Component {
	constructor(props) {
		super(props);

		this.state = {
			language: 'hr',
			language_label: 'English version',
			prijava_label: 'Prijava otpada',
			buvljak_label: 'Web buvljak',
			narudzba_label: 'Narudžba odvoza glomaznog otpada'
		};
	}

	changeLanguage() {
		if(this.state.language_label == 'English version') {
			this.setState({
				language: 'en',
				language_label: 'Hrvatska verzija',
				prijava_label: 'Report a location of illegal waste dump',
				buvljak_label: 'Flea market',
				narudzba_label: 'Order a pickup of cumbersome waste'
			});
		} else {
			this.setState({
				language: 'hr',
				language_label: 'English version',
				prijava_label: 'Prijava otpada',
				buvljak_label: 'Web buvljak',
				narudzba_label: 'Narudžba odvoza glomaznog otpada'
			});
		}
	}

	renderPrijavaLabel() {
		if(this.state.language == 'hr') {
			return (
				<Text style={styles.buttonTextStyle}><Text style={{ fontWeight: 'bold' }}>Prijava</Text> otpada</Text>
			);
		} else {
			return (
				<Text style={styles.buttonTextStyle}><Text style={{ fontWeight: 'bold' }}>Report</Text> a location of illegal waste dump</Text>
			);
		}
	}
	renderOtpadLabel() {
		if(this.state.language == 'hr') {
			return (
				<Text style={styles.buttonTextStyle}><Text style={{ fontWeight: 'bold' }}>Narudžba odvoza</Text> glomaznog otpada</Text>
			);
		} else {
			return (
				<Text style={styles.buttonTextStyle}><Text style={{ fontWeight: 'bold' }}>Order a pickup</Text> of cumbersome waste</Text>
			);
		}
	}
	renderBuvljakLabel() {
		if(this.state.language == 'hr') {
			return (
				<Text style={styles.buttonTextStyle}>Web <Text style={{ fontWeight: 'bold' }}>buvljak</Text></Text>
			);
		} else {
			return (
				<Text style={styles.buttonTextStyle}>Flea <Text style={{ fontWeight: 'bold' }}>market</Text></Text>
			);
		}
	}

	render() {
		return (
			<LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['#A8D136', '#009144']} style={{ flex: 1, flexDirection: 'column' }}>
				<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
					<Image source={ require('./../assets/images/logo-white.png') } style={{ alignSelf: 'center', width: 160, height: 35, marginVertical: 30, resizeMode: "contain"}} />
				</View>
				<View style={styles.buttonWrapperStyle}>
					<Button style={styles.buttonStyle} transparent onPress={() => {
						if(this.state.language == 'hr') {
							this.props.navigation.navigate('Za korisnike', { tab: 'Prijava' });
						} else {
							this.props.navigation.navigate('ForUsersScreen', { tab: 'Report' });
						}
					}}>
						<View style={{ width: 30, alignItems: 'center' }}>
							<Image source={ require('./../assets/images/photo-camera.png') } style={{ width: 25, height: 20 }} />
						</View>
						{this.renderPrijavaLabel()}
					</Button>
					<Button style={styles.buttonStyle} transparent onPress={() => {
						if(this.state.language == 'hr') {
							this.props.navigation.navigate('Za korisnike', { tab: 'OdvozOtpada' });
						} else {
							this.props.navigation.navigate('ForUsersScreen', { tab: 'WastePickup' });
						}
					}}>
						<View style={{ width: 30, alignItems: 'center' }}>
							<Image source={ require('./../assets/images/sofa.png') } style={{ width: 30, height: 20 }} />
						</View>
						{this.renderOtpadLabel()}
					</Button>
					<Button style={styles.buttonStyle} transparent onPress={() => {
						if(this.state.language == 'hr') {
							this.props.navigation.navigate('Za korisnike', { tab: 'WebBuvljak' });
						} else {
							this.props.navigation.navigate('ForUsersScreen', { tab: 'FleaMarket' });
						}
					}}>
						<View style={{ width: 30, alignItems: 'center' }}>
							<Image source={ require('./../assets/images/heart.png') } style={{ width: 24, height: 24 }} />
						</View>
						{this.renderBuvljakLabel()}
					</Button>
				</View>
				<View style={{ alignItems: 'center', marginBottom: 15 }}>
					<Button transparent onPress={() => this.changeLanguage()}>
						<Text style={{ color: '#ffffff' }}>{this.state.language_label}</Text>
					</Button>
				</View>
			</LinearGradient>
		);
	}
}

const styles = {
	buttonWrapperStyle: {
		flex: 3,
		padding: 20,
		justifyContent: 'center'
	},
	buttonStyle: {
		backgroundColor: '#ffffff',
		justifyContent: 'center',
		marginVertical: 10,
		paddingLeft: 35,
		paddingRight: 50,
		height: 60,
		justifyContent: 'flex-start'
	},
	buttonTextStyle: {
		marginLeft: 35,
		paddingVertical: 7,
		fontWeight: '400',
		color: '#019147',
		textTransform: 'uppercase'
	}
};

export default HomeScreen;
