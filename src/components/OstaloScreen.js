import React, { Component } from "react";
import {
	View,
	FlatList,
	Image,
} from "react-native";
import {
	Header,
	Container,
	Content,
	Body,
	Title,
	Text,
	ListItem,
	Left,
	Right,
	Icon,
	Button
} from "native-base";

const screens = [
	{
		id: '0',
		title: "Kontakt",
		screen: "KontaktScreen"
	},
	{
		id: '1',
		title: "O projektu",
		screen: "OProjektuScreen"
	}
];

class OstaloScreen extends Component {
	constructor(props) {
		super(props);
	}

	renderItem = ({ item }) => {
		return (
			<ListItem onPress={() => this.props.navigation.navigate(item.screen)}>
				<Left>
					<Text>
						{item.title}
					</Text>
				</Left>
				<Right>
					<Icon name="arrow-forward" />
				</Right>
			</ListItem>
		);
	};

	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<Header style={{ backgroundColor: '#ffffff' }}>
					<Left>
						<Button transparent onPress={() => {
							this.props.navigation.navigate('HomeScreen');
						}}>
							<Icon name="menu" style={{ color: '#019147' }} />
						</Button>
					</Left>
					<Body>
						<Image source={ require('./../assets/images/logo.png') } style={{alignSelf: 'center', width: 200, height: 35, resizeMode: "contain"}} />
					</Body>
					<Right />
				</Header>
				<Content>
					<FlatList
						data={screens}
						renderItem={this.renderItem}
						keyExtractor={item => item.id}
					/>
				</Content>
			</Container>
		);
	}
}

export default OstaloScreen;
