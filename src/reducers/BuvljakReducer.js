import {
	LISTING_NAME_CHANGED,
	LISTING_EMAIL_CHANGED,
	LISTING_MESSAGE_CHANGED,
	FETCH_LISTINGS_BEGIN,
	FETCH_LISTINGS_SUCCESS,
	FETCH_LISTINGS_FAIL,
	SEND_LISTING_BEGIN,
	SEND_LISTING_SUCCESS,
	SEND_LISTING_FAIL
} from '../actions/types';

const INITIAL_STATE = {
	loading: false,
	listing_name: '',
	listing_email: '',
	listing_message: '',
	listing_sent: false,
	listings: null,
	error: ''
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case FETCH_LISTINGS_BEGIN:
			return { ...state, loading: true, error: '' };
		case FETCH_LISTINGS_SUCCESS:
			return { ...state, ...INITIAL_STATE, listings: action.payload };
		case FETCH_LISTINGS_FAIL:
			return { ...state, loading: false, error: action.payload };

		case LISTING_NAME_CHANGED:
			return { ...state, listing_name: action.payload };
		case LISTING_EMAIL_CHANGED:
			return { ...state, listing_email: action.payload };
		case LISTING_MESSAGE_CHANGED:
			return { ...state, listing_message: action.payload };

		case SEND_LISTING_BEGIN:
			return { ...state, loading: true, error: '' };
		case SEND_LISTING_SUCCESS:
			return { ...state, loading: false, listing_sent: true, listing_name: '', listing_email: '', listing_message: '', error: '' };
		case SEND_LISTING_FAIL:
			return { ...state, loading: false, error: action.payload };
		default:
			return state;
	}
};
