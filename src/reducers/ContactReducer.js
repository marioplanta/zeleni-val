import {
	CONTACT_NAME_CHANGED,
	CONTACT_EMAIL_CHANGED,
	CONTACT_MESSAGE_CHANGED,
	SEND_MESSAGE_BEGIN,
	SEND_MESSAGE_SUCCESS,
	SEND_MESSAGE_FAIL
} from '../actions/types';

const INITIAL_STATE = {
	loading: false,
	contact_name: '',
	contact_email: '',
	contact_message: '',
	message_sent: false,
	error: ''
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case CONTACT_NAME_CHANGED:
			return { ...state, contact_name: action.payload };
		case CONTACT_EMAIL_CHANGED:
			return { ...state, contact_email: action.payload };
		case CONTACT_MESSAGE_CHANGED:
			return { ...state, contact_message: action.payload };

		case SEND_MESSAGE_BEGIN:
			return { ...state, loading: true, error: '' };
		case SEND_MESSAGE_SUCCESS:
			return { ...state, ...INITIAL_STATE, message_sent: true };
		case SEND_MESSAGE_FAIL:
			return { ...state, loading: false, error: action.payload };
		default:
			return state;
	}
};
