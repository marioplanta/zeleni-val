import {
	FETCH_NEWS_BEGIN,
	FETCH_NEWS_SUCCESS,
	FETCH_NEWS_FAIL
} from '../actions/types';

const INITIAL_STATE = {
	loading: false,
	news: null,
	error: ''
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case FETCH_NEWS_BEGIN:
			return { ...state, loading: true, error: '' };
		case FETCH_NEWS_SUCCESS:
			return { ...state, ...INITIAL_STATE, news: action.payload };
		case FETCH_NEWS_FAIL:
			return { ...state, loading: false, error: action.payload };
		default:
			return state;
	}
};
