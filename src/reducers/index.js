import { combineReducers } from 'redux';
import NewsReducer from './NewsReducer';
import ContactReducer from './ContactReducer';
import ReportReducer from './ReportReducer';
import RequestReducer from './RequestReducer';
import BuvljakReducer from './BuvljakReducer';

export default combineReducers({
	newsReducer: NewsReducer,
	contactReducer: ContactReducer,
	reportReducer: ReportReducer,
	requestReducer: RequestReducer,
	buvljakReducer: BuvljakReducer
});
