import {
	REPORT_NAME_CHANGED,
	REPORT_EMAIL_CHANGED,
	REPORT_LOCATION_CHANGED,
	REPORT_MESSAGE_CHANGED,
	SEND_REPORT_BEGIN,
	SEND_REPORT_SUCCESS,
	SEND_REPORT_FAIL
} from '../actions/types';

const INITIAL_STATE = {
	loading: false,
	report_name: '',
	report_email: '',
	report_location: '',
	report_message: '',
	report_sent: false,
	error: ''
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case REPORT_NAME_CHANGED:
			return { ...state, report_name: action.payload };
		case REPORT_EMAIL_CHANGED:
			return { ...state, report_email: action.payload };
		case REPORT_LOCATION_CHANGED:
			return { ...state, report_location: action.payload };
		case REPORT_MESSAGE_CHANGED:
			return { ...state, report_message: action.payload };

		case SEND_REPORT_BEGIN:
			return { ...state, loading: true, report_sent: false, error: '' };
		case SEND_REPORT_SUCCESS:
			return { ...state, ...INITIAL_STATE, report_sent: true };
		case SEND_REPORT_FAIL:
			return { ...state, loading: false, report_sent: false, error: action.payload };
		default:
			return state;
	}
};
