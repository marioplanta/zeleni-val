import {
	REQUEST_NAME_CHANGED,
	REQUEST_EMAIL_CHANGED,
	REQUEST_LOCATION_CHANGED,
	REQUEST_MESSAGE_CHANGED,
	SEND_REQUEST_BEGIN,
	SEND_REQUEST_SUCCESS,
	SEND_REQUEST_FAIL
} from '../actions/types';

const INITIAL_STATE = {
	loading: false,
	request_name: '',
	request_email: '',
	request_location: '',
	request_message: '',
	request_sent: false,
	error: ''
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case REQUEST_NAME_CHANGED:
			return { ...state, request_name: action.payload };
		case REQUEST_EMAIL_CHANGED:
			return { ...state, request_email: action.payload };
		case REQUEST_LOCATION_CHANGED:
			return { ...state, request_location: action.payload };
		case REQUEST_MESSAGE_CHANGED:
			return { ...state, request_message: action.payload };

		case SEND_REQUEST_BEGIN:
			return { ...state, loading: true, request_sent: false, error: '' };
		case SEND_REQUEST_SUCCESS:
			return { ...state, ...INITIAL_STATE, request_sent: true };
		case SEND_REQUEST_FAIL:
			return { ...state, loading: false, request_sent: false, error: action.payload };
		default:
			return state;
	}
};
