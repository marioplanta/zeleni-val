import React, { Component } from 'react';
import { Animated, Easing } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator} from 'react-navigation-tabs';
import Icon from "react-native-vector-icons/Ionicons";

import HomeScreen from './components/HomeScreen';

import OdrzivostScreen from './components/OdrzivostScreen';
import EdukativniKutakScreen from './components/EdukativniKutakScreen';
import AktivnostiScreen from './components/AktivnostiScreen';
import ZaKorisnikeScreen from './components/ZaKorisnikeScreen';
import OstaloScreen from './components/OstaloScreen';
import SingleVijestScreen from './components/SingleVijestScreen';

import ForUsersScreen from './components/ForUsersScreen';

import GradPloceScreen from './components/ZaKorisnike/OdvojenoPrikupljanje/GradPloceScreen';
import OpcinaGradacScreen from './components/ZaKorisnike/OdvojenoPrikupljanje/OpcinaGradacScreen';
import RezultatiOdvojenogSakupljanjaScreen from './components/ZaKorisnike/OdvojenoPrikupljanje/RezultatiOdvojenogSakupljanjaScreen';
import PilotProjektiScreen from './components/ZaKorisnike/RasporedOdvoza/PilotProjektiScreen';
import JavniSpremniciScreen from './components/ZaKorisnike/RasporedOdvoza/JavniSpremniciScreen';

import WebBuvljakScreen from './components/ZaKorisnike/WebBuvljakScreen';

import ReportLocationScreen from './components/ZaKorisnike/ReportLocationScreen';
import WastePickupScreen from './components/ZaKorisnike/WastePickupScreen';
import FleaMarketScreen from './components/ZaKorisnike/FleaMarketScreen';

import PapirScreen from './components/EdukativniKutak/PapirScreen';
import PlastikaScreen from './components/EdukativniKutak/PlastikaScreen';
import StakloScreen from './components/EdukativniKutak/StakloScreen';
import MetalScreen from './components/EdukativniKutak/MetalScreen';

import KontaktScreen from './components/Ostalo/KontaktScreen';
import OProjektuScreen from './components/Ostalo/OProjektuScreen';

const OdrzivoGospodarenjeOtpadomTab = createStackNavigator(
	{
		OdrzivostScreen: {
			screen: OdrzivostScreen
		}
	},
	{
		headerMode: 'none'
	}
);
const AktivnostiTab = createStackNavigator(
	{
		AktivnostiScreen: {
			screen: AktivnostiScreen
		}
	},
	{
		headerMode: 'none'
	}
);
const ZaKorisnikeTab = createStackNavigator(
	{
		ZaKorisnikeScreen: {
			screen: ZaKorisnikeScreen
		}
	},
	{
		headerMode: 'none'
	}
);
const EdukativniKutakTab = createStackNavigator(
	{
		EdukativniKutakScreen: {
			screen: EdukativniKutakScreen
		}
	},
	{
		headerMode: 'none'
	}
);
const OstaloTab = createStackNavigator(
	{
		OstaloScreen: {
			screen: OstaloScreen
		}
	},
	{
		headerMode: 'none'
	}
);

const TabNavigator = createBottomTabNavigator(
	{
		'Održivost': {
			screen: OdrzivoGospodarenjeOtpadomTab
		},
		'Aktivnosti': {
			screen: AktivnostiTab
		},
		'Edukacija': {
			screen: EdukativniKutakTab
		},
		'Za korisnike': {
			screen: ZaKorisnikeTab
		},
		'Ostalo': {
			screen: OstaloTab
		}
	},
	{
		headerMode: "none",
		transitionConfig: () => ({
			transitionSpec: {
				duration: 0,
				timing: Animated.timing,
				easing: Easing.step0
			}
		}),
		initialRouteName: "Održivost",
		defaultNavigationOptions: ({ navigation }) => ({
			tabBarIcon: ({ focused, tintColor }) => {
				const { routeName } = navigation.state;
				let iconName;
				if (routeName === "Održivost") {
					return <Icon name={"ios-leaf"} style={{ fontSize: 22, color: tintColor, paddingTop: 5 }} />;
				} else if (routeName === "Aktivnosti") {
					return <Icon name={"ios-apps"} style={{ fontSize: 22, color: tintColor, paddingTop: 5 }} />;
				} else if (routeName === "Za korisnike") {
					return <Icon name={"ios-people"} style={{ fontSize: 24, color: tintColor, paddingTop: 5 }} />;
				} else if (routeName === "Edukacija") {
					return <Icon name={"ios-book"} style={{ fontSize: 22, color: tintColor, paddingTop: 5 }} />;
				} else if (routeName === "Ostalo") {
					return <Icon name={"ios-list"} style={{ fontSize: 22, color: tintColor, paddingTop: 5 }} />;
				}

				return <Icon name={"ios-arrow-back"} style={{ fontSize: 18, color: tintColor, paddingTop: 5 }} />;
			}
		}),
		tabBarOptions: {
			activeTintColor: "#019147",
			inactiveTintColor: "#8e8e93",
			inactiveBackgroundColor: "#ffffff",
			activeBackgroundColor: "#ffffff",
			labelStyle: {
				paddingBottom: 5
			},
		},
	},
);

const MainStack = createStackNavigator(
	{
		HomeScreen: {
			screen: HomeScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		TabNavigator: {
			screen: TabNavigator,
			navigationOptions: ({ navigation }) => ({
				header: null,
				headerStyle: {
					backgroundColor: "#333333",
					elevation: 0,
					borderBottomWidth: 0,
					shadowColor: "transparent"
				},
				headerTintColor: "#ffe600",
				headerTitleStyle: {
					color: "#ffffff",
				},
			})
		},
		ForUsersScreen: {
			screen: ForUsersScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		ReportLocationScreen: {
			screen: ReportLocationScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		WastePickupScreen: {
			screen: WastePickupScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		FleaMarketScreen: {
			screen: FleaMarketScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		KontaktScreen: {
			screen: KontaktScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		OProjektuScreen: {
			screen: OProjektuScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		PapirScreen: {
			screen: PapirScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		PlastikaScreen: {
			screen: PlastikaScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		StakloScreen: {
			screen: StakloScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		MetalScreen: {
			screen: MetalScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		GradPloceScreen: {
			screen: GradPloceScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		OpcinaGradacScreen: {
			screen: OpcinaGradacScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		RezultatiOdvojenogSakupljanjaScreen: {
			screen: RezultatiOdvojenogSakupljanjaScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		PilotProjektiScreen: {
			screen: PilotProjektiScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		JavniSpremniciScreen: {
			screen: JavniSpremniciScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
		SingleVijestScreen: {
			screen: SingleVijestScreen,
			navigationOptions: ({ navigation }) => ({
				header: null,
			})
		},
	}
);

export default createAppContainer(MainStack);
