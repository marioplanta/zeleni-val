import React, { Component } from 'react';

import MainNavigator from './MainNavigator';

class Main extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<MainNavigator />
		);
	}
}

export default Main;
