import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	tagline: {
		color: '#86bb46',
		textTransform: 'uppercase',
		marginBottom: 5
	},
	title: {
		fontSize: 30,
		fontWeight: '700',
		textTransform: 'uppercase',
		marginBottom: 10
	},
	subtitle: {
		fontSize: 20,
		fontWeight: '600',
		marginTop: 10,
		marginBottom: 10
	},
	paragraph: {
		fontSize: 16,
		marginBottom: 15
	},
	paragraphNoMargin: {
		fontSize: 16
	},

	backButtonText: {
		color: '#019147'
	},
	backButtonIcon: {
		color: '#019147'
	},

	iconStyle: {
		color: '#019147',
		fontSize: 54
	},
});